
#
# Define the constraints
#
# constrain class_set perm_set expression ;
#
# expression : ( expression )
#	     | not expression
#	     | expression and expression
#	     | expression or expression
#	     | u1 op u2
#	     | r1 role_op r2
#	     | t1 op t2
#	     | u1 op names
#	     | u2 op names
#	     | r1 op names
#	     | r2 op names
#	     | t1 op names
#	     | t2 op names
#
# op : == | !=
# role_op : == | != | eq | dom | domby | incomp
#
# names : name | { name_list }
# name_list : name | name_list name
#

define(`basic_ubac_conditions',`
	u1 == u2
	or u1 == system_u
	or u2 == system_u
	or t1 != ubac_constrained_type
	or t2 != ubac_constrained_type
')

define(`basic_ubac_constraint',`
	constrain $1 *
	(
		basic_ubac_conditions
	);
')

define(`exempted_ubac_constraint',`
	constrain $1 *
	(
		basic_ubac_conditions
		or t1 == $2
	);
')

########################################
#
# File rules
#

exempted_ubac_constraint(dir, ubacfile)
exempted_ubac_constraint(file, ubacfile)
exempted_ubac_constraint(lnk_file, ubacfile)
exempted_ubac_constraint(fifo_file, ubacfile)
exempted_ubac_constraint(sock_file, ubacfile)
exempted_ubac_constraint(chr_file, ubacfile)
exempted_ubac_constraint(blk_file, ubacfile)

# SELinux object identity change constraint:
constrain dir_file_class_set { create relabelto relabelfrom }
(
	u1 == u2
	or ( u2 == system_u and t1 == ubac_can_change_object_identity_to_sys )
	or ( u1 == system_u and t1 == ubac_can_change_object_identity_to_user )
	or ( t1 == container_manager_domain and u2 == system_u and t2 == containerfile )
	or t1 == ubac_can_change_object_identity_total
);

########################################
#
# Process rules
#

constrain process { sigchld sigkill sigstop signull signal ptrace getsched setsched getsession getpgid setpgid getcap setcap share getattr setrlimit getrlimit }
(
	basic_ubac_conditions
	or t1 == ubacproc
);

# These permissions do not have ubac constraint exemptions:
constrain process { fork setexec setfscreate setcurrent execmem execstack execheap setkeycreate setsockcreate }
(
	u1 == u2
);

define(`process_transition_conditions',`
	u1 == u2
	or ( u1 == system_u and t1 == ubac_can_change_process_identity_to_user and t2 == process_user_target )
	or ( u2 == system_u and t1 == ubac_can_change_process_identity_to_sys )
	or ( t1 == container_manager_domain and u2 == system_u and t2 == container_domain )
	or ( t1 == ubac_can_change_process_identity_total )
')

constrain process { transition dyntransition noatsecure siginh rlimitinh }
(
	process_transition_conditions
);

constrain process2 { nnp_transition nosuid_transition }
(
	process_transition_conditions
);

undefine(`process_transition_conditions')

########################################
#
# File descriptor rules
#

exempted_ubac_constraint(fd, ubacfd)

########################################
#
# Socket rules
#

exempted_ubac_constraint(socket, ubacsock)
exempted_ubac_constraint(tcp_socket, ubacsock)
exempted_ubac_constraint(udp_socket, ubacsock)
exempted_ubac_constraint(rawip_socket, ubacsock)
exempted_ubac_constraint(netlink_socket, ubacsock)
exempted_ubac_constraint(packet_socket, ubacsock)
exempted_ubac_constraint(key_socket, ubacsock)
exempted_ubac_constraint(unix_stream_socket, ubacsock)
exempted_ubac_constraint(unix_dgram_socket, ubacsock)
exempted_ubac_constraint(netlink_route_socket, ubacsock)
exempted_ubac_constraint(netlink_tcpdiag_socket, ubacsock)
exempted_ubac_constraint(netlink_nflog_socket, ubacsock)
exempted_ubac_constraint(netlink_xfrm_socket, ubacsock)
exempted_ubac_constraint(netlink_selinux_socket, ubacsock)
exempted_ubac_constraint(netlink_audit_socket, ubacsock)
exempted_ubac_constraint(netlink_dnrt_socket, ubacsock)
exempted_ubac_constraint(netlink_kobject_uevent_socket, ubacsock)
exempted_ubac_constraint(appletalk_socket, ubacsock)
exempted_ubac_constraint(dccp_socket, ubacsock)
exempted_ubac_constraint(tun_socket, ubacsock)
exempted_ubac_constraint(netlink_iscsi_socket, ubacsock)
exempted_ubac_constraint(netlink_fib_lookup_socket, ubacsock)
exempted_ubac_constraint(netlink_connector_socket, ubacsock)
exempted_ubac_constraint(netlink_netfilter_socket, ubacsock)
exempted_ubac_constraint(netlink_generic_socket, ubacsock)
exempted_ubac_constraint(netlink_scsitransport_socket, ubacsock)
exempted_ubac_constraint(netlink_rdma_socket, ubacsock)
exempted_ubac_constraint(netlink_crypto_socket, ubacsock)
exempted_ubac_constraint(sctp_socket, ubacsock)
exempted_ubac_constraint(icmp_socket, ubacsock)
exempted_ubac_constraint(ax25_socket, ubacsock)
exempted_ubac_constraint(ipx_socket, ubacsock)
exempted_ubac_constraint(netrom_socket, ubacsock)
exempted_ubac_constraint(atmpvc_socket, ubacsock)
exempted_ubac_constraint(x25_socket, ubacsock)
exempted_ubac_constraint(rose_socket, ubacsock)
exempted_ubac_constraint(decnet_socket, ubacsock)
exempted_ubac_constraint(atmsvc_socket, ubacsock)
exempted_ubac_constraint(rds_socket, ubacsock)
exempted_ubac_constraint(irda_socket, ubacsock)
exempted_ubac_constraint(pppox_socket, ubacsock)
exempted_ubac_constraint(llc_socket, ubacsock)
exempted_ubac_constraint(can_socket, ubacsock)
exempted_ubac_constraint(tipc_socket, ubacsock)
exempted_ubac_constraint(bluetooth_socket, ubacsock)
exempted_ubac_constraint(iucv_socket, ubacsock)
exempted_ubac_constraint(rxrpc_socket, ubacsock)
exempted_ubac_constraint(isdn_socket, ubacsock)
exempted_ubac_constraint(phonet_socket, ubacsock)
exempted_ubac_constraint(ieee802154_socket, ubacsock)
exempted_ubac_constraint(caif_socket, ubacsock)
exempted_ubac_constraint(alg_socket, ubacsock)
exempted_ubac_constraint(nfc_socket, ubacsock)
exempted_ubac_constraint(vsock_socket, ubacsock)
exempted_ubac_constraint(kcm_socket, ubacsock)
exempted_ubac_constraint(qipcrtr_socket, ubacsock)
exempted_ubac_constraint(smc_socket, ubacsock)
exempted_ubac_constraint(xdp_socket, ubacsock)
exempted_ubac_constraint(mctp_socket, ubacsock)

constrain socket_class_set { create relabelto relabelfrom }
(
	u1 == u2
	or ( u2 == system_u and t1 == ubac_can_change_object_identity_to_sys )
	or t1 == ubac_can_change_object_identity_total
);

########################################
#
# SysV IPC rules

exempted_ubac_constraint(sem, ubacipc)
exempted_ubac_constraint(msg, ubacipc)
exempted_ubac_constraint(msgq, ubacipc)
exempted_ubac_constraint(shm, ubacipc)
exempted_ubac_constraint(ipc, ubacipc)

########################################
#
# D-BUS rules
#

exempted_ubac_constraint(dbus, ubacdbus)

########################################
#
# Key rules
#

exempted_ubac_constraint(key, ubackey)

########################################
#
# Anonymous inode rules
#

exempted_ubac_constraint(anon_inode, ubacproc)

# SELinux object role change constraint:
constrain anon_inode { create relabelto relabelfrom }
(
	u1 == u2
	#or ( u2 == system_u and t1 == ubac_can_change_object_identity_to_sys )
	#or t1 == ubac_can_change_object_identity_total
);


# these classes have no UBAC restrictions
#class security
#class system
#class service
#class capability
#class memprotect
#class passwd			# userspace
#class association
#class node
#class netif
#class packet
#class peer
#class capability2
#class context			# userspace
#class filesystem
#class kernel_service
#class binder
#class infiniband_pkey
#class infiniband_endport
#class cap_userns
#class cap_userns2
#class bpf
#class perf_event
#class lockdown
#class io_uring
#class user_namespace



undefine(`basic_ubac_constraint')
undefine(`basic_ubac_conditions')
undefine(`exempted_ubac_constraint')
