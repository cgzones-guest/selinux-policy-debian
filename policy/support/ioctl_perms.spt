########################################
#
# Support macros for extended ioctl permissions
#


#
# Wrapper for ioctl permissions
# Bare ioctl permission usage signals.un-constrained access.
#
# Parameters:
# 1. source domain
# 2. target domain
# 3. target class
# 4. extended permissions
#
define(`xperm_pattern',`
	allow $1 $2:$3 ioctl;

	ifdef(`xperm_rules',`
		allowxperm $1 $2:$3 ioctl $4;
	')
')


# Pseudo permission to deny all.
define(`xperm_denyall', `{ 0x0000 }')

# Pseudo permission for O_IOCTL access.
define(`xperm_o_ioctl', `{ 0x0007 }')


# Get disk parameters.
define(`ioctl_fdgetprm', `{ 0x0204 }')
# Get the floppy controller state.
define(`ioctl_fdgetfdcstat', `{ 0x0215 }')
# Eject the disk.
define(`ioctl_fdeject', `{ 0x025a }')
# Get the BIOS disk parameters.
define(`ioctl_hdio_getgeo', `{ 0x0301 }')
# Get current IDE blockmode setting.
define(`ioctl_hdio_get_multcount', `{ 0x0304 }')
# Get IDE identification info.
define(`ioctl_hdio_get_identity', `{ 0x030d }')


# Get the Context Identifier of the local machine.
define(`ioctl_vm_sockets_get_local_cid', `{ 0x07b9 }')


# Get read-only status.
define(`ioctl_blkroget', `{ 0x125e }')
# Forces a reread of the SCSI disk partition tables.
define(`ioctl_blkrrpart', `{ 0x125f }')
# Get the device size in sectors (std + 64).
define(`ioctl_blkgetsize', `{ 0x1260 0x1272 }')
# Flush buffer cache.
define(`ioctl_blkflsbuf', `{ 0x1261 }')
# Get block device physical sector size.
define(`ioctl_blksszget', `{ 0x1268 }')
# Modify partitions.
define(`ioctl_blkpg', `{ 0x1269 }')
# Get block device logical sector size.
define(`ioctl_blkbszget', `{ 0x1270 }')
# Get smallest preferred I/O size.
define(`ioctl_blkiomin', `{ 0x1278 }')
# Get the optimal I/O size.
define(`ioctl_blkioopt', `{ 0x1279 }')
# Get alignment status of a device.
define(`ioctl_blkalignoff', `{ 0x127a }')
# Get device physical sector size.
define(`ioctl_blkpbszget', `{ 0x127b }')
# Get if discarded blocks are zeroed or not.
define(`ioctl_blkdiscardzeroes', `{ 0x127c }')
# Retrieves disk sequence number.
define(`ioctl_blkgetdiskseq', `{ 0x1280 }')
# Get the device zone size in number of 512 B sectors.
define(`ioctl_blkgetzonesz', `{ 0x1284 }')


# Get the driver version.
define(`ioctl_sg_get_version_num', `{ 0x2282 }')
# Send SCSI commands to a device.
define(`ioctl_sg_io', `{ 0x2285 }')


# TODO : SNDRV_PCM_IOCTL_PVERSION
define(`ioctl_sndrv_pcm_ioctl_pversion', `{ 0x4100 }')
# TODO : SNDRV_PCM_IOCTL_INFO
define(`ioctl_sndrv_pcm_ioctl_info', `{ 0x4101 }')
# TODO : SNDRV_PCM_IOCTL_TTSTAMP
define(`ioctl_sndrv_pcm_ioctl_ttstamp', `{ 0x4103 }')
# TODO : SNDRV_PCM_IOCTL_USER_PVERSION
define(`ioctl_sndrv_pcm_ioctl_user_pversion', `{ 0x4104 }')
# TODO : SNDRV_PCM_IOCTL_HW_REFINE
define(`ioctl_sndrv_pcm_ioctl_hw_refine', `{ 0x4110 }')
# TODO : SNDRV_PCM_IOCTL_HW_PARAMS
define(`ioctl_sndrv_pcm_ioctl_hw_params', `{ 0x4111 }')
# TODO : SNDRV_PCM_IOCTL_HW_FREE
define(`ioctl_sndrv_pcm_ioctl_hw_free', `{ 0x4112 }')
# TODO : SNDRV_PCM_IOCTL_SW_PARAMS
define(`ioctl_sndrv_pcm_ioctl_sw_params', `{ 0x4113 }')
# TODO : SNDRV_PCM_IOCTL_HWSYNC
define(`ioctl_sndrv_pcm_ioctl_hwsync', `{ 0x4122 }')
# TODO : SNDRV_PCM_IOCTL_STATUS_EXT
define(`ioctl_sndrv_pcm_ioctl_status_ext', `{ 0x4124 }')
# TODO : SNDRV_PCM_IOCTL_CHANNEL_INFO
define(`ioctl_sndrv_pcm_ioctl_channel_info', `{ 0x4132 }')
# TODO : SNDRV_PCM_IOCTL_PREPARE
define(`ioctl_sndrv_pcm_ioctl_prepare', `{ 0x4140 }')
# TODO : SNDRV_PCM_IOCTL_START
define(`ioctl_sndrv_pcm_ioctl_start', `{ 0x4142 }')
# TODO : SNDRV_PCM_IOCTL_DROP
define(`ioctl_sndrv_pcm_ioctl_drop', `{ 0x4143 }')
# TODO : SNDRV_PCM_IOCTL_REWIND
define(`ioctl_sndrv_pcm_ioctl_rewind', `{ 0x4146 }')
# TODO: SNDRV_PCM_IOCTL_RESUME
define(`ioctl_sndrv_pcm_ioctl_resume', `{ 0x4147 }')


## linux/input.h

# Get driver version.
define(`ioctl_eviocgversion', `{ 0x4501 }')
# Get device ID.
define(`ioctl_eviocgid', `{ 0x4502 }')
# Retrieve keymap data.
define(`ioctl_eviocgkeycode', `{ 0x4504 }')
# Get device name.
define(`ioctl_eviocgname', `{ 0x4506 }')
# Get physical location.
define(`ioctl_eviocgphys', `{ 0x4507 }')
# Get unique identifier.
define(`ioctl_eviocguniq', `{ 0x4508 }')
# Get device properties.
define(`ioctl_eviocgprop', `{ 0x4509 }')
# Get MT slot values.
define(`ioctl_eviocgmtslots', `{ 0x450a }')
# Get global key state.
define(`ioctl_eviocgkey', `{ 0x4518 }')
# Get all LEDs.
define(`ioctl_eviocgled', `{ 0x4519 }')
# Get all switch states.
define(`ioctl_eviocgsw', `{ 0x451b }')
# Get event bits.
define(`ioctl_eviocgbit', `{ 0x4520-0x4525 0x4531-0x4532 0x4534-0x4537 }')
# Get abs value/limits.
define(`ioctl_eviocgabs', `{ 0x4540-0x457f }')
# Revoke device-access irrecoverably for open-file
define(`ioctl_eviocrevoke', `{ 0x4591 }')
# Set event-masks.
define(`ioctl_eviocsmask', `{ 0x4593 }')


# Get screen information.
define(`ioctl_fbioget_fscreeninfo', `{ 0x4602 }')


# Connect to firmware Feature/Client.
define(`ioctl_mei_connect_client', `{ 0x4801 }')


# Get Bluetooth device list.
define(`ioctl_hcigetdevlist', `{ 0x48d2 }')


# Generate tone of specified length.
define(`ioctl_kdmktone', `{ 0x4b30 }')
# Get keyboard type.
define(`ioctl_kdgkbtype', `{ 0x4b33 }')
# Set text/graphics mode.
define(`ioctl_kdsetmode', `{ 0x4b3a }')
# Get text/graphics mode.
define(`ioctl_kdgetmode', `{ 0x4b3b }')
# Get current keyboard mode.
define(`ioctl_kdgkbmode', `{ 0x4b44 }')
# Set current keyboard mode.
define(`ioctl_kdskbmode', `{ 0x4b45 }')
# Indicate willingness to accept signal when it is generated by pressing an appropriate key combination.
define(`ioctl_kdsigaccept', `{ 0x4b4e }')
# Get keyboard flags CapsLock, NumLock, ScrollLock (not lights).
define(`ioctl_kdgkbled', `{ 0x4b64 }')
# Put unicode-to-font mapping in kernel.
define(`ioctl_pio_unimap', `{ 0x4b67 }')
# Clear table, possibly advise hash algorithm.
define(`ioctl_pio_unimapclr', `{ 0x4b68 }')
# Get the current default color map from kernel.
define(`ioctl_gio_cmap', `{ 0x4b70 }')
# Change the default text-mode color map.
define(`ioctl_pio_cmap', `{ 0x4b71 }')
# Font operations (set or get).
define(`ioctl_kdfontop', `{ 0x4b72 }')
# Write kernel accent table.
define(`ioctl_kdskbdiacruc', `{ 0x4bfb }')


# Disassociate the loop device from any file descriptor.
define(`ioctl_loop_clr_fd', `{ 0x4c01 }')
# Get the status of the loop device.
define(`ioctl_loop_get_status', `{ 0x4c03 0x4c05 }')
# Setup and configure loop device parameters.
define(`ioctl_loop_configure', `{ 0x4c0a }')
# Remove a loop device.
define(`ioctl_loop_ctl_remove', `{ 0x4c81 }')
# Allocate or find a free loop device for use.
define(`ioctl_loop_ctl_get_free', `{ 0x4c82 }')


# Retrieve entropy count of input pool.
define(`ioctl_rndgetentcnt', `{ 0x5200 }')
# Add additional entropy to input pool.
define(`ioctl_rndaddentropy', `{ 0x5203 }')


## sound/asequencer.h

# TODO: SNDRV_SEQ_IOCTL_PVERSION
define(`ioctl_sndrv_seq_ioctl_pversion', `{ 0x5300 }')
# TODO: SNDRV_SEQ_IOCTL_CLIENT_ID
define(`ioctl_sndrv_seq_ioctl_client_id', `{ 0x5301 }')
# TODO: SNDRV_SEQ_IOCTL_RUNNING_MODE
define(`ioctl_sndrv_seq_ioctl_running_mode', `{ 0x5303 }')
# TODO: SNDRV_SEQ_IOCTL_USER_PVERSION
define(`ioctl_sndrv_seq_ioctl_user_pversion', `{ 0x5304 }')
# TODO: SNDRV_SEQ_IOCTL_GET_CLIENT_INFO
define(`ioctl_sndrv_seq_ioctl_get_client_info', `{ 0x5310 }')
# TODO: SNDRV_SEQ_IOCTL_SET_CLIENT_INFO
define(`ioctl_sndrv_seq_ioctl_set_client_info', `{ 0x5311 }')
# TODO: SNDRV_SEQ_IOCTL_CREATE_PORT
define(`ioctl_sndrv_seq_ioctl_create_port', `{ 0x5320 }')
# TODO: SNDRV_SEQ_IOCTL_GET_PORT_INFO
define(`ioctl_sndrv_seq_ioctl_get_port_info', `{ 0x5322 }')
# TODO: SNDRV_SEQ_IOCTL_SUBSCRIBE_PORT
define(`ioctl_sndrv_seq_ioctl_subscribe_port', `{ 0x5330 }')
# TODO: SNDRV_SEQ_IOCTL_CREATE_QUEUE
define(`ioctl_sndrv_seq_ioctl_create_queue', `{ 0x5332 }')
# TODO: SNDRV_SEQ_IOCTL_GET_QUEUE_TIMER
define(`ioctl_sndrv_seq_ioctl_get_queue_timer', `{ 0x5345 }')
# TODO: SNDRV_SEQ_IOCTL_SET_QUEUE_TIMER
define(`ioctl_sndrv_seq_ioctl_set_queue_timer', `{ 0x5346 }')
# TODO: SNDRV_SEQ_IOCTL_GET_CLIENT_POOL
define(`ioctl_sndrv_seq_ioctl_get_client_pool', `{ 0x534b }')
# TODO: SNDRV_SEQ_IOCTL_SET_CLIENT_POOL
define(`ioctl_sndrv_seq_ioctl_set_client_pool', `{ 0x534c }')
# TODO: SNDRV_SEQ_IOCTL_QUERY_NEXT_CLIENT
define(`ioctl_sndrv_seq_ioctl_query_next_client', `{ 0x5351 }')
# TODO: SNDRV_SEQ_IOCTL_QUERY_NEXT_PORT
define(`ioctl_sndrv_seq_ioctl_query_next_port', `{ 0x5352 }')


## linux/cdrom.h

# Get tray position, etc. .
define(`ioctl_cdrom_drive_status', `{ 0x5326 }')
# Lock or unlock door.
define(`ioctl_cdrom_lockdoor', `{ 0x5329 }')
# Get capabilities.
define(`ioctl_cdrom_get_capability', `{ 0x5331 }')
# Get last block written on disc.
define(`ioctl_cdrom_last_written', `{ 0x5395 }')


# Get the current serial port settings.
define(`ioctl_tcgets', `{ 0x5401 0x542a }')
# Set the current serial port settings.
define(`ioctl_tcsets', `{ 0x5402 0x542b }')
# Allow the output buffer to drain, and set the current serial port settings.
define(`ioctl_tcsetsw', `{ 0x5403 0x542c }')
# Allow the output buffer to drain, discard pending input, and set the current serial port settings.
define(`ioctl_tcsetsf', `{ 0x5404 0x542d }')
# Send a break
define(`ioctl_tcsbrk',`{ 0x5409 }')
# Suspends transmission or reception of data
define(`ioctl_tcxonc', `{ 0x540a }')
# Discards data written to the object but not transmitted, or data received but not read.
define(`ioctl_tcflsh', `{ 0x540b }')
# Disable exclusive mode.
define(`ioctl_tiocnxcl', `{ 0x540d }')
# Make the given terminal the controlling terminal of the calling process.
define(`ioctl_tiocsctty', `{ 0x540e }')
# Get the process group ID of the foreground process group on this terminal.
define(`ioctl_tiocgpgrp', `{ 0x540f }')
# Set the foreground process group ID of this terminal.
define(`ioctl_tiocspgrp', `{ 0x5410 }')
# Get the number of bytes in the output buffer.
define(`ioctl_tiocoutq', `{ 0x5411 }')
# Get window size.
define(`ioctl_tiocgwinsz', `{ 0x5413 }')
# Set window size.
define(`ioctl_tiocswinsz', `{ 0x5414 }')
# Redirect output that would have gone to /dev/console or /dev/tty0 to the given terminal.
define(`ioctl_tioccons', `{ 0x541d }')
# Get the serial line information.
define(`ioctl_tiocgserial', `{ 0x541e }')
# Detach the calling process from its controlling terminal.
define(`ioctl_tiocnotty', `{ 0x5422 }')
# Get the session ID of the given terminal.
define(`ioctl_tiocgsid', `{ 0x5429 }')
# Get Pty Number (of pty-mux device)
define(`ioctl_tiocgptn', `{ 0x5430 }')
# Set or remove pseudoterminal slave device.
define(`ioctl_tiocsptlck', `{ 0x5431 }')
# Clean tty shutdown of all ttys.
define(`ioctl_tiocvhangup', `{ 0x5437 }')
# Get current packet mode setting.
define(`ioctl_tiocgpkt', `{ 0x5438 }')
# Given a file descriptor in fd that refers to a pseudoterminal master, open and return a new file descriptor that refers to the peer pseudoterminal slave device.
define(`ioctl_tiocgptpeer', `{ 0x5441 }')
# Unset close-on-exec (obsolete with polcap ioctl_skip_cloexec).
#define(`ioctl_fionclex', `{ 0x5450 }')
# Set close-on-exec (obsolete with polcap ioctl_skip_cloexec).
#define(`ioctl_fioclex', `{ 0x5451 }')
# Get locking status of the termios structure of the terminal.
define(`ioctl_tiocglcktrmios', `{ 0x5456 }')
# Set locking status of the termios structure of the terminal. (requires CAP_ADMIN)
define(`ioctl_tiocslcktrmios', `{ 0x5457 }')


# Get version.
define(`ioctl_sndrv_ctl_ioctl_pversion', `{ 0x5500 }')
# Get card info.
define(`ioctl_sndrv_ctl_ioctl_card_info', `{ 0x5501 }')
# Get elements list.
define(`ioctl_sndrv_ctl_ioctl_elem_list', `{ 0x5510 }')
# Get element info.
define(`ioctl_sndrv_ctl_ioctl_elem_info', `{ 0x5511 }')
# Read element.
define(`ioctl_sndrv_ctl_ioctl_elem_read', `{ 0x5512 }')
# Write element.
define(`ioctl_sndrv_ctl_ioctl_elem_write', `{ 0x5513 }')
# Lock element.
define(`ioctl_sndrv_ctl_ioctl_elem_lock', `{ 0x5514 }')
# Unlock element.
define(`ioctl_sndrv_ctl_ioctl_elem_unlock', `{ 0x5515 }')
# Subscribe events.
define(`ioctl_sndrv_ctl_ioctl_subscribe_events', `{ 0x5516 }')
# Add element.
define(`ioctl_sndrv_ctl_ioctl_elem_add', `{ 0x5517 }')
# TODO : SNDRV_CTL_IOCTL_TLV_READ
define(`ioctl_sndrv_ctl_ioctl_tlv_read', `{ 0x551a }')
# TODO : SNDRV_CTL_IOCTL_TLV_WRITE
define(`ioctl_sndrv_ctl_ioctl_tlv_write', `{ 0x551b }')
# TODO: SNDRV_CTL_IOCTL_PCM_NEXT_DEVICE
define(`ioctl_sndrv_ctl_ioctl_pcm_next_device', `{ 0x5530 }')
# TODO: SNDRV_CTL_IOCTL_PCM_INFO
define(`ioctl_sndrv_ctl_ioctl_pcm_info', `{ 0x5531 }')
# TODO : SNDRV_CTL_IOCTL_PCM_PREFER_SUBDEVICE
define(`ioctl_sndrv_ctl_ioctl_pcm_prefer_subdevice', `{ 0x5532 }')


## linux/vt.h

# Return the first available non-opened console.
define(`ioctl_vt_openqry', `{ 0x5600 }')
# Get mode of active vt.
define(`ioctl_vt_getmode', `{ 0x5601 }')
# Set mode of active vt.
define(`ioctl_vt_setmode', `{ 0x5602 }')
# Get global vt state info.
define(`ioctl_vt_getstate', `{ 0x5603 }')
# Release a display.
define(`ioctl_vt_reldisp', `{ 0x5605 }')
# Switch to vt.
define(`ioctl_vt_activate', `{ 0x5606 }')
# Wait until vt argp has been activated.
define(`ioctl_vt_waitactive', `{ 0x5607 }')
# Deallocate all unused consoles
define(`ioctl_vt_disallocate', `{ 0x5608 }')


## linux/videodev2.h

# Query device capabilities.
define(`ioctl_vidioc_querycap', `{ 0x5600 }')
# Enumerate image formats.
define(`ioctl_vidioc_enum_fmt', `{ 0x5602 }')
# Set the data format.
define(`ioctl_vidioc_s_fmt', `{ 0x5605 }')
# Initiate Memory Mapping, User Pointer I/O or DMA buffer I/O.
define(`ioctl_vidioc_reqbufs', `{ 0x5608 }')
# Query the status of a buffer.
define(`ioctl_vidioc_querybufs', `{ 0x5609 }')
# Exchange a buffer with the driver.
define(`ioctl_vidioc_qbuf', `{ 0x560f }')
# Export a buffer as a DMABUF file descriptor.
define(`ioctl_vidioc_expbuf', `{ 0x5610 }')
# Exchange a buffer with the driver.
define(`ioctl_vidioc_dqbuf', `{ 0x5611 }')
# Start streaming I/O.
define(`ioctl_vidioc_streamon', `{ 0x5612 }')
# Stop streaming I/O.
define(`ioctl_vidioc_streamoff', `{ 0x5613 }')
# Get streaming parameters.
define(`ioctl_vidioc_g_parm', `{ 0x5615 }')
# Set streaming parameters.
define(`ioctl_vidioc_s_parm', `{ 0x5616 }')
# Query the video standard of the current input.
define(`ioctl_vidioc_g_std', `{ 0x5617 }')
# Enumerate supported video standards.
define(`ioctl_vidioc_enumstd', `{ 0x5619 }')
# Enumerate video inputs.
define(`ioctl_vidioc_enuminput', `{ 0x561a }')
# Get the value of a control.
define(`ioctl_vidioc_g_ctrl', `{ 0x561b }')
# Enumerate controls and menu control items.
define(`ioctl_vidioc_queryctrl', `{ 0x5624 }')
# Enumerate controls and menu control items.
define(`ioctl_vidioc_querymenu', `{ 0x5625 }')
# Query the current video input.
define(`ioctl_vidioc_g_input', `{ 0x5626 }')
# Information about the video cropping and scaling abilities.
define(`ioctl_vidioc_cropcap', `{ 0x563a }')
# Set the current cropping rectangle.
define(`ioctl_vidioc_s_crop', `{ 0x563c }')
# Try a data format.
define(`ioctl_vidioc_try_fmt', `{ 0x5640 }')
# Enumerate frame sizes.
define(`ioctl_vidioc_enum_framesizes', `{ 0x564a }')
# Enumerate frame intervals.
define(`ioctl_vidioc_enum_frameintervals', `{ 0x564b }')
# Subscribe event.
define(`ioctl_vidioc_subscribe_event', `{ 0x565a }')
# Create buffers for Memory Mapped or User Pointer or DMA Buffer I/O.
define(`ioctl_vidioc_create_bufs', `{ 0x565c }')
# Get one of the selection rectangles.
define(`ioctl_vidioc_g_selection', `{ 0x565e }')
# Set one of the selection rectangles.
define(`ioctl_vidioc_s_selection', `{ 0x565f }')
# Enumerate controls and menu control items.
define(`ioctl_vidioc_query_ext_ctrl', `{ 0x5667 }')


# Ask what the device can do.
define(`ioctl_wdioc_getsupport', `{ 0x5700 }')
# Ask for the current status.
define(`ioctl_wdioc_getstatus', `{ 0x5701 }')
# Ask for the status at the last reboot.
define(`ioctl_wdioc_getbootstatus', `{ 0x5702 }')
# Control aspects of the cards operation.
define(`ioctl_wdioc_setoptions', `{ 0x5704 }')
# Pinging the watchdog.
define(`ioctl_wdioc_keepalive', `{ 0x5705 }')
# Modify the watchdog timeout on the fly.
define(`ioctl_wdioc_settimeout', `{ 0x5706 }')
# Query the current timeout.
define(`ioctl_wdioc_gettimeout', `{ 0x5707 }')
# Query the current pretimeout.
define(`ioctl_wdioc_getpretimeout', `{ 0x5709 }')
# Query the remaining time before the system will reboot.
define(`ioctl_wdioc_gettimeleft', `{ 0x570a }')


## linux/fs.h

# Get extended attributes.
define(`ioctl_fs_ioc_fsgetxattr', `{ 0x581f }')
# Invoke file system batched discard support.
define(`ioctl_fitrim', `{ 0x5879 }')


# Get version information.
define(`ioctl_drm_ioctl_version', `{ 0x6400 }')
# Close gem buffer.
define(`ioctl_drm_ioctl_gem_close', `{ 0x6409 }')
# Get capabilities.
define(`ioctl_drm_ioctl_get_cap', `{ 0x640c }')
# Acquire master (VT switching).
define(`ioctl_drm_ioctl_set_master', `{ 0x641e }')
# Drop master (VT switching).
define(`ioctl_drm_ioctl_drop_master', `{ 0x641f }')
# Convert handle to file descriptor.
define(`ioctl_drm_ioctl_prime_handle_to_fd', `{ 0x642d }')
# Convert file descriptor to handle.
define(`ioctl_drm_ioctl_prime_fd_to_handle', `{ 0x642e }')

# Device specific commands.
define(`ioctl__drm_device_specific_commands', `{ 0x6440-0x6442 0x6444-0x6446 0x6448-0x6450 }')


## linux/ext4.h

# Resize the filesystem to a new size.
define(`ioctl_ext4_ioc_resize_fs', `{ 0x6610 }')


# Get the RTC's time.
define(`ioctl_rtc_rd_time', `{ 0x7009 }')


## linux/media.h

# Enumerate the graph topology and graph element properties.
define(`ioctl_media_ioc_g_topology', `{ 0x7c04 }')


# Get receive timestamp of the last packet passed to the user. (_NEW)
define(`ioctl_siocgstamp', `{ 0x8906 }')
# Get the nanosecond-precise timestamp of the last received packet on the socket.
define(`ioctl_siocgstampns', `{ 0x8907 }')
# Get name of the interface.
define(`ioctl_siocgifname', `{ 0x8910 }')
# Get list of interface (transport layer) addresses.
define(`ioctl_siocgifconf', `{ 0x8912 }')
# Get the active flag word of the device.
define(`ioctl_siocgifflags', `{ 0x8913 }')
# Set the active flag word of the device.
define(`ioctl_siocsifflags', `{ 0x8914 }')
# Get the address of the device.
define(`ioctl_siocgifaddr', `{ 0x8915 }')
# Get the destination address of a point-to-point device.
define(`ioctl_siocgifdstaddr', `{ 0x8917 }')
# Get the broadcast address for a device.
define(`ioctl_siocgifbrdaddr', `{ 0x8919 }')
# Get the network mask for a device.
define(`ioctl_siocgifnetmask', `{ 0x891b }')
# Get the MTU (Maximum Transfer Unit) of a device.
define(`ioctl_siocgifmtu', `{ 0x8921 }')
# Get the hardware address of a device.
define(`ioctl_siocgifhwaddr', `{ 0x8927 }')
# Get interface index.
define(`ioctl_siocgifindex', `{ 0x8933 }')
# Get the transmit queue length of a device.
define(`ioctl_siocgiftxqlen', `{ 0x8942 }')
# Set or get the field ifr_data of type char* in the structure ifreq.
define(`ioctl_siocethtool', `{ 0x8946 }')
# Get contents of MII registers directly from the hardware.
define(`ioctl_siocgmiiphy', `{ 0x8947 }')
# Get socket network namespace.
define(`ioctl_siocgskns', `{ 0x894c }')
# Set an ARP mapping.
define(`ioctl_siocsarp', `{ 0x8955 }')
# Get the interface's hardware parameters.
define(`ioctl_siocgifmap', `{ 0x8970 }')
# Set 802.1Q VLAN options.
define(`ioctl_siocsifvlan', `{ 0x8983 }')
# Get info about bond state.
define(`ioctl_siocbondinfoquery', `{ 0x8994 }')
# Get wireless extension protocol version.
define(`ioctl_siocgiwname', `{ 0x8b01 }')
# Get wireless operation mode.
define(`ioctl_siocgiwmode', `{ 0x8b07 }')
# Get ESSID of the network.
define(`ioctl_siocgiwessd', `{ 0x8b1b }')


# Get autofs dev ioctl version.
define(`ioctl_autofs_dev_ioctl_version', `{ 0x9371 }')
# Get autofs module protocol version.
define(`ioctl_autofs_dev_ioctl_protover', `{ 0x9372 }')
# Get autofs module protocol sub version.
define(`ioctl_autofs_dev_ioctl_protosubver', `{ 0x9373 }')
# Open a file descriptor on an autofs mount point.
define(`ioctl_autofs_dev_ioctl_openmount', `{ 0x9374 }')
# Send "ready" status for an existing wait (either a mount or an expire request).
define(`ioctl_autofs_dev_ioctl_ready', `{ 0x9376 }')
# Send "failed" status for an existing wait.
define(`ioctl_autofs_dev_ioctl_fail', `{ 0x9377 }')
# Set the autofs mount timeout.
define(`ioctl_autofs_dev_ioctl_timeout', `{ 0x937a }')


# Initiate tree search.
define(`ioctl_btrfs_ioc_tree_search', `{ 0x9411 }')
# Check if device is known by the kernel (i.e. ready to mount).
define(`ioctl_btrfs_ioc_devices_ready', `{ 0x9427 }')


# https://www.kernel.org/doc/Documentation/virtual/kvm/api.txt
# Get API version.
define(`ioctl_kvm_get_api_version', `{ 0xae00 }')
# Create a vm handle.
define(`ioctl_kvm_create_vm', `{ 0xae01 }')
# Get the guest msrs that are supported.
define(`ioctl_kvm_get_msr_index_list', `{ 0xae02 }')
# Check if extension is supported.
define(`ioctl_kvm_check_extension', `{ 0xae03 }')
# Get the supported cpuid features.
define(`ioctl_kvm_get_supported_cpuid', `{ 0xae05 }')
# Get the list of MSRs that can be passed to the KVM_GET_MSRS system ioctl.
define(`ioctl_kvm_get_msr_feature_index_list', `{ 0xae0a }')
# Get the number of MSR-based features.
define(`ioctl_kvm_get_msrs', `{ 0xae88 }')
# Get supported MCE capabilities.
define(`ioctl_kvm_x86_get_mce_cap_supported', `{ 0xae9d }')


# Get the version information for the ioctl interface.
define(`ioctl_dm_version', `{ 0xfd00 }')
# Get a list of all the dm device names.
define(`ioctl_dm_list_devices', `{ 0xfd02 }')
# Retrieve the status for the table in the active slot.
define(`ioctl_dm_dev_status', `{ 0xfd07 }')
# Return a set of device dependencies for the active table.
define(`ioctl_dm_table_deps', `{ 0xfd0b }')
# Return the targets status for the active table.
define(`ioctl_dm_table_status', `{ 0xfd0c }')
# Lists all available targets with their version.
define(`ioctl_dm_list_versions', `{ 0xfd0d }')
