########################################
#
# Support macros for sets of object classes and permissions
#
# This file should only have object class and permission set macros - they
# can only reference object classes and/or permissions.


########################################
#
# Macros for sets of classes
#

#
# All directory and file classes
#
define(`dir_file_class_set', `{ dir file_class_set }')

#
# All non-directory file classes.
#
define(`file_class_set', `{ devfile_class_set notdevfile_class_set }')

#
# Non-device file classes.
#
define(`notdevfile_class_set', `{ fifo_file file lnk_file sock_file }')

#
# Device file classes.
#
define(`devfile_class_set', `{ blk_file chr_file }')

#
# All socket classes.
#
define(`socket_class_set', `{ socket key_socket netlink_socket netlink_selinux_socket usual_socket_class_set }')

define(`usual_socket_class_set', `{ tcp_socket udp_socket rawip_socket packet_socket unix_stream_socket unix_dgram_socket appletalk_socket dccp_socket tun_socket sctp_socket icmp_socket ax25_socket ipx_socket netrom_socket atmpvc_socket x25_socket rose_socket decnet_socket atmsvc_socket rds_socket irda_socket pppox_socket llc_socket can_socket tipc_socket bluetooth_socket iucv_socket rxrpc_socket isdn_socket phonet_socket ieee802154_socket caif_socket alg_socket nfc_socket vsock_socket kcm_socket qipcrtr_socket smc_socket xdp_socket mctp_socket netlink_socket_class_set }')

define(`bind_socket_class_set', `{ dccp_socket sctp_socket tcp_socket udp_socket }')
define(`connection_socket_class_set', `{ dccp_socket sctp_socket tcp_socket }')

#
# Netlink socket classes.
#
define(`netlink_socket_class_set', `{ netlink_route_socket netlink_tcpdiag_socket netlink_nflog_socket netlink_xfrm_socket netlink_audit_socket netlink_dnrt_socket netlink_kobject_uevent_socket netlink_iscsi_socket netlink_fib_lookup_socket netlink_connector_socket netlink_netfilter_socket netlink_generic_socket netlink_scsitransport_socket netlink_rdma_socket netlink_crypto_socket }')

# TODO: implement via python test
#neverallow * *:~usual_socket_class_set { bind connect listen accept getopt setopt shutdown recvfrom sendto name_bind };

#
# Datagram socket classes.
#
define(`dgram_socket_class_set', `{ udp_socket unix_dgram_socket }')

#
# Stream socket classes.
#
define(`stream_socket_class_set', `{ tcp_socket unix_stream_socket sctp_socket }')

#
# Unprivileged socket classes (exclude rawip, netlink, packet).
#
define(`unpriv_socket_class_set', `{ tcp_socket udp_socket unix_stream_socket unix_dgram_socket sctp_socket }')

#
# Node bind socket classes.
#
define(`nodebind_socket_class_set', `{ tcp_socket udp_socket rawip_socket dccp_socket sctp_socket icmp_socket }')


########################################
#
# Macros for sets of permissions
#

#
# Permissions to mount and unmount file systems.
#
define(`mount_fs_perms', `{ mount remount unmount getattr }')

#
# Permissions for using sockets.
#
define(`rw_socket_perms', `{ read getattr write setattr append bind connect getopt setopt shutdown }')

#
# Permissions for creating and using sockets.
#
define(`create_socket_perms', `{ create rw_socket_perms }')

#
# Permissions for using stream sockets.
#
define(`rw_stream_socket_perms', `{ rw_socket_perms listen accept }')

#
# Permissions for creating and using stream sockets.
#
define(`create_stream_socket_perms', `{ create_socket_perms listen accept }')

#
# Permissions for creating and using sockets.
#
define(`connected_socket_perms', `{ create read getattr write setattr append bind getopt setopt shutdown }')

#
# Permissions for creating and using sockets.
#
define(`connected_stream_socket_perms', `{ connected_socket_perms listen accept }')

#
# Permissions for using netlink sockets for operations that modify state.
#
define(`rw_netlink_socket_perms', `{ create_socket_perms nlmsg_read nlmsg_write }')

#
# Permissions for using netlink sockets for operations that observe state.
#
define(`r_netlink_socket_perms', `{ create_socket_perms nlmsg_read }')

#
# Permissions for sending all signals.
#
define(`signal_perms', `{ sigchld sigkill sigstop signull signal }')

#
# Permissions for using System V IPC
#
define(`r_sem_perms', `{ associate getattr read unix_read }')
define(`rw_sem_perms', `{ r_sem_perms unix_write write }')
define(`create_sem_perms', `{ create destroy rw_sem_perms setattr }')

define(`r_msgq_perms', `{ associate getattr read unix_read }')
define(`rw_msgq_perms', `{ enqueue r_msgq_perms unix_write write }')
define(`create_msgq_perms', `{ create destroy rw_msgq_perms setattr }')

define(`r_shm_perms', `{ associate getattr read unix_read }')
define(`rw_shm_perms', `{ r_shm_perms unix_write write }')
define(`create_shm_perms', `{ create destroy rw_shm_perms setattr }')

#
# Directory (dir)
#
define(`getattr_dir_perms',`{ getattr }')
define(`setattr_dir_perms',`{ setattr }')
define(`search_dir_perms',`{ getattr search }')
define(`list_dir_perms',`{ getattr search open read lock }')
define(`watch_dir_perms',`{ read watch }')
define(`add_entry_dir_perms',`{ getattr search open lock write add_name }')
define(`del_entry_dir_perms',`{ getattr search open lock write remove_name }')
define(`rw_dir_perms', `{ open read getattr lock search add_name remove_name write }')
define(`create_dir_perms',`{ getattr create }')
define(`rename_dir_perms',`{ getattr rename }')
define(`delete_dir_perms',`{ getattr rmdir }')
define(`manage_dir_perms',`{ create open getattr setattr read write link rename search add_name remove_name reparent rmdir lock }')
define(`relabelfrom_dir_perms',`{ getattr relabelfrom }')
define(`relabelto_dir_perms',`{ getattr relabelto }')
define(`relabel_dir_perms',`{ getattr relabelfrom relabelto }')

#
# Regular file (file)
#
define(`getattr_file_perms',`{ getattr }')
define(`setattr_file_perms',`{ setattr }')
define(`read_inherited_file_perms',`{ getattr read lock ioctl }')
define(`read_file_perms',`{ getattr open read lock ioctl }')
define(`mmap_read_inherited_file_perms',`{ getattr map read ioctl }')
define(`mmap_read_file_perms',`{ map read_file_perms }')
define(`mmap_exec_inherited_file_perms',`{ getattr map read execute ioctl }')
define(`mmap_exec_file_perms',`{ getattr open map read execute ioctl }')
define(`exec_file_perms',`{ getattr open map read execute ioctl execute_no_trans }')
define(`append_file_perms',`{ getattr open append lock ioctl }')
define(`write_inherited_file_perms',`{ getattr write append lock ioctl }')
define(`write_file_perms',`{ getattr open write append lock ioctl }')
define(`rw_inherited_file_perms',`{ getattr read write append ioctl lock }')
define(`rw_file_perms',`{ open rw_inherited_file_perms }')
define(`mmap_rw_inherited_file_perms',`{ getattr map read write ioctl }')
define(`mmap_rw_file_perms',`{ getattr open map read write ioctl lock }')
define(`create_file_perms',`{ getattr create open }')
define(`rename_file_perms',`{ getattr rename }')
define(`delete_file_perms',`{ getattr unlink }')
define(`manage_file_perms',`{ create open getattr setattr read write map append rename link unlink ioctl lock }')
define(`relabelfrom_file_perms',`{ getattr relabelfrom }')
define(`relabelto_file_perms',`{ getattr relabelto }')
define(`relabel_file_perms',`{ getattr relabelfrom relabelto }')
define(`logging_file_perms', `{ append_file_perms create setattr }')

#
# Symbolic link (lnk_file)
#
define(`getattr_lnk_file_perms',`{ getattr }')
define(`setattr_lnk_file_perms',`{ setattr }')
define(`read_lnk_file_perms',`{ getattr read }')
define(`create_lnk_file_perms',`{ create getattr }')
define(`rename_lnk_file_perms',`{ getattr rename }')
define(`delete_lnk_file_perms',`{ getattr unlink }')
define(`manage_lnk_file_perms',`{ create read getattr setattr link unlink rename }')
define(`relabelfrom_lnk_file_perms',`{ getattr relabelfrom }')
define(`relabelto_lnk_file_perms',`{ getattr relabelto }')
define(`relabel_lnk_file_perms',`{ getattr relabelfrom relabelto }')

#
# (Un)named Pipes/FIFOs (fifo_file)
#
define(`getattr_fifo_file_perms',`{ getattr }')
define(`setattr_fifo_file_perms',`{ setattr }')
define(`read_inherited_fifo_file_perms',`{ getattr read ioctl }')
define(`read_fifo_file_perms',`{ getattr open read ioctl }')
define(`append_fifo_file_perms',`{ getattr open append ioctl }')
define(`write_fifo_file_perms',`{ getattr open write append ioctl }')
define(`rw_inherited_fifo_file_perms',`{ getattr read write append ioctl }')
define(`rw_fifo_file_perms',`{ open rw_inherited_fifo_file_perms }')
define(`create_fifo_file_perms',`{ getattr create open }')
define(`rename_fifo_file_perms',`{ getattr rename }')
define(`delete_fifo_file_perms',`{ getattr unlink }')
define(`manage_fifo_file_perms',`{ create open getattr setattr read write append rename link unlink ioctl }')
define(`relabelfrom_fifo_file_perms',`{ getattr relabelfrom }')
define(`relabelto_fifo_file_perms',`{ getattr relabelto }')
define(`relabel_fifo_file_perms',`{ getattr relabelfrom relabelto }')

#
# (Un)named Sockets (sock_file)
#
define(`getattr_sock_file_perms',`{ getattr }')
define(`setattr_sock_file_perms',`{ setattr }')
define(`read_sock_file_perms',`{ getattr open read }')
define(`write_sock_file_perms',`{ getattr write open append }')
define(`rw_sock_file_perms',`{ getattr open read write append }')
define(`create_sock_file_perms',`{ getattr create open }')
define(`rename_sock_file_perms',`{ getattr rename }')
define(`delete_sock_file_perms',`{ getattr unlink }')
define(`manage_sock_file_perms',`{ create open getattr setattr read write rename link unlink append }')
define(`relabelfrom_sock_file_perms',`{ getattr relabelfrom }')
define(`relabelto_sock_file_perms',`{ getattr relabelto }')
define(`relabel_sock_file_perms',`{ getattr relabelfrom relabelto }')

#
# Block device nodes (blk_file)
#
define(`getattr_blk_file_perms',`{ getattr }')
define(`setattr_blk_file_perms',`{ setattr }')
define(`read_blk_file_perms',`{ getattr open read lock }')
define(`write_blk_file_perms',`{ getattr open write lock }')
define(`rw_blk_file_perms',`{ getattr open read write lock }')
define(`create_blk_file_perms',`{ getattr create }')
define(`rename_blk_file_perms',`{ getattr rename }')
define(`delete_blk_file_perms',`{ getattr unlink }')
define(`manage_blk_file_perms',`{ create open getattr setattr read write rename link unlink lock }')
define(`relabelfrom_blk_file_perms',`{ getattr relabelfrom }')
define(`relabelto_blk_file_perms',`{ getattr relabelto }')
define(`relabel_blk_file_perms',`{ getattr relabelfrom relabelto }')

#
# Character device nodes (chr_file)
#
define(`getattr_chr_file_perms',`{ getattr }')
define(`setattr_chr_file_perms',`{ setattr }')
define(`read_chr_file_perms',`{ getattr open read }')
define(`append_chr_file_perms',`{ getattr open append }')
define(`write_chr_file_perms',`{ getattr open write append }')
define(`rw_inherited_chr_file_perms', `{ getattr read write append }')
define(`rw_chr_file_perms',`{ getattr open read write append }')
define(`create_chr_file_perms',`{ getattr create }')
define(`rename_chr_file_perms',`{ getattr rename }')
define(`delete_chr_file_perms',`{ getattr unlink }')
define(`manage_chr_file_perms',`{ create open getattr setattr read write append rename link unlink }')
define(`relabelfrom_chr_file_perms',`{ getattr relabelfrom }')
define(`relabelto_chr_file_perms',`{ getattr relabelto }')
define(`relabel_chr_file_perms',`{ getattr relabelfrom relabelto }')

#
# Keys
#
define(`manage_key_perms', `{ create link read search setattr view write } ')
