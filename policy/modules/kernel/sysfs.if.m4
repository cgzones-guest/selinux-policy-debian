########################################
#
# Sysfs Interface generated macros
#
########################################

define(`create_sysfs_type_interfaces',``
########################################
## <summary>
##	Search the $1 hardware state information.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`sysfs_search_$1',`
	gen_require(`
		type $1_sysfs_t;
	')

	sysfs_search_generic(dollarsone)
	$2
	allow dollarsone $1_sysfs_t:dir search_dir_perms;
')

########################################
## <summary>
##	List the $1 hardware state information.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`sysfs_list_$1',`
	gen_require(`
		type $1_sysfs_t;
	')

	sysfs_search_generic(dollarsone)
	$2
	allow dollarsone $1_sysfs_t:dir list_dir_perms;
')

########################################
## <summary>
##	Watch the $1 hardware state information directories.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`sysfs_watch_$1_dirs',`
	gen_require(`
		type $1_sysfs_t;
	')

	sysfs_search_generic(dollarsone)
	$2
	allow dollarsone $1_sysfs_t:dir watch_dir_perms;
')

########################################
## <summary>
##	Get the attributes of $1 hardware state information.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`sysfs_getattr_$1',`
	gen_require(`
		type $1_sysfs_t;
	')

	sysfs_search_generic(dollarsone)
	$2
	allow dollarsone $1_sysfs_t:dir list_dir_perms;
	allow dollarsone $1_sysfs_t:file getattr;
	allow dollarsone $1_sysfs_t:lnk_file read_lnk_file_perms;
')

########################################
## <summary>
##	Read the $1 hardware state information.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`sysfs_read_$1',`
	gen_require(`
		type $1_sysfs_t;
	')

	sysfs_search_generic(dollarsone)
	$2
	allow dollarsone $1_sysfs_t:dir list_dir_perms;
	allow dollarsone $1_sysfs_t:file read_file_perms;
	allow dollarsone $1_sysfs_t:lnk_file read_lnk_file_perms;
')

########################################
## <summary>
##	Do not audit reading the $1 hardware state information.
## </summary>
## <param name="domain">
##	<summary>
##	Domain to not audit.
##	</summary>
## </param>
#
interface(`sysfs_dontaudit_read_$1',`
	gen_require(`
		type $1_sysfs_t;
	')

	dontaudit dollarsone $1_sysfs_t:dir list_dir_perms;
	dontaudit dollarsone $1_sysfs_t:file read_file_perms;
	dontaudit dollarsone $1_sysfs_t:lnk_file read_lnk_file_perms;
')

########################################
## <summary>
##	Read and mmap the $1 hardware state information.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`sysfs_mapread_$1',`
	gen_require(`
		type $1_sysfs_t;
	')

	sysfs_search_generic(dollarsone)
	$2
	allow dollarsone $1_sysfs_t:dir list_dir_perms;
	allow dollarsone $1_sysfs_t:file mmap_read_file_perms;
	allow dollarsone $1_sysfs_t:lnk_file read_lnk_file_perms;
')

########################################
## <summary>
##	Check write access on $1 hardware state information directories (DAC wise).
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`sysfs_check_write_$1_dirs',`
	gen_require(`
		type $1_sysfs_t;
	')

	sysfs_search_generic(dollarsone)
	$2
	can_check_write_dir(dollarsone, $1_sysfs_t)
')

########################################
## <summary>
##	Mount on $1 hardware state information directories.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`sysfs_mounton_$1_dirs',`
	gen_require(`
		type $1_sysfs_t;
	')

	sysfs_search_generic(dollarsone)
	$2
	allow dollarsone $1_sysfs_t:dir { getattr mounton };
')

########################################
## <summary>
##	Write the $1 hardware state information.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`sysfs_write_$1',`
	gen_require(`
		type $1_sysfs_t;
	')

	sysfs_search_generic(dollarsone)
	$2
	allow dollarsone $1_sysfs_t:dir list_dir_perms;
	allow dollarsone $1_sysfs_t:file write_file_perms;
	allow dollarsone $1_sysfs_t:lnk_file read_lnk_file_perms;
')

########################################
## <summary>
##	Read and write the $1 hardware state information.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`sysfs_rw_$1',`
	gen_require(`
		type $1_sysfs_t;
	')

	sysfs_search_generic(dollarsone)
	$2
	allow dollarsone $1_sysfs_t:dir list_dir_perms;
	allow dollarsone $1_sysfs_t:file rw_file_perms;
	allow dollarsone $1_sysfs_t:lnk_file read_lnk_file_perms;
')

########################################
## <summary>
##	Create, read, write and unlink the $1 hardware state information files.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`sysfs_manage_$1_files',`
	gen_require(`
		type $1_sysfs_t;
	')

	sysfs_search_generic(dollarsone)
	$2
	allow dollarsone $1_sysfs_t:dir rw_dir_perms;
	allow dollarsone $1_sysfs_t:file manage_file_perms;
	allow dollarsone $1_sysfs_t:lnk_file read_lnk_file_perms;
')

########################################
## <summary>
##	Dontaudit search the $1 hardware state information.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`sysfs_dontaudit_search_$1',`
	gen_require(`
		type $1_sysfs_t;
	')

	allow dollarsone $1_sysfs_t:dir search_dir_perms;
')
'') dnl end create_sysfs_type_interfaces



#
# sysfs_detail(name, path, [,dependencies [,...]])
#
define(`sysfs_detail',`
create_sysfs_type_interfaces($1, create_deps(decr(decr($#)), shift(shift($*))))
')

define(`create_deps',`dnl
ifelse(`$1', `0', ,`dnl
	gen_require(``
		type $2_sysfs_t;
	'')
	allow dollarsone $2_sysfs_t:dir search_dir_perms;
	allow dollarsone $2_sysfs_t:lnk_file read_lnk_file_perms;
create_deps(decr($1), shift(shift($*)))dnl
')dnl
')dnl
