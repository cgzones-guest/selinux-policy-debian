policy_module(kernel)

########################################
#
# Declarations
#

## <desc>
##	<p>
##	Disable kernel module loading.
##	</p>
## </desc>
gen_bool(secure_mode_insmod, false)

## <desc>
##	<p>
##	Allow all modules to be loaded, e.g. use no allow-list.
##	</p>
## </desc>
gen_bool(permit_all_kernel_modules, false)


# assertion related attributes
attribute can_load_kernmodule;
attribute can_receive_kernel_messages;
attribute can_dump_kernel;

neverallow ~can_load_kernmodule self:capability sys_module;
neverallow ~can_load_kernmodule *:system module_load;

# anonymous inodes
attribute anoninode;
attribute iouring_anoninode;

# regular entries in proc
attribute proc_type;

# sysctls
attribute sysctl_type;

role system_r;

#
# kernel_t is the domain of kernel threads.
# It is also the target type when checking permissions in the system class.
# It is not associated with the domain attribute to not be included in
# domain_*_all_domains() interfaces
type kernel_t; #, can_load_kernmodule;
role system_r types kernel_t;
sid kernel gen_context(system_u:system_r:kernel_t,mls_systemhigh)
neverallow kernel_t *:file execute_no_trans;

# do not modify proc data
neverallow * kernel_t:dir ~list_dir_perms;
neverallow * kernel_t:lnk_file ~read_lnk_file_perms;
neverallow * kernel_t:file ~read_file_perms;

#
# init_t is the domain for userland processes started before the initial
# policy load.
type init_t;
domain_type(init_t)
role system_r types init_t;
sid init gen_context(system_u:system_r:init_t,s0)


#
# DebugFS
#

type debugfs_t;
fs_xattr_type(debugfs_t)
genfscon debugfs / gen_context(system_u:object_r:debugfs_t,s0)

#
# kvmFS
#

type kvmfs_t;
fs_kernfs_type(kvmfs_t)
genfscon kvmfs / gen_context(system_u:object_r:kvmfs_t,s0)

#
# Procfs types
#

type proc_t, proc_type;
fs_kernfs_type(proc_t)
genfscon proc / gen_context(system_u:object_r:proc_t,s0)

type proc_asound_t, proc_type;
genfscon proc /asound gen_context(system_u:object_r:proc_asound_t,s0)

type proc_afs_t, proc_type;
genfscon proc /fs/openafs gen_context(system_u:object_r:proc_afs_t,s0)

type proc_cmdline_t, proc_type;
genfscon proc /cmdline gen_context(system_u:object_r:proc_cmdline_t,s0)

type proc_info_t, proc_type;
genfscon proc /cgroups gen_context(system_u:object_r:proc_info_t,s0)
genfscon proc /cpuinfo gen_context(system_u:object_r:proc_info_t,s0)
genfscon proc /devices gen_context(system_u:object_r:proc_info_t,s0)
genfscon proc /diskstats gen_context(system_u:object_r:proc_info_t,s0)
genfscon proc /filesystems gen_context(system_u:object_r:proc_info_t,s0)
genfscon proc /loadavg gen_context(system_u:object_r:proc_info_t,s0)
genfscon proc /meminfo gen_context(system_u:object_r:proc_info_t,s0)
genfscon proc /pressure gen_context(system_u:object_r:proc_info_t,s0)
genfscon proc /stat gen_context(system_u:object_r:proc_info_t,s0)
genfscon proc /swaps gen_context(system_u:object_r:proc_info_t,s0)
genfscon proc /tty/drivers gen_context(system_u:object_r:proc_info_t,s0)
genfscon proc /uptime gen_context(system_u:object_r:proc_info_t,s0)

# kernel message interface
type proc_kmsg_t, proc_type;
genfscon proc /kmsg gen_context(system_u:object_r:proc_kmsg_t,mls_systemhigh)
neverallow ~can_receive_kernel_messages proc_kmsg_t:file ~getattr;

# /proc kcore: inaccessible
type proc_kcore_t, proc_type;
neverallow ~can_dump_kernel proc_kcore_t:file ~getattr;
genfscon proc /kcore gen_context(system_u:object_r:proc_kcore_t,mls_systemhigh)

type proc_mdstat_t, proc_type;
genfscon proc /mdstat gen_context(system_u:object_r:proc_mdstat_t,s0)

type proc_modules_t, proc_type;
genfscon proc /modules gen_context(system_u:object_r:proc_modules_t,s0)

type proc_mtrr_t, proc_type;
genfscon proc /mtrr gen_context(system_u:object_r:proc_mtrr_t,s0)

type proc_net_t, proc_type;
genfscon proc /net gen_context(system_u:object_r:proc_net_t,s0)

type proc_sysvipc_t, proc_type;
genfscon proc /sysvipc gen_context(system_u:object_r:proc_sysvipc_t,s0)

type proc_xen_t, proc_type;
files_mountpoint(proc_xen_t)
genfscon proc /xen gen_context(system_u:object_r:proc_xen_t,s0)

#
# Sysctl types
#

# /proc/sys directory, base directory of sysctls
type sysctl_t, sysctl_type;
files_mountpoint(sysctl_t)
genfscon proc /sys gen_context(system_u:object_r:sysctl_t,s0)

# /proc/irq directory and files
type sysctl_irq_t, sysctl_type;
genfscon proc /irq gen_context(system_u:object_r:sysctl_irq_t,s0)

# /proc/net/rpc directory and files
type sysctl_rpc_t, sysctl_type;
genfscon proc /net/rpc gen_context(system_u:object_r:sysctl_rpc_t,s0)

# /proc/sys/crypto directory and files
type sysctl_crypto_t, sysctl_type;
genfscon proc /sys/crypto gen_context(system_u:object_r:sysctl_crypto_t,s0)

# public /proc/sys/crypto files
type sysctl_crypto_public_t, sysctl_type;
genfscon proc /sys/crypto/fips_enabled gen_context(system_u:object_r:sysctl_crypto_public_t,s0)

# /proc/sys/fs directory and files
type sysctl_fs_t, sysctl_type;
genfscon proc /sys/fs gen_context(system_u:object_r:sysctl_fs_t,s0)

# public /proc/sys/fs files
type sysctl_fs_public_t, sysctl_type;
genfscon proc /sys/fs/inotify/max_user_watches gen_context(system_u:object_r:sysctl_fs_public_t,s0)
genfscon proc /sys/fs/nr_open gen_context(system_u:object_r:sysctl_fs_public_t,s0)

# /proc/sys/kernel directory and files
type sysctl_kernel_t, sysctl_type;
genfscon proc /sys/kernel gen_context(system_u:object_r:sysctl_kernel_t,s0)

# public /proc/sys/kernel files
type sysctl_kernel_public_t, sysctl_type;
genfscon proc /sys/kernel/cap_last_cap		gen_context(system_u:object_r:sysctl_kernel_public_t,s0)
genfscon proc /sys/kernel/core_pattern		gen_context(system_u:object_r:sysctl_kernel_public_t,s0)
genfscon proc /sys/kernel/core_uses_pid		gen_context(system_u:object_r:sysctl_kernel_public_t,s0)
genfscon proc /sys/kernel/hostname		gen_context(system_u:object_r:sysctl_kernel_public_t,s0)
genfscon proc /sys/kernel/ngroups_max		gen_context(system_u:object_r:sysctl_kernel_public_t,s0)
genfscon proc /sys/kernel/osrelease		gen_context(system_u:object_r:sysctl_kernel_public_t,s0)
genfscon proc /sys/kernel/overflowgid		gen_context(system_u:object_r:sysctl_kernel_public_t,s0)
genfscon proc /sys/kernel/overflowuid		gen_context(system_u:object_r:sysctl_kernel_public_t,s0)
genfscon proc /sys/kernel/pid_max		gen_context(system_u:object_r:sysctl_kernel_public_t,s0)
genfscon proc /sys/kernel/random/boot_id	gen_context(system_u:object_r:sysctl_kernel_public_t,s0)
genfscon proc /sys/kernel/random/entropy_avail	gen_context(system_u:object_r:sysctl_kernel_public_t,s0)
genfscon proc /sys/kernel/random/poolsize	gen_context(system_u:object_r:sysctl_kernel_public_t,s0)
genfscon proc /sys/kernel/random/uuid		gen_context(system_u:object_r:sysctl_kernel_public_t,s0)
genfscon proc /sys/kernel/seccomp/actions_avail	gen_context(system_u:object_r:sysctl_kernel_public_t,s0)
genfscon proc /sys/kernel/threads-max		gen_context(system_u:object_r:sysctl_kernel_public_t,s0)

# /proc/sys/kernel/modprobe file
type sysctl_modprobe_t, sysctl_type;
genfscon proc /sys/kernel/modprobe gen_context(system_u:object_r:sysctl_modprobe_t,s0)

# /proc/sys/kernel/hotplug file
type sysctl_hotplug_t, sysctl_type;
genfscon proc /sys/kernel/hotplug gen_context(system_u:object_r:sysctl_hotplug_t,s0)

# /proc/sys/kernel/yama/ptrace_scope
type sysctl_yama_t, sysctl_type;
genfscon proc /sys/kernel/yama/ptrace_scope gen_context(system_u:object_r:sysctl_yama_t,s0)

# /proc/sys/net directory and files
type sysctl_net_t, sysctl_type;
genfscon proc /sys/net gen_context(system_u:object_r:sysctl_net_t,s0)

# public /proc/sys/net files
type sysctl_net_public_t, sysctl_type;
genfscon proc /sys/net/core/somaxconn			gen_context(system_u:object_r:sysctl_net_public_t,s0)
genfscon proc /sys/net/ipv6/bindv6only			gen_context(system_u:object_r:sysctl_net_public_t,s0)
genfscon proc /sys/net/ipv6/conf/all/disable_ipv6	gen_context(system_u:object_r:sysctl_net_public_t,s0)
genfscon proc /sys/net/netfilter/nf_conntrack_max	gen_context(system_u:object_r:sysctl_net_public_t,s0)
genfscon proc /sys/net/nf_conntrack_max			gen_context(system_u:object_r:sysctl_net_public_t,s0)

# /proc/sys/net/unix directory and files
type sysctl_net_unix_t, sysctl_type;
genfscon proc /sys/net/unix gen_context(system_u:object_r:sysctl_net_unix_t,s0)

# /proc/sys/vm directory and files
type sysctl_vm_t, sysctl_type;
genfscon proc /sys/vm gen_context(system_u:object_r:sysctl_vm_t,s0)

# public /proc/sys/vm files
type sysctl_vm_public_t, sysctl_type;
genfscon proc /sys/vm/max_map_count	gen_context(system_u:object_r:sysctl_vm_public_t,s0)

type sysctl_vm_overcommit_t, sysctl_type;
genfscon proc /sys/vm/overcommit_memory gen_context(system_u:object_r:sysctl_vm_overcommit_t,s0)

# /proc/sys/dev directory and files
type sysctl_dev_t, sysctl_type;
genfscon proc /sys/dev gen_context(system_u:object_r:sysctl_dev_t,s0)

type sysctl_dev_public_t, sysctl_type;
genfscon proc /sys/dev/i915/perf_stream_paranoid gen_context(system_u:object_r:sysctl_dev_public_t,s0)

# /proc/sys/dev/parport directory and files
type sysctl_parport_t, sysctl_type;
genfscon proc /sys/dev/parport gen_context(system_u:object_r:sysctl_parport_t,s0)

#
# invalid_t is the type for objects which labels have been invalidated
# by the currently loaded policy.
#
type invalid_t;
fs_associate_xattr(invalid_t)
sid file gen_context(system_u:object_r:invalid_t,mls_systemhigh)
neverallow * invalid_t:file entrypoint;
neverallow * invalid_t:{ dir_file_class_set anon_inode filesystem socket_class_set } relabelto;

#
# unlabeled_t is the type of unlabeled objects.
# Objects that have no known labeling information are treated as having
# this type.
#
# Mountpoint permissions are for the case when a file has been assigned
# an extended attribute for the first time.
# Directories where filesystems are mounted may never get relabeled.
#
type unlabeled_t;
sid unlabeled gen_context(system_u:object_r:unlabeled_t,s0)
neverallow unlabeled_t *:process *;
neverallow * unlabeled_t:process *;
neverallow * unlabeled_t:file entrypoint;
neverallow * unlabeled_t:{ dir_file_class_set anon_inode filesystem socket_class_set } relabelto;

# These initial sids are no longer used, and can be removed:
sid any_socket		gen_context(system_u:object_r:unlabeled_t,mls_systemhigh)
sid file_labels		gen_context(system_u:object_r:unlabeled_t,mls_systemhigh)
sid fs			gen_context(system_u:object_r:fs_t,mls_systemhigh)
sid icmp_socket		gen_context(system_u:object_r:unlabeled_t,mls_systemhigh)
sid igmp_packet		gen_context(system_u:object_r:unlabeled_t,mls_systemhigh)
sid kmod		gen_context(system_u:object_r:unlabeled_t,mls_systemhigh)
sid policy		gen_context(system_u:object_r:unlabeled_t,mls_systemhigh)
sid scmp_packet		gen_context(system_u:object_r:unlabeled_t,mls_systemhigh)
sid sysctl		gen_context(system_u:object_r:sysctl_t,mls_systemhigh)
sid sysctl_modprobe	gen_context(system_u:object_r:unlabeled_t,mls_systemhigh)
sid sysctl_fs		gen_context(system_u:object_r:unlabeled_t,mls_systemhigh)
sid sysctl_kernel	gen_context(system_u:object_r:unlabeled_t,mls_systemhigh)
sid sysctl_net		gen_context(system_u:object_r:unlabeled_t,mls_systemhigh)
sid sysctl_net_unix	gen_context(system_u:object_r:unlabeled_t,mls_systemhigh)
sid sysctl_vm		gen_context(system_u:object_r:unlabeled_t,mls_systemhigh)
sid sysctl_dev		gen_context(system_u:object_r:unlabeled_t,mls_systemhigh)
sid tcp_socket		gen_context(system_u:object_r:unlabeled_t,mls_systemhigh)

#
# rootfs_t is the type for rootfs filesystem.
#
type rootfs_t;
fs_xattr_type(rootfs_t)
genfscon rootfs / gen_context(system_u:object_r:rootfs_t,s0)

########################################
#
# Policy for kernel threads
#

allow kernel_t self:process fork;

#allow kernel_t self:capability { audit_control audit_write chown dac_override dac_read_search fowner fsetid ipc_lock ipc_owner kill lease linux_immutable mknod net_admin net_bind_service net_broadcast net_raw setfcap setgid setpcap setuid sys_admin sys_boot sys_chroot sys_nice sys_pacct sys_ptrace sys_rawio sys_resource sys_time sys_tty_config };
#allow kernel_t self:capability2 perfmon;
#allow kernel_t self:process { fork getattr getcap getpgid getrlimit getsched getsession noatsecure rlimitinh setcap setkeycreate setpgid setsched setsockcreate share siginh signal_perms };
#allow kernel_t self:dir list_dir_perms;
#allow kernel_t self:lnk_file read_lnk_file_perms;
#allow kernel_t self:file rw_file_perms;
#allow kernel_t self:shm create_shm_perms;
#allow kernel_t self:sem create_sem_perms;
#allow kernel_t self:msg { receive send };
#allow kernel_t self:msgq create_msgq_perms;
#allow kernel_t self:unix_dgram_socket create_socket_perms;
#allow kernel_t self:unix_stream_socket create_stream_socket_perms;
#allow kernel_t self:unix_dgram_socket sendto;
#allow kernel_t self:unix_stream_socket connectto;
#allow kernel_t self:fifo_file rw_fifo_file_perms;
#allow kernel_t self:sock_file read_sock_file_perms;
#allow kernel_t self:fd use;
#allow kernel_t self:perf_event cpu;
# cfg80211
allow kernel_t self:key search;
# cryptomgr_test
allow kernel_t self:system module_request;

allow kernel_t debugfs_t:dir search_dir_perms;

#allow kernel_t proc_t:dir { list_dir_perms mounton };
#allow kernel_t proc_t:file read_file_perms;
#allow kernel_t proc_t:lnk_file read_lnk_file_perms;

#allow kernel_t proc_net_t:dir list_dir_perms;
#allow kernel_t proc_net_t:file read_file_perms;

#allow kernel_t proc_mdstat_t:file read_file_perms;

#allow kernel_t proc_kcore_t:file getattr;

#allow kernel_t proc_kmsg_t:file getattr;

#allow kernel_t sysctl_kernel_t:dir list_dir_perms;
#allow kernel_t sysctl_kernel_t:file read_file_perms;
#allow kernel_t sysctl_t:dir list_dir_perms;

# Kernel-generated traffic e.g., TCP resets on
# connections with invalidated labels:
allow kernel_t unlabeled_t:packet send;


corecmd_search_bin(kernel_t)


#corenet_all_recvfrom_unlabeled(kernel_t)
#corenet_all_recvfrom_netlabel(kernel_t)
# Kernel-generated traffic e.g., ICMP and TCP replies:
corenet_send_all_nodes(kernel_t)
corenet_send_all_if(kernel_t)
corenet_send_all_packets(kernel_t)

#corenet_relabelto_all_packets(kernel_t)

#corenet_ib_access_all_pkeys(kernel_t)
#corenet_ib_access_unlabeled_pkeys(kernel_t)
#corenet_ib_manage_subnet_all_endports(kernel_t)
#corenet_ib_manage_subnet_unlabeled_endports(kernel_t)


#dev_search_usbfs(kernel_t)
# devtmpfs handling:
dev_setattr_all_chr_files(kernel_t)
dev_delete_all_chr_files(kernel_t)
dev_create_generic_dirs(kernel_t)
dev_delete_generic_dirs(kernel_t)
dev_create_generic_blk_files(kernel_t)
dev_delete_generic_blk_files(kernel_t)
dev_setattr_generic_blk_files(kernel_t)
dev_create_generic_chr_files(kernel_t)
dev_delete_generic_chr_files(kernel_t)
#dev_rw_generic_chr_files(kernel_t)
dev_setattr_generic_chr_files(kernel_t)
#dev_delete_generic_symlinks(kernel_t)
#dev_mounton_dirs(kernel_t)
#dev_getattr_fs(kernel_t)
#dev_write_kmsg(kernel_t)
dev_setup_vsock(kernel_t)


#domain_signal_all_domains(kernel_t)
#domain_search_all_domains_state(kernel_t)


files_search_root(kernel_t)
#files_list_root(kernel_t)
#files_list_etc(kernel_t)
#files_list_home(kernel_t)
#files_read_usr_files__todo(kernel_t)


# Mount root file system. Used when loading a policy
# from initrd, then mounting the root filesystem
#fs_mount_all_fs(kernel_t)
#fs_unmount_all_fs(kernel_t)

#fs_getattr_tmpfs(kernel_t)


libs_use_ld_so(kernel_t)
# /usr/lib/firmware/brcm/brcmfmac43430-sdio.bin
# /usr/lib/firmware/cypress/cyfmac43430-sdio.bin
libs_read_files(kernel_t)


#logging_set_audit_parameters(kernel_t)
#logging_send_syslog_msg(kernel_t)


# kworker: /etc/alternatives/regulatory.db
miscfiles_read_alternatives(kernel_t)


# cfg80211
modutils_search_key(kernel_t)


#mount_use_fds(kernel_t)
#mount_read_loopback_files(kernel_t)


#selinux_getattr_fs(kernel_t)
#selinux_load_policy(kernel_t)
#selinux_compute_create_context(kernel_t)


#seutil_read_config(kernel_t)
#seutil_read_bin_policy(kernel_t)
#seutil_domtrans_setfiles(kernel_t)


# kdevtmpfs removable device handling
storage_setattr_fixed_disk_dev(kernel_t)
storage_delete_fixed_disk_dev(kernel_t)

storage_create_loop_dev(kernel_t)
storage_setattr_loop_dev(kernel_t)
storage_delete_loop_dev(kernel_t)
storage_dev_filetrans_loop_dev(kernel_t)

#sysfs_mounton_generic_dir(kernel_t)
#sysfs_read_all(kernel_t)
#sysfs_getattr_fs(kernel_t)


#term_getattr_pty_fs(kernel_t)
#term_use_console(kernel_t)
# for kdevtmpfs
#term_setattr_unallocated_ttys(kernel_t)
#term_unlink_unallocated_ttys(kernel_t)


# cfg80211
udev_search_key(kernel_t)


#optional_policy(`
#	# loop devices
#	fstools_use_fds(kernel_t)
#')


########################################
#
# Policy for userspace processes started before the initial policy load.
#

# systemd computing its context
selinux_compute_create_context(init_t)


########################################
#
# Invalid process local policy
#

# If you load a new policy that removes active domains, processes can
# get stuck if you do not allow unlabeled processes to signal init.
# If you load an incompatible policy, you should probably reboot,
# since you may have compromised system security.
systemd_sigchld(invalid_t)


########################################
#
# Unlabeled process local policy
#

# Allow unlabeled network traffic
allow unlabeled_t self:packet { forward_in forward_out };

# [node recvfrom] and [netif ingress] use the peer label of the remote peer
# (not the receiving task on the local system) as subject.
corenet_receive_all_nodes(unlabeled_t)
corenet_receive_all_if(unlabeled_t)
# VPN traffic
corenet_send_stdextern_if(unlabeled_t)
corenet_send_container_if(unlabeled_t)
corenet_send_wireguard_if(unlabeled_t)
corenet_send_container_node(unlabeled_t)
corenet_send_generic_node(unlabeled_t)
corenet_forward_ipsecnat_server_packets(unlabeled_t)


########################################
#
# Kernel module loading policy
#

if( ! secure_mode_insmod ) {
	allow can_load_kernmodule self:capability sys_module;
	auditallow can_load_kernmodule self:capability sys_module;

	# load_module() calls stop_machine() which
	# calls sched_setscheduler()
	allow can_load_kernmodule self:capability sys_nice;
	kernel_setsched(can_load_kernmodule)

	# verify modules are signed in lockdown mode
	kernel_search_key(can_load_kernmodule)

	modutils_load_permitted_kernel_modules(can_load_kernmodule)
}

if( ! secure_mode_insmod && permit_all_kernel_modules ) {
	modutils_load_all_kernel_modules(can_load_kernmodule)
}
