policy_module(files)

########################################
#
# Declarations
#

attribute base_file_type;
attribute base_ro_file_type;
# TODO: enable neverallows to constrain modifying base files
#neverallow ~can_write_base_file base_file_type:file ~read_file_perms;
#neverallow ~can_write_base_file base_file_type:dir ~rw_dir_perms;
#neverallow ~can_write_base_ro_file base_ro_file_type:file ~read_file_perms;
#neverallow ~can_write_base_ro_file base_ro_file_type:dir ~list_dir_perms;

attribute file_type;

attribute cachefile;
attribute configfile;
attribute hugetlbfile;
attribute lockfile;
attribute mountpoint;
attribute runtimefile;
attribute shmfsfile;
attribute spoolfile;
attribute statefile;

attribute containerfile;


# sensitive security files whose accesses should
# not be dontaudited for uses
attribute security_file_type;
# and its opposite
attribute non_security_file_type;

# sensitive authentication files whose accesses should
# not be dontaudited for uses
attribute auth_file_type;
# and its opposite
attribute non_auth_file_type;

attribute tmpfile;
attribute tmpentryfile;
attribute tmpexecfile;
attribute tmpfsfile;
attribute tmpfsexecfile;

#
# boot_t is the type for files in /boot
#
type boot_t;
files_ro_base_file(boot_t)
files_mountpoint(boot_t)

# default_t is the default type for files that do not
# match any specification in the file_contexts configuration
# other than the generic /.* specification.
type default_t;
files_base_file(default_t)

#
# etc_t is the type of the system etc directories.
#
type etc_t, configfile;
files_ro_base_file(etc_t)

#
# etc_runtime_t is the type of various
# files in /etc that are automatically
# generated during initialization.
#
type etc_runtime_t;
files_base_file(etc_runtime_t)

#
# home_root_t is the type for the directory where user home directories
# are created
#
type home_root_t;
files_base_file(home_root_t)
files_mountpoint(home_root_t)

#
# lost_found_t is the type for the lost+found directories.
#
type lost_found_t;
files_base_file(lost_found_t)

#
# mnt_t is the type for mount points such as /mnt/cdrom
#
type mnt_t;
files_base_file(mnt_t)
files_mountpoint(mnt_t)

type no_access_t;
files_type(no_access_t)

type readable_t;
files_type(readable_t)

#
# root_t is the type for the root directory.
#
type root_t;
files_base_file(root_t)
files_mountpoint(root_t)

#
# src_t is the type of files in the system src directories.
#
type src_t;
files_ro_base_file(src_t)
files_mountpoint(src_t)

#
# system_map_t is for the system.map files in /boot
#
type system_map_t;
files_type(system_map_t)
files_mountpoint(system_map_t)
genfscon proc /kallsyms gen_context(system_u:object_r:system_map_t,s0)

#
# tmp_t is the type of the temporary directories
#
type tmp_t;
files_base_file(tmp_t)
files_tmp_file(tmp_t)
files_mountpoint(tmp_t)

#
# usr_t is the type for /usr.
#
type usr_t;
files_ro_base_file(usr_t)
files_mountpoint(usr_t)

#
# var_t is the type of /var
#
type var_t;
files_base_file(var_t)
files_mountpoint(var_t)

#
# var_cache_t is the type of /var/cache
#
type var_cache_t, cachefile;
files_base_file(var_cache_t)
files_mountpoint(var_cache_t)

#
# var_lib_t is the type of /var/lib
#
type var_lib_t;
files_base_file(var_lib_t)
files_mountpoint(var_lib_t)

#
# var_lock_t is the type of /var/lock
#
type var_lock_t;
files_base_file(var_lock_t)
files_lock_file(var_lock_t)
files_mountpoint(var_lock_t)

#
# run_t is the type of /run, usually
# used for pid and other runtime files.
#
type run_t;
files_base_file(run_t)
files_runtime_file(run_t)
files_mountpoint(run_t)

#
# var_spool_t is the type of /var/spool
#
type var_spool_t;
files_base_file(var_spool_t)


########################################
#
# Rules for all file types
#

gen_require(` #selint-disable:S-001
	type unlabeled_t;
')
neverallow { file_type -unlabeled_t } *:process *;
neverallow { file_type -unlabeled_t } *:process2 *;
neverallow * { file_type -unlabeled_t }:process *;
neverallow * { file_type -unlabeled_t }:process2 *;


fs_associate_xattr(file_type)


########################################
#
# Rules for elevated file types
#

# mmap(2) does not revalidate after context changes
auditallow basic_domain { auth_file_type security_file_type policy_config_t }:file { relabelfrom relabelto };  #selint-disable:W-001


########################################
#
# Rules for all cache file types
#

# /run/systemd/unit-root/var/cache/private
fs_associate_tmpfs(cachefile)


########################################
#
# Rules for all lock file types
#

fs_associate_tmpfs(lockfile)


########################################
#
# Rules for all runtime file types
#

fs_associate_tmpfs(runtimefile)


########################################
#
# Rules for all tmp file types
#

fs_associate_tmpfs(tmpfile)


########################################
#
# Rules for all tmpfs file types
#

fs_associate_tmpfs(tmpfsfile)


########################################
#
# Rules for all hugetlb file types
#

fs_associate_hugetlbfs(hugetlbfile)


########################################
#
# Rules for all shm filesystem file types
#

fs_associate_tmpfs(shmfsfile) # shmfs is a tmpfs
