policy_module(sysfs)

########################################
#
# Declarations
#

attribute sysfs_type;


#
# sysfs_t is the type for the /sys pseudofs
#
type sysfs_t, sysfs_type;
files_mountpoint(sysfs_t)
fs_xattr_type(sysfs_t)
genfscon sysfs / gen_context(system_u:object_r:sysfs_t,s0)


#
# /sys/module/apparmor
#
sysfs_detail(apparmor, /module/apparmor, modules)

#
# /sys/module/compression
#
sysfs_detail(modules_compression, /module/compression, modules)

#
# /sys/devices/system/cpu
#
sysfs_detail(cpu, /devices/system/cpu, generic_device)

#
# /sys/devices/system/cpu/online
#
sysfs_detail(cpu_online, /devices/system/cpu/online, cpu, generic_device)

#
# /sys/devices/system/cpu/possible
#
sysfs_detail(cpu_possible, /devices/system/cpu/possible, cpu, generic_device)

#
# node information
#
sysfs_detail(node_device, /devices/system/node, generic_device)

#
# /sys/module/kernel/parameters/crash_kexec_post_notifiers
#
sysfs_detail(crash_notifiers, /module/kernel/parameters/crash_kexec_post_notifiers, modules)

#
# /sys/firmware
#
sysfs_detail(firmware, /firmware)

#
# /sys/devices
#
sysfs_detail(generic_device, /devices)

#
# hardrive devices
#
sysfs_detail(harddrive, none, generic_device)

#
# hugepage data
# /sys/kernel/mm/hugepages
#
sysfs_detail(hugepage, /kernel/mm/hugepages)

#
# hugepage information
# /sys/kernel/mm/transparent_hugepage/enabled
# /sys/kernel/mm/transparent_hugepage/hpage_pmd_size
#
sysfs_detail(hugepage_info, /kernel/mm/transparent_hugepage/enabled)
sysfs_fscon(hugepage_info, /kernel/mm/transparent_hugepage/hpage_pmd_size)

#
# hwmon devices
#
sysfs_detail(hwmon, none, generic_device)

#
# kernel modules
# /sys/module
#
sysfs_detail(modules, /module)

#
# configfs kernel module files
# /sys/module/configfs
#
sysfs_detail(configfs_module, /module/configfs, modules)

#
# FUSE kernel module files
# /sys/module/fuse
#
sysfs_detail(fuse_module, /module/fuse, modules)

#
# PCIe Active State Power Management kernel module files
# /sys/module/pcie_aspm
#
sysfs_detail(pcieaspm_module, /module/pcie_aspm, modules)

#
# Intel Soundcard kernel module files
# /sys/module/snd_hda_intel
#
sysfs_detail(sndhda_module, /module/snd_hda_intel, modules)

#
# ZSwap kernel module files
# /sys/module/zswap
#
sysfs_detail(zswap_module, /module/zswap, modules)

#
# /sys/class/net and net devices
#
sysfs_detail(net, /class/net, generic_device)

#
# /sys/power
#
sysfs_detail(power, /power)

#
# /sys/devices/virtual/block/ for zram
#
sysfs_detail(virtual_dev, /devices/virtual/block, generic_device)
