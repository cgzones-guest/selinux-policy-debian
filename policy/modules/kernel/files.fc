#
# /
#
/.+					gen_context(system_u:object_r:default_t,s0)
/				-d	gen_context(system_u:object_r:root_t,s0)
/\.journal				<<none>>
/afs				-d	gen_context(system_u:object_r:mnt_t,s0)
/initrd\.img.*			-l	gen_context(system_u:object_r:boot_t,s0)
/vmlinuz.*			-l	gen_context(system_u:object_r:boot_t,s0)

#
# /boot
#
/boot					-d	gen_context(system_u:object_r:boot_t,s0)
/boot/.+					gen_context(system_u:object_r:boot_t,s0)
/boot/\.journal					<<none>>
/boot/efi/(?:.*/)?System\.map(?:-.+)?	--	gen_context(system_u:object_r:system_map_t,s0)
/boot/lost\+found			-d	gen_context(system_u:object_r:lost_found_t,mls_systemhigh)
/boot/lost\+found/.+				<<none>>
/boot/System\.map(?:-.+)?		--	gen_context(system_u:object_r:system_map_t,s0)

#
# /etc
#
/etc				-d	gen_context(system_u:object_r:etc_t,s0)
/etc/.+					gen_context(system_u:object_r:etc_t,s0)
/etc/\.fstab\.hal\..+		--	gen_context(system_u:object_r:etc_runtime_t,s0)
/etc/blkid			-d	gen_context(system_u:object_r:etc_runtime_t,s0)
/etc/blkid/.+				gen_context(system_u:object_r:etc_runtime_t,s0)
/etc/cmtab			--	gen_context(system_u:object_r:etc_runtime_t,s0)
/etc/fstab\.REVOKE		--	gen_context(system_u:object_r:etc_runtime_t,s0)
/etc/ioctl\.save		--	gen_context(system_u:object_r:etc_runtime_t,s0)
/etc/killpower			--	gen_context(system_u:object_r:etc_runtime_t,s0)
/etc/localtime			-l	gen_context(system_u:object_r:etc_t,s0)
/etc/mtab			--	gen_context(system_u:object_r:etc_runtime_t,s0)
/etc/mtab~[0-9]*		--	gen_context(system_u:object_r:etc_runtime_t,s0)
/etc/mtab\.tmp			--	gen_context(system_u:object_r:etc_runtime_t,s0)
/etc/mtab\.fuselock		--	gen_context(system_u:object_r:etc_runtime_t,s0)
/etc/nohotplug			--	gen_context(system_u:object_r:etc_runtime_t,s0)
/etc/nologin.*			--	gen_context(system_u:object_r:etc_runtime_t,s0)

/etc/network/ifstate		--	gen_context(system_u:object_r:etc_runtime_t,s0)

#
# HOME_ROOT
# expanded by genhomedircon
#
HOME_ROOT			-d	gen_context(system_u:object_r:home_root_t,s0-mls_systemhigh)
HOME_ROOT			-l	gen_context(system_u:object_r:home_root_t,s0)
HOME_ROOT/\.journal			<<none>>
HOME_ROOT/lost\+found		-d	gen_context(system_u:object_r:lost_found_t,mls_systemhigh)
HOME_ROOT/lost\+found/.+		<<none>>

#
# /initrd
#
# initrd mount point, only used during boot
/initrd				-d	gen_context(system_u:object_r:root_t,s0)

#
# /lost+found
#
/lost\+found			-d	gen_context(system_u:object_r:lost_found_t,mls_systemhigh)
/lost\+found/.+				<<none>>

#
# /media
#
# Mount points; do not relabel subdirectories, since
# we don't want to change any removable media by default.
/media				-d	gen_context(system_u:object_r:mnt_t,s0)
/media/[^/]+			-d	gen_context(system_u:object_r:mnt_t,s0)
/media/[^/]+			-l	gen_context(system_u:object_r:mnt_t,s0)
/media/[^/]+/.+				<<none>>
/media/\.hal-.*			--	gen_context(system_u:object_r:mnt_t,s0)

#
# /misc
#
/misc				-d	gen_context(system_u:object_r:mnt_t,s0)

#
# /mnt
#
/mnt				-d	gen_context(system_u:object_r:mnt_t,s0)
/mnt/[^/]+			-d	gen_context(system_u:object_r:mnt_t,s0)
/mnt/[^/]+			-l	gen_context(system_u:object_r:mnt_t,s0)
/mnt/[^/]+/.+				<<none>>

#
# /net
#
/net				-d	gen_context(system_u:object_r:mnt_t,s0)

#
# /opt
#
/opt				-d	gen_context(system_u:object_r:usr_t,s0)
/opt/.+					gen_context(system_u:object_r:usr_t,s0)

/opt/(?:.*/)?var/lib(?:64)?	-d	gen_context(system_u:object_r:var_lib_t,s0)
/opt/(?:.*/)?var/lib(?:64)?/.+		gen_context(system_u:object_r:var_lib_t,s0)

#
# /run
#
/run				-d	gen_context(system_u:object_r:run_t,s0-mls_systemhigh)
/run				-l	gen_context(system_u:object_r:run_t,s0)
/run/runit			-d	gen_context(system_u:object_r:run_t,s0)
/run/runit/supervise		-d	gen_context(system_u:object_r:run_t,s0)
/run/shm			-l	gen_context(system_u:object_r:run_t,s0)
/run/initctl			-l	gen_context(system_u:object_r:run_t,s0)
/run/.+					<<none>>

#
# /srv
#
/srv				-d	gen_context(system_u:object_r:var_t,s0)
/srv/.+					gen_context(system_u:object_r:var_t,s0)

#
# /tmp
#
/tmp				-d	gen_context(system_u:object_r:tmp_t,s0-mls_systemhigh)
/tmp/.+					<<none>>
/tmp/\.journal				<<none>>

/tmp/lost\+found		-d	gen_context(system_u:object_r:lost_found_t,mls_systemhigh)
/tmp/lost\+found/.+			<<none>>

/tmp/systemd-private-[^/]+	-d	gen_context(system_u:object_r:tmp_t,s0-mls_systemhigh)
/tmp/systemd-private-[^/]+/tmp	-d	gen_context(system_u:object_r:tmp_t,s0-mls_systemhigh)
/tmp/systemd-private-[^/]+/tmp/.+	<<none>>

#
# /usr
#
/usr				-d	gen_context(system_u:object_r:usr_t,s0)
/usr/.+					gen_context(system_u:object_r:usr_t,s0)
/usr/\.journal				<<none>>

/usr/doc/(?:.*/)?lib		-d	gen_context(system_u:object_r:usr_t,s0)
/usr/doc/(?:.*/)?lib/.+			gen_context(system_u:object_r:usr_t,s0)

/usr/include			-d	gen_context(system_u:object_r:usr_t,s0)
/usr/include/.+				gen_context(system_u:object_r:usr_t,s0)

/usr/lib/klibc/bin		-d	gen_context(system_u:object_r:usr_t,s0)

/usr/local/\.journal			<<none>>

/usr/local/lost\+found		-d	gen_context(system_u:object_r:lost_found_t,mls_systemhigh)
/usr/local/lost\+found/.+		<<none>>

/usr/lost\+found		-d	gen_context(system_u:object_r:lost_found_t,mls_systemhigh)
/usr/lost\+found/.+			<<none>>

/usr/share/doc/(?:.*/)?README.*		gen_context(system_u:object_r:usr_t,s0)

/usr/tmp			-d	gen_context(system_u:object_r:tmp_t,s0-mls_systemhigh)
/usr/tmp/.+				<<none>>

#
# /var
#
/var				-d	gen_context(system_u:object_r:var_t,s0)
/var/.+					gen_context(system_u:object_r:var_t,s0)
/var/\.journal				<<none>>

/var/cache			-d	gen_context(system_u:object_r:var_cache_t,s0)
/var/cache/.+				gen_context(system_u:object_r:var_cache_t,s0)

/var/lib			-d	gen_context(system_u:object_r:var_lib_t,s0)
/var/lib/.+				gen_context(system_u:object_r:var_lib_t,s0)

/var/lock			-d	gen_context(system_u:object_r:var_lock_t,s0-mls_systemhigh)
/var/lock			-l	gen_context(system_u:object_r:var_lock_t,s0)
/var/lock/subsys		-d	gen_context(system_u:object_r:var_lock_t,s0)
/var/lock/.+				<<none>>

/var/log/lost\+found		-d	gen_context(system_u:object_r:lost_found_t,mls_systemhigh)
/var/log/lost\+found/.+			<<none>>

/var/log/audit/lost\+found	-d	gen_context(system_u:object_r:lost_found_t,mls_systemhigh)
/var/log/audit/lost\+found/.+		<<none>>

/var/lost\+found		-d	gen_context(system_u:object_r:lost_found_t,mls_systemhigh)
/var/lost\+found/.+			<<none>>

/var/run			-l	gen_context(system_u:object_r:run_t,s0)

/var/spool			-d	gen_context(system_u:object_r:var_spool_t,s0)
/var/spool/.+				gen_context(system_u:object_r:var_spool_t,s0)

/var/tmp				-d	gen_context(system_u:object_r:tmp_t,s0-mls_systemhigh)
/var/tmp				-l	gen_context(system_u:object_r:tmp_t,s0)
/var/tmp/.+					<<none>>
/var/tmp/lost\+found			-d	gen_context(system_u:object_r:lost_found_t,mls_systemhigh)
/var/tmp/lost\+found/.+				<<none>>
/var/tmp/systemd-private-[^/]+		-d	gen_context(system_u:object_r:tmp_t,s0-mls_systemhigh)
/var/tmp/systemd-private-[^/]+/tmp	-d	gen_context(system_u:object_r:tmp_t,s0-mls_systemhigh)
/var/tmp/systemd-private-[^/]+/tmp/.+		<<none>>
/var/tmp/vi\.recover			-d	gen_context(system_u:object_r:tmp_t,s0)
