## <summary>rkhunter - rootkit checker.</summary>

########################################
## <summary>
##	Execute a domain transition to run rkhunter.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed to transition.
##	</summary>
## </param>
#
interface(`rkhunter_domtrans',`
	gen_require(`
		type rkhunter_t, rkhunter_exec_t;
	')

	corecmd_search_bin($1)
	domtrans_pattern($1, rkhunter_exec_t, rkhunter_t)
')

########################################
## <summary>
##	Execute rkhunter in the rkhunter domain,
##	and allow the specified role
##	the rkhunter domain.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed to transition.
##	</summary>
## </param>
## <param name="role">
##	<summary>
##	Role allowed access.
##	</summary>
## </param>
#
interface(`rkhunter_run',`
	gen_require(`
		attribute_role rkhunter_roles;
	')

	rkhunter_domtrans($1)
	roleattribute $2 rkhunter_roles;
')

########################################
## <summary>
##	Read the rkhunter configuration files.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed to transition.
##	</summary>
## </param>
## <param name="role">
##	<summary>
##	Role allowed access.
##	</summary>
## </param>
#
interface(`rkhunter_read_config',`
	gen_require(`
		type rkhunter_conf_t;
	')

	files_search_etc($1)
	allow $1 rkhunter_conf_t:file read_file_perms;
')

########################################
## <summary>
##	All of the rules required to
##	administrate an rkhunter environment.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
## <param name="role">
##	<summary>
##	Role allowed access.
##	</summary>
## </param>
#
interface(`rkhunter__admin',`
	gen_require(`
		type rkhunter_t, rkhunter_conf_t, rkhunter_log_t;
		type rkhunter_shmfs_t, rkhunter_state_t;
	')

	admin_process_pattern($1, rkhunter_t)

	files_search_etc($1)
	admin_pattern($1, rkhunter_conf_t)

	logging_search_logs($1)
	admin_pattern($1, rkhunter_log_t)

	files_search_var_lib($1)
	admin_pattern($1, rkhunter_state_t)

	fs_search_shmfs($1)
	admin_pattern($1, rkhunter_shmfs_t)

	rkhunter_run($1, $2)
')
