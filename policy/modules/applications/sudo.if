## <summary>sudo - provide limited super user privileges to specific users.</summary>

########################################
## <summary>
##	The role template for the sudo module.
## </summary>
## <desc>
##	<p>
##	This template creates a derived domain which is allowed
##	to change the linux user id, to run commands as a different
##	user.
##	</p>
## </desc>
## <param name="role_prefix">
##	<summary>
##	The prefix of the user role (e.g., user
##	is the prefix for user_r).
##	</summary>
## </param>
## <param name="user_role">
##	<summary>
##	The user role.
##	</summary>
## </param>
## <param name="user_domain">
##	<summary>
##	The user domain associated with the role.
##	</summary>
## </param>
#
template(`sudo_role_template',`
	gen_require(`
		attribute sudo_domain;
		type sudo_exec_t;
	')

	##############################
	#
	# Declarations
	#

	type $1_sudo_t, sudo_domain;
	userdom_user_application_domain($1_sudo_t, sudo_exec_t)
	domain_interactive_fd_object($1_sudo_t)
	#domain_role_change_exemption($1_sudo_t)
	role $2 types $1_sudo_t;

	##############################
	#
	# Local Policy
	#

	# Enter this derived domain from the user domain
	domtrans_pattern($3, sudo_exec_t, $1_sudo_t)

	# /proc/<pid>/stat
	read_process_pattern($1_sudo_t, $3)

	# By default, revert to the calling domain when a shell is executed.
	corecmd_shell_domtrans($1_sudo_t, $3)
	corecmd_bin_domtrans($1_sudo_t, $3)

	# attribute interfaces
	logging_send_audit_msgs($1_sudo_t)
')

########################################
## <summary>
##	The system template for the sudo module.
## </summary>
## <desc>
##	<p>
##	This template creates a derived domain which is allowed
##	to change the linux user id, to run commands as a different
##	user.
##	</p>
## </desc>
## <param name="type_prefix">
##	<summary>
##	The prefix of the caller (e.g., user
##	is the prefix for user_r).
##	</summary>
## </param>
## <param name="domain">
##	<summary>
##	The domain allowed access.
##	</summary>
## </param>
## <param name="file_type">
##	<summary>
##	The entrypoint type of the new process.
##	</summary>
## </param>
## <param name="target_domain">
##	<summary>
##	The type of the new process.
##	</summary>
## </param>
#
template(`sudo_system_template',`
	gen_require(`
		attribute sudo_domain;
		type sudo_exec_t;
	')

	##############################
	#
	# Declarations
	#

	type $1_sudo_t, sudo_domain;
	application_domain($1_sudo_t, sudo_exec_t)
	#domain_interactive_fd_object($1_sudo_t)
	#domain_role_change_exemption($1_sudo_t)
	role system_r types $1_sudo_t;

	##############################
	#
	# Local Policy
	#

	# Enter this derived domain from the user domain
	domtrans_pattern($2, sudo_exec_t, $1_sudo_t)

	systemd_use_fds($1_sudo_t)
	systemd_rw_inherited_stream_sockets($1_sudo_t)

	# attribute interfaces
	logging_send_audit_msgs($1_sudo_t)
')

########################################
## <summary>
##	Read sudo config files.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`sudo_read_config',`
	gen_require(`
		type sudo_conf_t;
	')

	files_search_etc($1)
	allow $1 sudo_conf_t:dir search_dir_perms;
	allow $1 sudo_conf_t:file read_file_perms;
')

########################################
## <summary>
##	Read sudoers config files.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`sudo_read_sudoers_config',`
	gen_require(`
		type sudoers_conf_t;
	')

	files_search_etc($1)
	allow $1 sudoers_conf_t:dir search_dir_perms;
	allow $1 sudoers_conf_t:file read_file_perms;
')

########################################
## <summary>
##	Exempt the domain from the constraint relabeling
##	sudo config files.
## </summary>
## <param name="domain">
##	<summary>
##	Domain to be exempted.
##	</summary>
## </param>
#
interface(`sudo_exempt_can_relabel_config',`
	gen_require(`
		attribute sudo_can_relabel_config;
	')

	typeattribute $1 sudo_can_relabel_config;
')

########################################
## <summary>
##	Reverse interface for systemd-tmpfiles.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`sudo__systemdtmpfiles',`
	gen_require(`
		type sudo_runtime_t;
	')

	allow $1 sudo_runtime_t:dir { add_name create getattr open read relabelfrom relabelto search write };
')

########################################
## <summary>
##	Admin interface.
## </summary>
## <param name="user_domain">
##	<summary>
##	The user domain associated with the role.
##	</summary>
## </param>
## <param name="user_role">
##	<summary>
##	The user role.
##	</summary>
## </param>
## <param name="role_prefix">
##	<summary>
##	The prefix of the user role (e.g., user
##	is the prefix for user_r).
##	</summary>
## </param>
#
template(`sudo__admin',`
	gen_require(`
		attribute sudo_can_relabel_config, sudo_can_write_config;
		type sudo_conf_t, sudoers_conf_t;
		type visudo_t, visudo_exec_t;
	')

	typeattribute $1 sudo_can_relabel_config, sudo_can_write_config;
	admin_pattern($1, { sudo_conf_t sudoers_conf_t })

	sudo_role_template($3, $2, $1)

	domtrans_pattern($1, visudo_exec_t, visudo_t)
	role $2 types visudo_t;
')
