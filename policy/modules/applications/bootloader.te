policy_module(bootloader)

########################################
#
# Declarations
#

attribute_role bootloader_roles;
roleattribute system_r bootloader_roles;


type bootloader_t;
type bootloader_exec_t;
application_domain(bootloader_t, bootloader_exec_t)
systemd_service_domain(bootloader_t, bootloader_exec_t)
role bootloader_roles types bootloader_t;
# vgs: create /run/lock/lvm/P_global:aux with system_u as sysadm_u
ubac_change_object_to_sys_exemption(bootloader_t)


type bootloader_conf_t;
files_config_file(bootloader_conf_t)

type bootloader_script_exec_t;
corecmd_executable_file(bootloader_script_exec_t)

type bootloader_tmp_t;
files_tmp_file(bootloader_tmp_t)

type bootloader_unit_t;
systemd_service_unit_file(bootloader_unit_t)


########################################
#
# bootloader local policy
#

allow bootloader_t self:capability { chown dac_override fsetid mknod sys_admin sys_rawio };
allow bootloader_t self:process { getsched setfscreate signal_perms };
allow bootloader_t self:fifo_file rw_fifo_file_perms;


allow bootloader_t bootloader_conf_t:dir list_dir_perms;
allow bootloader_t bootloader_conf_t:file read_file_perms;

can_exec(bootloader_t, bootloader_exec_t)

can_exec(bootloader_t, bootloader_script_exec_t)

allow bootloader_t bootloader_tmp_t:dir manage_dir_perms;
allow bootloader_t bootloader_tmp_t:lnk_file manage_lnk_file_perms;
allow bootloader_t bootloader_tmp_t:file manage_file_perms;
allow bootloader_t bootloader_tmp_t:blk_file manage_blk_file_perms;
allow bootloader_t bootloader_tmp_t:chr_file manage_chr_file_perms;
files_tmp_filetrans(bootloader_t, bootloader_tmp_t, { dir file lnk_file chr_file blk_file })


kernel_read_system_state(bootloader_t)
kernel_read_software_raid_state(bootloader_t)
kernel_read_public_kernel_sysctls(bootloader_t)
kernel_request_load_module(bootloader_t)
kernel_search_debugfs(bootloader_t)
kernel_read_cmdline(bootloader_t)
# apt-cache policy reads /proc/sys/crypto/fips_enabled
kernel_read_public_crypto_sysctls(bootloader_t)


corecmd_exec_bin(bootloader_t)
corecmd_exec_shell(bootloader_t)

dev_getattr_all_chr_files(bootloader_t)
dev_read_rand(bootloader_t)
dev_read_urand(bootloader_t)

domain_use_interactive_fds(bootloader_t)

files_create_boot_dirs(bootloader_t)
files_manage_boot_files(bootloader_t)
files_manage_boot_symlinks(bootloader_t)
files_read_usr_src_files(bootloader_t)
# /usr/share/grub
files_read_usr_files(bootloader_t)
files_list_root(bootloader_t)
files_getattr_all_shmfs_files(bootloader_t)
files_search_var_lib(bootloader_t)
files_search_locks(bootloader_t)

# grub-install --target=x86_64-efi -> /boo/efi/
fs_manage_dos_files(bootloader_t)
fs_manage_efivarfs_files(bootloader_t)
fs_getattr_efivarfs_fs(bootloader_t)
fs_list_hugetlbfs(bootloader_t)
fs_list_mqueuefs(bootloader_t)
fs_list_shmfs(bootloader_t)
fs_getattr_xattr_fs(bootloader_t)

# /usr/lib/python3/dist-packages/__pycache__/
libs_dontaudit_write_lib_dirs(bootloader_t)

logging_send_syslog_msg(bootloader_t)

miscfiles_read_config(bootloader_t)
miscfiles_read_localization(bootloader_t)

# modprobe efivars
modutils_exec(bootloader_t)
modutils_read_module_config(bootloader_t)
modutils_read_module_deps(bootloader_t)

selinux_getattr_fs(bootloader_t)
# via udevadm
selinux_map_status(bootloader_t)
# vgs
selinux_validate_context(bootloader_t)

seutil_read_config(bootloader_t)
seutil_read_file_contexts(bootloader_t)
# vgs
seutil_search_setrans_runtime(bootloader_t)

storage_raw_rw_fixed_disk_dev(bootloader_t)
storage_raw_control_fixed_disk_dev(bootloader_t, {
	ioctl_blkgetsize
	ioctl_blkflsbuf
	ioctl_blksszget
	ioctl_blkpbszget
	ioctl_cdrom_get_capability
})
storage_raw_read_removable_dev(bootloader_t)
storage_raw_control_removable_dev(bootloader_t, { ioctl_cdrom_get_capability })
storage_read_loop_dev(bootloader_t)
storage_control_loop_dev(bootloader_t, { ioctl_blkgetsize })
storage_getattr_zram_dev(bootloader_t)

sysfs_list_generic(bootloader_t)
sysfs_read_firmware(bootloader_t)
sysfs_read_generic_device(bootloader_t)
sysfs_read_harddrive(bootloader_t)
sysfs_read_virtual_dev(bootloader_t)
sysfs_read_modules_compression(bootloader_t)

systemd_check_exec(bootloader_t)

term_list_ptys(bootloader_t)

# udevadm
udev_exec(bootloader_t)
udev_read_config(bootloader_t)
udev_read_runtime_files(bootloader_t)

userdom_use_inherited_user_terminals(bootloader_t)
userdom_dontaudit_search_user_home_dirs(bootloader_t)


optional_policy(`
	apt_use_ptys(bootloader_t)
')

optional_policy(`
	dev_rw_lvm_control(bootloader_t)
	dev_control_lvm_control(bootloader_t, { ioctl_dm_version ioctl_dm_list_devices ioctl_dm_table_deps ioctl_dm_table_status })
')

optional_policy(`
	dpkg_read_config(bootloader_t)
	# dpkg --compare-versions
	dpkg_exec(bootloader_t)
')

optional_policy(`
	fstools_exec(bootloader_t)
	fstools_runtime_filetrans(bootloader_t, dir, "blkid")
	fstools_manage_runtime(bootloader_t)
')

optional_policy(`
	lsbrelease_run(bootloader_t, bootloader_roles)
')

optional_policy(`
	lvm_exec(bootloader_t)
	lvm_manage_lock_files(bootloader_t)
	lvm_manage_runtime_files(bootloader_t)
	lvm_read_config(bootloader_t)

	# vgs --options vg_uuid,pv_name --noheadings --separator :
	kernel_dontaudit_getattr_core_if(bootloader_t)
	systemd_getattr_initctl(bootloader_t)
	systemd_journal_getattr_sink(bootloader_t)
	term_getattr_pty_dirs(bootloader_t)
')
