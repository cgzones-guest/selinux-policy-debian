policy_module(certbot)

########################################
#
# Declarations
#

attribute_role certbot_roles;

type certbot_t;
type certbot_exec_t;
application_domain(certbot_t, certbot_exec_t)
role certbot_roles types certbot_t;
# create apache config file with system_u
ubac_change_object_to_sys_exemption(certbot_t)
# signal apache2 daemon
rbac_process_subject_full_exemption(certbot_t)


type certbot_conf_t;
files_config_file(certbot_conf_t)

type certbot_cert_t;
miscfiles_cert_type(certbot_cert_t)

type certbot_lock_t;
files_lock_file(certbot_lock_t)

type certbot_log_t;
logging_log_file(certbot_log_t)

type certbot_state_t;
files_state_file(certbot_state_t)

type certbot_tls_privkey_t;
miscfiles_tls_privkey_type(certbot_tls_privkey_t)

type certbot_tmp_t;
files_tmp_file(certbot_tmp_t)

type certbot_unit_t;
systemd_service_unit_file(certbot_unit_t)


########################################
#
# Certbot policy
#

allow certbot_t self:process setrlimit;
allow certbot_t self:fifo_file { getattr ioctl read write };
allow certbot_t self:unix_stream_socket { connect create };

allow certbot_t certbot_cert_t:dir rw_dir_perms;
allow certbot_t certbot_cert_t:lnk_file manage_lnk_file_perms;
allow certbot_t certbot_cert_t:file manage_file_perms;

allow certbot_t certbot_conf_t:dir rw_dir_perms;
allow certbot_t certbot_conf_t:lnk_file read_lnk_file_perms;
allow certbot_t certbot_conf_t:file manage_file_perms;

allow certbot_t certbot_lock_t:file { create getattr lock open unlink write };
filetrans_add_pattern(certbot_t, certbot_log_t, certbot_lock_t, file, ".certbot.lock")

allow certbot_t certbot_log_t:dir rw_dir_perms;
allow certbot_t certbot_log_t:file { append_file_perms create };

allow certbot_t certbot_state_t:dir manage_dir_perms;
allow certbot_t certbot_state_t:file manage_file_perms;
# /var/lib/letsencrypt/temp_checkpoint/git-ssl.conf_0
allow certbot_t certbot_state_t:file relabelfrom;

allow certbot_t certbot_tls_privkey_t:lnk_file manage_lnk_file_perms;
allow certbot_t certbot_tls_privkey_t:file { getattr unlink };

allow certbot_t certbot_tmp_t:dir manage_dir_perms;
allow certbot_t certbot_tmp_t:file manage_file_perms;
files_tmp_filetrans(certbot_t, certbot_tmp_t, { dir file })


kernel_read_vm_overcommit_sysctl(certbot_t)


corecmd_exec_bin(certbot_t)
corecmd_exec_shell(certbot_t)

corenet_send_generic_node(certbot_t)

corenet_send_stdextern_if(certbot_t)

corenet_tcp_bind_lo_node(certbot_t)
corenet_sendrecv_http_client_packets(certbot_t)
corenet_tcp_connect_http_port(certbot_t)

dev_read_urand(certbot_t)

domain_use_interactive_fds(certbot_t)

files_read_usr_files(certbot_t)
# /etc/
files_list_etc(certbot_t)
files_search_locks(certbot_t)
files_search_runtime(certbot_t)
files_search_var_lib(certbot_t)

# ldconfig -p
libs_exec_ldconfig(certbot_t)

logging_search_logs(certbot_t)

miscfiles_read_generic_certs(certbot_t)
miscfiles_read_config(certbot_t)
miscfiles_read_localization(certbot_t)
miscfiles_read_terminfo(certbot_t)
miscfiles_search_web_root(certbot_t)

selinux_getattr_fs(certbot_t)

seutil_search_config(certbot_t)
seutil_search_setrans_runtime(certbot_t)

sysnet_dns_name_resolve(certbot_t)

userdom_use_inherited_user_terminals(certbot_t)
userdom_control_user_ptys(certbot_t, { ioctl_tiocswinsz })
userdom_dontaudit_search_user_home_dirs(certbot_t)


optional_policy(`
	apache2_read_config(certbot_t)
	# TODO: refactor
	gen_require(` #selint-disable:S-001
		type apache2_t, apache2_conf_t, apache2_content_t, apache2_ctl_exec_t, apache2_exec_t, apache2_lock_t, apache2_log_t, apache2_runtime_t;
	')
	can_exec(certbot_t, apache2_ctl_exec_t)
	# apache2ctl configtest
	can_exec(certbot_t, apache2_exec_t)

	files_search_var_lib(apache2_t) #selint-disable:C-001
	allow apache2_t certbot_state_t:dir search_dir_perms; #selint-disable:C-001
	allow apache2_t certbot_state_t:file mmap_read_file_perms; #selint-disable:C-001

	# /etc/apache2/sites-available/default-ssl.conf.GqVrXE
	allow certbot_t apache2_conf_t:dir { add_name remove_name write };
	allow certbot_t apache2_conf_t:file { create relabelfrom relabelto rename setattr unlink write };
	allow certbot_t apache2_content_t:dir search_dir_perms;
	allow certbot_t apache2_lock_t:dir getattr;
	allow certbot_t apache2_runtime_t:dir search_dir_perms;
	allow certbot_t apache2_runtime_t:file read_file_perms;
	allow certbot_t apache2_t:process { signal signull };

	apache2_append_logs(certbot_t)
')

optional_policy(`
	lsbrelease_run(certbot_t, certbot_roles)
')
