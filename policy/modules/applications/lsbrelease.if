## <summary>Linux Standard Base version reporting utility.</summary>

########################################
## <summary>
##	Execute lsb_release in the lsbrelease domain.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed to transition.
##	</summary>
## </param>
#
interface(`lsbrelease_domtrans',`
	gen_require(`
		type lsbrelease_t, lsbrelease_exec_t;
	')

	corecmd_search_bin($1)
	domtrans_pattern($1, lsbrelease_exec_t, lsbrelease_t)
')

########################################
## <summary>
##	Execute lsb_release in the lsbrelease domain.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed to transition.
##	</summary>
## </param>
## <param name="role">
##	<summary>
##	Role allowed access.
##	</summary>
## </param>
#
interface(`lsbrelease_run',`
	gen_require(`
		type lsbrelease_t;
	')

	lsbrelease_domtrans($1)
	role $2 types lsbrelease_t;
')

########################################
## <summary>
##	Execute lsb_release in the lsbrelease domain
##	from a NoNewPrivileges process.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed to transition.
##	</summary>
## </param>
#
interface(`lsbrelease_nnp_domtrans',`
	gen_require(`
		type lsbrelease_t;
	')

	allow $1 lsbrelease_t:process2 nnp_transition;
')

########################################
## <summary>
##	Execute lsb_release in the caller domain.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`lsbrelease_exec',`
	gen_require(`
		type lsbrelease_exec_t;
	')

	corecmd_search_bin($1)
	can_exec($1, lsbrelease_exec_t)
')

########################################
## <summary>
##	All of the rules required to
##	administrate a lsb_release environment.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
## <param name="role">
##	<summary>
##	Role allowed access.
##	</summary>
## </param>
#
interface(`lsbrelease__admin',`
	lsbrelease_run($1, $2)
')
