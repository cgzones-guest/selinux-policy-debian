## <summary>Run shells with substitute user and group.</summary>

########################################
## <summary>
##	The role template for the su module.
## </summary>
## <param name="role_prefix">
##	<summary>
##	The prefix of the user role (e.g., user
##	is the prefix for user_r).
##	</summary>
## </param>
## <param name="user_role">
##	<summary>
##	The role associated with the user domain.
##	</summary>
## </param>
## <param name="user_domain">
##	<summary>
##	The type of the user domain.
##	</summary>
## </param>
#
template(`su_role_template',`
	gen_require(`
		attribute su_domain;
		type su_exec_t;
	')

	########################################
	#
	# Declarations
	#

	type $1_su_t, su_domain;
	domain_interactive_fd_object($1_su_t)
	userdom_user_application_domain($1_su_t, su_exec_t)
	role $2 types $1_su_t;

	optional_policy(`
		dbus_system_bus_client($1_su_t)
	')


	########################################
	#
	# Role dependent policy
	#

	# shutdown signals
	allow $1_su_t $3:process { sigkill signal };
	allow $3 $1_su_t:process signal;

	allow $3 $1_su_t:process sigchld;

	# Transition from the user domain to this domain.
	domtrans_pattern($3, su_exec_t, $1_su_t)

	# By default revert to the calling domain when a shell is executed.
	corecmd_shell_domtrans($1_su_t, $3)

	logging_send_audit_msgs($1_su_t)
')

########################################
## <summary>
##	Read the process state of su programs.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`su_read_all_states',`
	gen_require(`
		attribute su_domain;
	')

	allow $1 su_domain:dir search_dir_perms;
	allow $1 su_domain:lnk_file read_lnk_file_perms;
	allow $1 su_domain:file read_file_perms;
')

########################################
## <summary>
##	All of the rules required to
##	administrate an su environment.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
## <param name="role">
##	<summary>
##	Role allowed access.
##	</summary>
## </param>
## <param name="role_prefix">
##	<summary>
##	The prefix of the user role (e.g., user
##	is the prefix for user_r).
##	</summary>
## </param>
#
template(`su__admin',`
	su_role_template($3, $2, $1)
')
