## <summary>binfmt - Support for extra binary formats.</summary>

########################################
## <summary>
##	Execute a domain transition to run update-binfmts.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed to transition.
##	</summary>
## </param>
#
interface(`binfmt_domtrans',`
	gen_require(`
		type binfmt_t, binfmt_exec_t;
	')

	corecmd_search_bin($1)
	domtrans_pattern($1, binfmt_exec_t, binfmt_t)
')

########################################
## <summary>
##	Execute update-binfmts in the update-binfmts domain,
##	and allow the specified role
##	the update-binfmts domain.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed to transition.
##	</summary>
## </param>
## <param name="role">
##	<summary>
##	Role allowed access.
##	</summary>
## </param>
#
interface(`binfmt_run',`
	gen_require(`
		type binfmt_t;
	')

	binfmt_domtrans($1)
	role $2 types binfmt_t;
')

########################################
## <summary>
##	Execute update-binfmts in the caller domain.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`binfmt_exec',`
	gen_require(`
		type binfmt_exec_t;
	')

	corecmd_search_bin($1)
	can_exec($1, binfmt_exec_t)
')

########################################
## <summary>
##	Read the binfmt database content.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`binfmt_read_state',`
	gen_require(`
		type binfmt_state_t;
	')

	files_search_var_lib($1)
	allow $1 binfmt_state_t:dir list_dir_perms;
	allow $1 binfmt_state_t:file read_file_perms;
')

########################################
## <summary>
##	All of the rules required to
##	administrate a binfmt environment.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
## <param name="role">
##	<summary>
##	Role allowed access.
##	</summary>
## </param>
#
interface(`binfmt__admin',`
	binfmt_run($1, $2)
')
