## <summary>Systemd coredump.</summary>

########################################
## <summary>
##	Search the systemd-coredump coredumps directory.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`systemd_coredump_search_coredumps',`
	gen_require(`
		type systemd_coredump_var_lib_t;
	')

	systemd_search_var_lib($1)
	allow $1 systemd_coredump_var_lib_t:dir search_dir_perms;
')

########################################
## <summary>
##	Read systemd-coredump entries.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`systemd_coredump_read_coredumps',`
	gen_require(`
		type systemd_coredump_var_lib_t;
	')

	systemd_search_var_lib($1)
	allow $1 systemd_coredump_var_lib_t:dir search_dir_perms;
	allow $1 systemd_coredump_var_lib_t:file read_file_perms;
')

########################################
## <summary>
##	Reverse interface for systemd-tmpfiles.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`systemd_coredump__systemdtmpfiles',`
	gen_require(`
		type systemd_coredump_var_lib_t;
	')

	systemd_search_var_lib($1)
	allow $1 systemd_coredump_var_lib_t:dir { getattr open read relabelfrom relabelto remove_name search setattr write };
	allow $1 systemd_coredump_var_lib_t:file { getattr lock open read unlink };
')

########################################
## <summary>
##	Reverse interface for systemd.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access (systemd_t).
##	</summary>
## </param>
## <param name="domain">
##	<summary>
##	Domain allowed access (systemd_generic_bin_t).
##	</summary>
## </param>
#
interface(`systemd_coredump__systemd',`
	gen_require(`
		type systemd_coredump_t, systemd_coredump_runtime_t, systemd_coredump_var_lib_t;
	')

	allow $1 systemd_coredump_t:unix_stream_socket { accept bind create getattr getopt listen setopt };

	allow $1 systemd_coredump_runtime_t:sock_file { create getattr unlink write };

	allow $1 systemd_coredump_var_lib_t:dir { getattr mounton open read };
')
