## <summary>Systemd journal, the system journal/log.</summary>
## <required val="true" />

########################################
## <summary>
##	Execute a domain transition to run journalctl.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed to transition.
##	</summary>
## </param>
#
interface(`systemd_journal_domtrans_ctl',`
	gen_require(`
		type systemd_journal_ctl_t, systemd_journal_ctl_exec_t;
	')

	corecmd_search_bin($1)
	domtrans_pattern($1, systemd_journal_ctl_exec_t, systemd_journal_ctl_t)
')

########################################
## <summary>
##	Execute journalctl in the journalctl domain,
##	and allow the specified role
##	the journalctl domain.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed to transition.
##	</summary>
## </param>
## <param name="role">
##	<summary>
##	Role allowed access.
##	</summary>
## </param>
#
interface(`systemd_journal_run_ctl',`
	gen_require(`
		attribute_role systemd_journal_ctl_roles;
	')

	systemd_journal_domtrans_ctl($1)
	roleattribute $2 systemd_journal_ctl_roles;
')

########################################
## <summary>
##	Execute journalctl in the caller domain.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`systemd_journal_exec_journalctl',`
	gen_require(`
		type systemd_journal_ctl_exec_t;
	')

	corecmd_search_bin($1)
	can_exec($1, systemd_journal_ctl_exec_t)
')

########################################
## <summary>
##	Read the systemd journal catalog.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`systemd_journal_read_catalog',`
	gen_require(`
		type systemd_journal_catalog_t;
	')

	systemd_search_var_lib($1)
	allow $1 systemd_journal_catalog_t:dir search_dir_perms;
	allow $1 systemd_journal_catalog_t:file mmap_read_file_perms;
')

########################################
## <summary>
##	Watch systemd journal log database directories.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`systemd_journal_watch_journal_dirs',`
	gen_require(`
		type systemd_journal_data_t;
	')

	allow $1 systemd_journal_data_t:dir watch_dir_perms;
')

########################################
## <summary>
##	Do not audit listing the systemd journal.
## </summary>
## <param name="domain">
##	<summary>
##	Domain to not audit.
##	</summary>
## </param>
#
interface(`systemd_journal_dontaudit_list_journal',`
	gen_require(`
		type systemd_journal_data_t;
	')

	dontaudit $1 systemd_journal_data_t:dir list_dir_perms;
')

########################################
## <summary>
##	Get the attributes of the systemd journal log database.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`systemd_journal_getattr_journal',`
	gen_require(`
		type systemd_journal_data_t;
	')

	files_search_runtime($1)
	logging_search_logs($1)
	allow $1 systemd_journal_data_t:dir list_dir_perms;
	allow $1 systemd_journal_data_t:file getattr;
	allow $1 systemd_journal_data_t:lnk_file getattr;
')

########################################
## <summary>
##	Read the systemd journal log database.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`systemd_journal_read_journal',`
	gen_require(`
		type systemd_journal_data_t;
	')

	files_search_runtime($1)
	logging_search_logs($1)
	allow $1 systemd_journal_data_t:dir list_dir_perms;
	allow $1 systemd_journal_data_t:file mmap_read_file_perms;
')

########################################
## <summary>
##	Use the inherited file descriptors from systemd-journald.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`systemd_journal_use_fds',`
	gen_require(`
		type systemd_journald_t;
	')

	allow $1 systemd_journald_t:fd use;
')

########################################
## <summary>
##	Connect to systemd-journald stream sockets.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`systemd_journal_connect_stream_sockets',`
	gen_require(`
		type systemd_journald_t;
	')

	allow $1 systemd_journald_t:unix_stream_socket connectto;
')

########################################
## <summary>
##	Get the attributes of the systemd-journald sink dev-log.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`systemd_journal_getattr_sink',`
	gen_require(`
		type systemd_journald_logsink_t;
	')

	allow $1 systemd_journald_logsink_t:sock_file getattr;
')

########################################
## <summary>
##	Allow the domain to act as an syslog daemon.
##	(like rsyslog)
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`systemd_journal_syslog_daemon',`
	gen_require(`
		type systemd_journald_t;
		type systemd_journald_logsink_t;
	')

	allow $1 systemd_journald_logsink_t:sock_file getattr;
	allow $1 systemd_journald_t:unix_dgram_socket { getattr getopt read sendto setopt write };
	allow systemd_journald_t $1:unix_dgram_socket sendto;
')

########################################
## <summary>
##	Allow the domain to act as a syslog client, sending messages.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`systemd_journal_syslog_client',`
	gen_require(`
		type systemd_journald_t;
		type systemd_journald_logsink_t, systemd_journald_runtime_t;
	')

	# systemd hardening: ProtectKernelLogs
	fs_read_tmpfs_symlinks($1)

	systemd_search_runtime($1)
	allow $1 systemd_journald_t:unix_dgram_socket sendto;
	allow $1 systemd_journald_runtime_t:dir search_dir_perms;
	allow $1 systemd_journald_logsink_t:sock_file write;
')

########################################
## <summary>
##	Reverse interface for systemd-tmpfiles.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`systemd_journal__systemdtmpfiles',`
	gen_require(`
		type systemd_journal_data_t;
	')

	logging_search_logs($1)
	allow $1 systemd_journal_data_t:dir { list_dir_perms relabel_dir_perms setattr };
	allow $1 systemd_journal_data_t:file { relabel_file_perms setattr };
')

########################################
## <summary>
##	Reverse interface for systemd.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access (systemd_t).
##	</summary>
## </param>
## <param name="domain">
##	<summary>
##	Domain allowed access (systemd_generic_bin_t).
##	</summary>
## </param>
#
interface(`systemd_journal__systemd',`
	gen_require(`
		type systemd_journald_t, systemd_journald_logsink_t, systemd_journald_runtime_t;
	')

	allow $1 systemd_journald_t:netlink_audit_socket { bind create getopt setopt };
	allow $1 systemd_journald_t:unix_dgram_socket { bind create getopt sendto setopt };
	allow $1 systemd_journald_t:unix_stream_socket { bind connectto create getattr getopt listen read setopt write };

	allow $1 systemd_journald_logsink_t:sock_file { create getattr unlink };

	allow $1 systemd_journald_runtime_t:dir { add_name create open read remove_name write };
')

########################################
## <summary>
##	All the rules to manage a journal environment.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`systemd_journal__admin',`
	systemd_journal_run_ctl($1, $2)
')
