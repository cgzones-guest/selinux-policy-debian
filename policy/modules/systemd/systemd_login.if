## <summary>Systemd logind.</summary>
## <required val="true" />

########################################
## <summary>
##	Send and receive messages from
##	systemd logind over dbus.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`systemd_login_dbus_chat',`
	gen_require(`
		type systemd_logind_t;
	')

	DBus_send_pattern($1, systemd_logind_t)

	# read /proc/<pid>/cgroup
	read_process_pattern(systemd_logind_t, $1)
')

########################################
## <summary>
##	Read the process state of systemd-logind.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`systemd_login_read_state',`
	gen_require(`
		type systemd_logind_t;
	')

	read_process_pattern($1, systemd_logind_t)
')

########################################
## <summary>
##	Use inherited systemd logind file descriptors.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`systemd_login_use_fds',`
	gen_require(`
		type systemd_logind_t;
	')

	allow $1 systemd_logind_t:fd use;
')

########################################
## <summary>
##	Allow specified domain to inform systemd-logind sessions.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`systemd_login_inform',`
	gen_require(`
		type systemd_logind_runtime_t;
	')

	allow $1 systemd_logind_runtime_t:fifo_file write;
')

########################################
## <summary>
##	Write inherited logind inhibit pipes.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`systemd_login_write_inherited_inhibit_pipes',`
	gen_require(`
		type systemd_logind_t, systemd_logind_inhibit_runtime_t;
	')

	allow $1 systemd_logind_t:fd use;
	allow $1 systemd_logind_inhibit_runtime_t:fifo_file write;
')

########################################
## <summary>
##	Search systemd-logind seats directories.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`systemd_login_search_seats',`
	gen_require(`
		type systemd_logind_seats_runtime_t;
	')

	systemd_search_runtime($1)
	allow $1 systemd_logind_seats_runtime_t:dir search_dir_perms;
')

########################################
## <summary>
##	Watch systemd-logind seats directories.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`systemd_login_watch_seats_dirs',`
	gen_require(`
		type systemd_logind_seats_runtime_t;
	')

	systemd_search_runtime($1)
	allow $1 systemd_logind_seats_runtime_t:dir watch_dir_perms;
')

########################################
## <summary>
##	Read systemd-logind seats.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`systemd_login_read_seats',`
	gen_require(`
		type systemd_logind_seats_runtime_t;
	')

	systemd_search_runtime($1)
	allow $1 systemd_logind_seats_runtime_t:dir search_dir_perms;
	allow $1 systemd_logind_seats_runtime_t:file read_file_perms;
')

########################################
## <summary>
##	Get the attributes of systemd-logind users directories.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`systemd_login_getattr_users_dirs',`
	gen_require(`
		type systemd_logind_users_runtime_t;
	')

	systemd_search_runtime($1)
	allow $1 systemd_logind_users_runtime_t:dir getattr;
')

########################################
## <summary>
##	Search systemd-logind users directories.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`systemd_login_search_users',`
	gen_require(`
		type systemd_logind_users_runtime_t;
	')

	systemd_search_runtime($1)
	allow $1 systemd_logind_users_runtime_t:dir search_dir_perms;
')

########################################
## <summary>
##	Watch systemd-logind users directories.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`systemd_login_watch_users_dirs',`
	gen_require(`
		type systemd_logind_users_runtime_t;
	')

	systemd_search_runtime($1)
	allow $1 systemd_logind_users_runtime_t:dir watch_dir_perms;
')

########################################
## <summary>
##	Read systemd-logind users files.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`systemd_login_read_users_files',`
	gen_require(`
		type systemd_logind_users_runtime_t;
	')

	systemd_search_runtime($1)
	allow $1 systemd_logind_users_runtime_t:dir search_dir_perms;
	allow $1 systemd_logind_users_runtime_t:file read_file_perms;
')

########################################
## <summary>
##	Write to systemd-logind sessions pipes.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`systemd_login_write_session_pipes',`
	gen_require(`
		type systemd_logind_sessions_runtime_t;
	')

	allow $1 systemd_logind_sessions_runtime_t:fifo_file write;
')

########################################
## <summary>
##	List systemd-logind sessions directories.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`systemd_login_list_sessions_dirs',`
	gen_require(`
		type systemd_logind_sessions_runtime_t;
	')

	systemd_search_runtime($1)
	allow $1 systemd_logind_sessions_runtime_t:dir list_dir_perms;
')

########################################
## <summary>
##	Watch systemd-logind sessions directories.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`systemd_login_watch_sessions_dirs',`
	gen_require(`
		type systemd_logind_sessions_runtime_t;
	')

	systemd_search_runtime($1)
	allow $1 systemd_logind_sessions_runtime_t:dir watch_dir_perms;
')

########################################
## <summary>
##	Read systemd-logind sessions files.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`systemd_login_read_sessions_files',`
	gen_require(`
		type systemd_logind_sessions_runtime_t;
	')

	systemd_search_runtime($1)
	allow $1 systemd_logind_sessions_runtime_t:dir search_dir_perms;
	allow $1 systemd_logind_sessions_runtime_t:file read_file_perms;
')

########################################
## <summary>
##	Reverse interface for systemd.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access (systemd_t).
##	</summary>
## </param>
## <param name="domain">
##	<summary>
##	Domain allowed access (systemd_generic_bin_t).
##	</summary>
## </param>
#
interface(`systemd_login__systemd',`
	gen_require(`
		type systemd_logind_t;
		type systemd_logind_inhibit_runtime_t, systemd_logind_seats_runtime_t, systemd_logind_sessions_runtime_t;
		type systemd_logind_users_runtime_t, systemd_logind_var_lib_t;
	')

	allow $1 systemd_logind_t:fd use;
	allow $1 systemd_logind_inhibit_runtime_t:dir { create getattr mounton open read };
	allow $1 systemd_logind_seats_runtime_t:dir { getattr mounton open read };
	allow $1 systemd_logind_sessions_runtime_t:dir { getattr mounton open read };
	allow $1 systemd_logind_sessions_runtime_t:fifo_file write;
	allow $1 systemd_logind_users_runtime_t:dir { getattr mounton open read };
	allow $1 systemd_logind_var_lib_t:dir { getattr mounton open read };

	systemd_login_dbus_chat($1)
')

########################################
## <summary>
##	Reverse interface for systemd-tmpfiles.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`systemd_login__systemdtmpfiles',`
	gen_require(`
		type systemd_logind_seats_runtime_t, systemd_logind_sessions_runtime_t, systemd_logind_users_runtime_t;
	')

	allow $1 { systemd_logind_seats_runtime_t systemd_logind_sessions_runtime_t systemd_logind_users_runtime_t }:dir { create_dir_perms list_dir_perms relabel_dir_perms };
')

########################################
## <summary>
##	Allow admin access to send instructions to systemd-logind.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`systemd_login__admin',`
	systemd_login_dbus_chat($1)
')
