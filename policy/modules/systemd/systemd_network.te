policy_module(systemd_network)

########################################
#
# Declarations
#

type systemd_networkd_t;
type systemd_networkd_exec_t;
systemd_service_domain(systemd_networkd_t, systemd_networkd_exec_t)

type systemd_networkd_wait_online_t;
type systemd_networkd_wait_online_exec_t;
systemd_service_domain(systemd_networkd_wait_online_t, systemd_networkd_wait_online_exec_t)


type systemd_networkd_conf_t;
files_config_file(systemd_networkd_conf_t)

type systemd_networkd_runtime_t;
files_runtime_file(systemd_networkd_runtime_t)

type systemd_networkd_tmpfs_t;
files_tmpfs_file(systemd_networkd_tmpfs_t)

type systemd_networkd_unit_t;
systemd_service_unit_file(systemd_networkd_unit_t)
optional_policy(`
	dbus_system_bus_started_unit(systemd_networkd_unit_t) #selint-disable: C-001
')

type systemd_networkd_var_lib_t;
files_state_file(systemd_networkd_var_lib_t)


########################################
#
# Policy for systemd-networkd
#

# sys_admin	: bpf
allow systemd_networkd_t self:capability { net_admin net_raw sys_admin };
allow systemd_networkd_t self:capability2 bpf;
allow systemd_networkd_t self:process { getcap setcap setfscreate };
allow systemd_networkd_t self:bpf { map_create map_read map_write prog_load prog_run };
allow systemd_networkd_t self:netlink_netfilter_socket { bind create getattr getopt read setopt write };
allow systemd_networkd_t self:netlink_kobject_uevent_socket { bind create getattr getopt read setopt };
allow systemd_networkd_t self:netlink_route_socket { bind create getattr getopt nlmsg_read nlmsg_write read setopt write };
allow systemd_networkd_t self:netlink_generic_socket { bind create getattr getopt read setopt write };
allow systemd_networkd_t self:packet_socket { bind create read setopt write };
allow systemd_networkd_t self:rawip_socket { create read setopt write };
allow systemd_networkd_t self:udp_socket { bind create read setopt write };
allow systemd_networkd_t self:unix_dgram_socket { connect create getopt setopt write };
allow systemd_networkd_t self:unix_stream_socket { accept bind connect create getattr getopt listen read setopt write };
xperm_pattern(systemd_networkd_t, self, udp_socket, { ioctl_siocgifname ioctl_siocethtool })
xperm_pattern(systemd_networkd_t, self, unix_dgram_socket, ioctl_siocgifname)


allow systemd_networkd_t systemd_networkd_conf_t:dir list_dir_perms;
allow systemd_networkd_t systemd_networkd_conf_t:file read_file_perms;

allow systemd_networkd_t systemd_networkd_runtime_t:dir manage_dir_perms;
allow systemd_networkd_t systemd_networkd_runtime_t:file manage_file_perms;
allow systemd_networkd_t systemd_networkd_runtime_t:sock_file manage_sock_file_perms;

# /memfd:data-fd
allow systemd_networkd_t systemd_networkd_tmpfs_t:file { getattr read write };
fs_tmpfs_filetrans(systemd_networkd_t, systemd_networkd_tmpfs_t, file)

allow systemd_networkd_t systemd_networkd_var_lib_t:dir manage_dir_perms;


systemd_use_notify(systemd_networkd_t)
systemd_read_state(systemd_networkd_t)
systemd_read_machine_id(systemd_networkd_t)
systemd_search_config(systemd_networkd_t)


# net-pf-16-proto-16-family-nl80211
kernel_request_load_module(systemd_networkd_t)
# /proc/sys/net/ipv6/conf/enp7s0/disable_ipv6
kernel_rw_net_sysctls(systemd_networkd_t)
kernel_getattr_proc(systemd_networkd_t)
kernel_read_public_kernel_sysctls(systemd_networkd_t)
kernel_read_system_state(systemd_networkd_t)
kernel_read_cmdline(systemd_networkd_t)
kernel_read_network_state(systemd_networkd_t)


corenet_send_stdextern_if(systemd_networkd_t)
corenet_send_generic_node(systemd_networkd_t)

# DHCP
corenet_udp_bind_lan_node(systemd_networkd_t)
corenet_udp_bind_dhcpc_port(systemd_networkd_t)
corenet_sendrecv_dhcpd_client_packets(systemd_networkd_t)

corenet_sendrecv_icmp_packets(systemd_networkd_t)

files_watch_generic_runtime_dirs(systemd_networkd_t)
files_watch_root_dir(systemd_networkd_t)
files_search_runtime(systemd_networkd_t)

fs_getattr_cgroup(systemd_networkd_t)
fs_list_cgroup_dirs(systemd_networkd_t)
fs_list_cgroup_systemslice_dirs(systemd_networkd_t)
# TODO: /sys/fs/cgroup/system.slice/systemd-logind.service/memory.pressure
fs_read_cgroup_systemslice_files(systemd_networkd_t)
fs_dontaudit_write_cgroup_systemslice_files(systemd_networkd_t)
fs_read_nsfs_files(systemd_networkd_t)
fs_getattr_xattr_fs(systemd_networkd_t)

logging_send_syslog_msg(systemd_networkd_t)

miscfiles_read_localization(systemd_networkd_t)

mount_search_runtime(systemd_networkd_t)

selinux_getattr_fs(systemd_networkd_t)

seutil_search_config(systemd_networkd_t)

# /sys/class/dmi/id/product_name
sysfs_read_generic_device(systemd_networkd_t)
# /sys/firmware/devicetree/base
sysfs_read_firmware(systemd_networkd_t)
sysfs_read_net(systemd_networkd_t)
# /sys/kernel/btf/vmlinux
sysfs_read_generic(systemd_networkd_t)

udev_read_runtime_files(systemd_networkd_t)


optional_policy(`
	dbus_system_bus_service(systemd_networkd_t)

	systemd_hostname_dbus_chat(systemd_networkd_t)

	dbus_watch_runtime_dirs(systemd_networkd_t)
	dbus_watch_runtime_sockets(systemd_networkd_t)
')


########################################
#
# Policy for systemd-networkd-wait-online
#

allow systemd_networkd_wait_online_t self:netlink_route_socket { bind create getattr getopt nlmsg_read read setopt write };
allow systemd_networkd_wait_online_t self:unix_dgram_socket { connect create getopt setopt };

allow systemd_networkd_wait_online_t systemd_networkd_runtime_t:dir { list_dir_perms watch };
allow systemd_networkd_wait_online_t systemd_networkd_runtime_t:file read_file_perms;


systemd_read_state(systemd_networkd_wait_online_t)


kernel_getattr_proc(systemd_networkd_wait_online_t)
kernel_read_system_state(systemd_networkd_wait_online_t)
kernel_read_cmdline(systemd_networkd_wait_online_t)
kernel_read_public_kernel_sysctls(systemd_networkd_wait_online_t)


fs_getattr_cgroup(systemd_networkd_wait_online_t)
fs_search_cgroup_dirs(systemd_networkd_wait_online_t)
fs_getattr_nsfs_files(systemd_networkd_wait_online_t)

logging_send_syslog_msg(systemd_networkd_wait_online_t)

selinux_getattr_fs(systemd_networkd_wait_online_t)

seutil_search_config(systemd_networkd_wait_online_t)
