## <summary>Policy for udev.</summary>
## <required val="true" />

########################################
## <summary>
##	Execute udev in the caller domain.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`udev_exec',`
	gen_require(`
		type udev_exec_t;
	')

	can_exec($1, udev_exec_t)
')

########################################
## <summary>
##	Connect to udev with a unix socket.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`udev_stream_connect',`
	gen_require(`
		type udev_t, udev_runtime_t;
	')

	files_search_runtime($1)
	stream_connect_pattern($1, udev_runtime_t, udev_runtime_t, udev_t)
')

########################################
## <summary>
##	Read the process state of udev.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`udev_read_state',`
	gen_require(`
		type udev_t;
	')

	read_process_pattern($1, udev_t)
')

########################################
## <summary>
##	Search the udev key ring.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`udev_search_key',`
	gen_require(`
		type udev_t;
	')

	allow $1 udev_t:key search;
')

########################################
## <summary>
##	Search udev runtime content.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`udev_search_runtime',`
	gen_require(`
		type udev_runtime_t;
	')

	files_search_runtime($1)
	search_dirs_pattern($1, udev_runtime_t, udev_runtime_t)
')

########################################
## <summary>
##	List and search udev runtime content.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`udev_list_runtime',`
	gen_require(`
		type udev_runtime_t;
	')

	files_search_runtime($1)
	allow $1 udev_runtime_t:dir list_dir_perms;
')

########################################
## <summary>
##	Read udev runtime files.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`udev_read_runtime_files',`
	gen_require(`
		type udev_runtime_t;
	')

	files_search_runtime($1)
	read_files_pattern($1, udev_runtime_t, udev_runtime_t)
')

########################################
## <summary>
##	Read udev runtime symbolic links.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`udev_read_runtime_symlinks',`
	gen_require(`
		type udev_runtime_t;
	')

	files_search_runtime($1)
	read_lnk_files_pattern($1, udev_runtime_t, udev_runtime_t)
')

########################################
## <summary>
##	Search the udev configuration directories.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`udev_search_config',`
	gen_require(`
		type udev_conf_t;
	')

	systemd_search_config($1)
	allow $1 udev_conf_t:dir search_dir_perms;
')

########################################
## <summary>
##	Read the udev config files.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`udev_read_config',`
	gen_require(`
		type udev_conf_t;
	')

	systemd_search_config($1)
	allow $1 udev_conf_t:dir list_dir_perms;
	allow $1 udev_conf_t:file read_file_perms;
')

########################################
## <summary>
##	List the udev rules.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`udev_list_rules',`
	gen_require(`
		type udev_rules_t;
	')

	allow $1 udev_rules_t:dir list_dir_perms;
')

########################################
## <summary>
##	Read the udev rules.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`udev_read_rules',`
	gen_require(`
		type udev_rules_t;
	')

	allow $1 udev_rules_t:dir list_dir_perms;
	allow $1 udev_rules_t:file read_file_perms;
')

########################################
## <summary>
##	Read the udev binary.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`udev_read_exec',`
	gen_require(`
		type udev_exec_t;
	')

	corecmd_search_bin($1)
	allow $1 udev_exec_t:file read_file_perms;
')

########################################
## <summary>
##	Reverse interface for systemd.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access (systemd_t).
##	</summary>
## </param>
## <param name="domain">
##	<summary>
##	Domain allowed access (systemd_generic_bin_t).
##	</summary>
## </param>
#
interface(`udev__systemd',`
	gen_require(`
		type udev_t, udev_runtime_t;
	')

	allow $1 udev_t:unix_stream_socket { bind create listen setopt };
	allow $1 udev_t:netlink_kobject_uevent_socket { bind create getopt setopt };

	allow $1 udev_runtime_t:dir { add_name relabelto remove_name write };
	allow $1 udev_runtime_t:lnk_file relabelto;
	allow $1 udev_runtime_t:file relabelto;
	allow $1 udev_runtime_t:sock_file { create getattr relabelto unlink write };
')
