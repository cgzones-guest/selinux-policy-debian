policy_module(systemd_login)

#########################################
#
# Declarations
#

type systemd_logind_t;
type systemd_logind_exec_t;
systemd_service_domain(systemd_logind_t, systemd_logind_exec_t)
# read user process state
rbac_process_subject_read_exemption(systemd_logind_t)
# accessible via dbus
rbac_public_system_dbus_object_exemption(systemd_logind_t)
rbac_dbus_system_subject_exemption(systemd_logind_t)
# accessible via fd
rbac_public_system_fd_object_exemption(systemd_logind_t)
# destroy user shared memory segments
rbac_sysvipc_subject_exemption(systemd_logind_t)


type systemd_logind_conf_t;
files_config_file(systemd_logind_conf_t)

type systemd_logind_var_lib_t;
files_state_file(systemd_logind_var_lib_t)

type systemd_logind_runtime_t;
files_runtime_file(systemd_logind_runtime_t)

type systemd_logind_inhibit_runtime_t;
files_runtime_file(systemd_logind_inhibit_runtime_t)

type systemd_logind_seats_runtime_t;
files_runtime_file(systemd_logind_seats_runtime_t)

type systemd_logind_users_runtime_t;
files_runtime_file(systemd_logind_users_runtime_t)

type systemd_logind_sessions_runtime_t;
files_runtime_file(systemd_logind_sessions_runtime_t)

type systemd_logind_unit_t;
systemd_service_unit_file(systemd_logind_unit_t)
optional_policy(`
	dbus_system_bus_started_unit(systemd_logind_unit_t)
')


#########################################
#
# systemd-logind policy
#

# sys_admin	: openat /dev/dri/card1
allow systemd_logind_t self:capability { chown dac_override fowner sys_admin sys_tty_config };
allow systemd_logind_t self:process { getcap setfscreate };

allow systemd_logind_t self:netlink_kobject_uevent_socket { bind create getattr getopt read setopt };

allow systemd_logind_t self:unix_dgram_socket create_socket_perms;
allow systemd_logind_t self:unix_stream_socket { bind connect create getattr getopt read setopt write };
allow systemd_logind_t self:fifo_file rw_fifo_file_perms;

allow systemd_logind_t systemd_logind_conf_t:file read_file_perms;

allow systemd_logind_t systemd_logind_var_lib_t:dir manage_dir_perms;
systemd_var_lib_filetrans(systemd_logind_t, systemd_logind_var_lib_t, dir)

allow systemd_logind_t systemd_logind_runtime_t:dir manage_dir_perms;
allow systemd_logind_t systemd_logind_runtime_t:file manage_file_perms;
allow systemd_logind_t systemd_logind_runtime_t:fifo_file manage_fifo_file_perms;

allow systemd_logind_t systemd_logind_inhibit_runtime_t:dir manage_dir_perms;
allow systemd_logind_t systemd_logind_inhibit_runtime_t:file manage_file_perms;
allow systemd_logind_t systemd_logind_inhibit_runtime_t:fifo_file manage_fifo_file_perms;
systemd_runtime_filetrans(systemd_logind_t, systemd_logind_inhibit_runtime_t, dir, "inhibit")

allow systemd_logind_t systemd_logind_seats_runtime_t:dir manage_dir_perms;
allow systemd_logind_t systemd_logind_seats_runtime_t:file manage_file_perms;
systemd_runtime_filetrans(systemd_logind_t, systemd_logind_seats_runtime_t, dir, "seats")

allow systemd_logind_t systemd_logind_users_runtime_t:dir rw_dir_perms;
allow systemd_logind_t systemd_logind_users_runtime_t:file manage_file_perms;
systemd_runtime_filetrans(systemd_logind_t, systemd_logind_users_runtime_t, dir, "users")

allow systemd_logind_t systemd_logind_sessions_runtime_t:dir rw_dir_perms;
allow systemd_logind_t systemd_logind_sessions_runtime_t:file manage_file_perms;
allow systemd_logind_t systemd_logind_sessions_runtime_t:fifo_file manage_fifo_file_perms;
systemd_runtime_filetrans(systemd_logind_t, systemd_logind_sessions_runtime_t, dir, "sessions")


systemd_state_system(systemd_logind_t)
systemd_start_system(systemd_logind_t)
systemd_stop_system(systemd_logind_t)
systemd_use_notify(systemd_logind_t)
systemd_use_userdb(systemd_logind_t)
systemd_read_state(systemd_logind_t)
systemd_startstop_session_units(systemd_logind_t)
systemd_startstop_transient_units(systemd_logind_t)
# acpi shutdown
systemd_start_power_units(systemd_logind_t)
# poweroff.target
systemd_state_system_targets(systemd_logind_t)
systemd_start_system_targets(systemd_logind_t)

systemd_machine_use_userdb(systemd_logind_t)

systemd_sleep_read_config(systemd_logind_t)

systemd_user_instance_read_state(systemd_logind_t)

systemd_user_runtime_dir_startstop_units(systemd_logind_t)


kernel_read_public_kernel_sysctls(systemd_logind_t)
kernel_read_system_state(systemd_logind_t)
kernel_read_sysvipc_state(systemd_logind_t)
kernel_read_cmdline(systemd_logind_t)
kernel_getattr_proc(systemd_logind_t)


auth_read_utmp(systemd_logind_t)
auth_watch_utmp(systemd_logind_t)
auth_read_all_pgm_domain_states(systemd_logind_t)

dev_getattr_dma_dev(systemd_logind_t)
dev_getattr_dri_dev(systemd_logind_t)
dev_getattr_generic_usb_dev(systemd_logind_t)
dev_getattr_kvm_dev(systemd_logind_t)
dev_getattr_sound_dev(systemd_logind_t)
dev_getattr_video_dev(systemd_logind_t)
dev_getattr_wireless_dev(systemd_logind_t)
dev_read_urand(systemd_logind_t)
dev_setattr_dri_dev(systemd_logind_t)
dev_setattr_generic_usb_dev(systemd_logind_t)
dev_setattr_kvm_dev(systemd_logind_t)
dev_setattr_sound_dev(systemd_logind_t)
dev_setattr_video_dev(systemd_logind_t)
dev_setattr_wireless_dev(systemd_logind_t)

dev_rw_dri(systemd_logind_t)
dev_control_dri(systemd_logind_t, { ioctl_drm_ioctl_set_master ioctl_drm_ioctl_drop_master })

dev_rw_input_dev(systemd_logind_t)
dev_control_input_dev(systemd_logind_t, { ioctl_eviocgname ioctl_eviocgsw ioctl_eviocgbit ioctl_eviocsmask ioctl_eviocrevoke ioctl_eviocgkey })

# /proc/<pid>/cgroup
domain_read_interactive_fds(systemd_logind_t)
domain_use_interactive_fds(systemd_logind_t)

# check for login session leftovers
files_getattr_all_shmfs_files(systemd_logind_t)
files_search_boot(systemd_logind_t)
files_search_var_lib(systemd_logind_t)

fs_getattr_xattr_fs(systemd_logind_t)
fs_getattr_cgroup(systemd_logind_t)
fs_read_cgroup_files(systemd_logind_t)
# TODO: /sys/fs/cgroup/system.slice/systemd-logind.service/memory.pressure
fs_read_cgroup_systemslice_files(systemd_logind_t)
fs_dontaudit_write_cgroup_systemslice_files(systemd_logind_t)
fs_getattr_dos_fs(systemd_logind_t)
# /boot/efi/loader/addons/
fs_list_dos(systemd_logind_t)
fs_read_efivarfs_files(systemd_logind_t)
fs_getattr_nsfs_files(systemd_logind_t)
fs_getattr_tmpfs(systemd_logind_t)
fs_mount_tmpfs(systemd_logind_t)
fs_unmount_tmpfs(systemd_logind_t)
fs_list_mqueuefs(systemd_logind_t)
fs_list_shmfs(systemd_logind_t)
fs_delete_generic_shmfs_dir_entries(systemd_logind_t)

logging_send_syslog_msg(systemd_logind_t)

miscfiles_read_config(systemd_logind_t)
miscfiles_read_localization(systemd_logind_t)

selinux_getattr_fs(systemd_logind_t)
selinux_map_status(systemd_logind_t)

seutil_read_config(systemd_logind_t)
seutil_read_file_contexts(systemd_logind_t)
seutil_read_newrole_state(systemd_logind_t)

# /dev/dm-2
storage_getattr_fixed_disk_dev(systemd_logind_t)

# ACL
storage_getattr_removable_dev(systemd_logind_t)
storage_setattr_removable_dev(systemd_logind_t)
storage_getattr_scsi_generic_dev(systemd_logind_t)
storage_setattr_scsi_generic_dev(systemd_logind_t)

# /sys/class/drm/version
sysfs_read_generic(systemd_logind_t)
# /sys/class/tty/tty0/active
sysfs_read_generic_device(systemd_logind_t)
# /sys/firmware/efi/
sysfs_search_firmware(systemd_logind_t)
# /sys/power/state
sysfs_rw_power(systemd_logind_t)
# /sys/block/sda/dev
sysfs_read_harddrive(systemd_logind_t)
# /sys/module/vt/parameters/default_utf8
sysfs_read_modules(systemd_logind_t)

term_use_unallocated_ttys(systemd_logind_t)
term_setattr_unallocated_ttys(systemd_logind_t)
term_control_unallocated_ttys(systemd_logind_t, { ioctl_kdsetmode ioctl_kdskbmode ioctl_vt_setmode ioctl_vt_reldisp ioctl_vt_activate })
term_search_ptys(systemd_logind_t)

# list /run/udev/tags/uaccess
udev_list_runtime(systemd_logind_t)
udev_read_runtime_files(systemd_logind_t)
udev_read_runtime_symlinks(systemd_logind_t)

userdom_destroy_all_userapps_ipc_objects(systemd_logind_t)
userdom_use_user_ptys(systemd_logind_t)
userdom_use_user_ttys(systemd_logind_t)
userdom_setattr_user_ttys(systemd_logind_t)
userdom_control_user_ttys(systemd_logind_t, { ioctl_kdsetmode ioctl_kdskbmode ioctl_vt_setmode ioctl_vt_getstate ioctl_vt_reldisp })


optional_policy(`
	dbus_system_bus_service(systemd_logind_t)
	dbus_use_system_bus_fds(systemd_logind_t)

	systemd_dbus_chat(systemd_logind_t)

	systemd_network_dbus_chat(systemd_logind_t)

	optional_policy(`
		networkmanager_dbus_chat_bidirectional(systemd_logind_t)
	')

	optional_policy(`
		packagekit_dbus_chat(systemd_logind_t)
	')

	optional_policy(`
		policykit_dbus_chat(systemd_logind_t)
	')

	optional_policy(`
		udisks_dbus_chat(systemd_logind_t)
	')

	optional_policy(`
		upower_dbus_chat_bidirectional(systemd_logind_t)
	')
')

optional_policy(`
	getty_start(systemd_logind_t)
')

optional_policy(`
	policykit_client(systemd_logind_t)
')

optional_policy(`
	su_read_all_states(systemd_logind_t)
')
