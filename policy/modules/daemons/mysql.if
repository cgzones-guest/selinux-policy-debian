## <summary>Open source database.</summary>

########################################
## <summary>
##	Execute mysqld in the caller domain.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`mysql_exec_mysqld',`
	gen_require(`
		type mysqld_exec_t;
	')

	corecmd_search_bin($1)
	can_exec($1, mysqld_exec_t)
')

########################################
## <summary>
##	Execute mariadb-install-db in the caller domain.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`mysql_exec_installdb',`
	gen_require(`
		type mysqld_installdb_exec_t;
	')

	corecmd_search_bin($1)
	can_exec($1, mysqld_installdb_exec_t)
')

########################################
## <summary>
##	Send generic signals to mysqld.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`mysql_signal',`
	gen_require(`
		type mysqld_t;
	')

	allow $1 mysqld_t:process signal;
')

########################################
## <summary>
##	Connect to mysqld with a unix
##	domain stream socket.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`mysql_stream_connect',`
	gen_require(`
		type mysqld_t, mysqld_db_t, mysqld_runtime_t;
	')

	files_search_runtime($1)
	stream_connect_pattern($1, { mysqld_db_t mysqld_runtime_t }, mysqld_runtime_t, mysqld_t)
')

########################################
## <summary>
##	Read the mysql databases.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`mysql_read_db',`
	gen_require(`
		type mysqld_db_t;
	')
	allow $1 mysqld_db_t:dir list_dir_perms;
	allow $1 mysqld_db_t:file mmap_read_file_perms;
')

########################################
## <summary>
##	Read mysqld configuration content.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`mysql_read_config',`
	gen_require(`
		type mysqld_conf_t;
	')

	files_search_etc($1)
	allow $1 mysqld_conf_t:dir list_dir_perms;
	allow $1 mysqld_conf_t:file read_file_perms;
	allow $1 mysqld_conf_t:lnk_file read_lnk_file_perms;
')

########################################
## <summary>
##	Manage mysqld configuration symlinks.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`mysql_manage_config_symlinks',`
	gen_require(`
		type mysqld_conf_t;
	')

	files_search_etc($1)
	allow $1 mysqld_conf_t:dir rw_dir_perms;
	allow $1 mysqld_conf_t:lnk_file manage_lnk_file_perms;
')

########################################
## <summary>
##	Ignore attempts to read mysqld home files.
## </summary>
## <param name="domain">
##	<summary>
##	Domain to not audit.
##	</summary>
## </param>
#
interface(`mysql_dontaudit_read_mysqld_home_files',`
	gen_require(`
		type mysqld_home_t;
	')

	dontaudit $1 mysqld_home_t:file read_file_perms;
')

########################################
## <summary>
##	Create, read, write, and delete
##	mysqld home files.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`mysql_manage_mysqld_home_files',`
	gen_require(`
		type mysqld_home_t;
	')

	userdom_search_user_home_dirs($1)
	allow $1 mysqld_home_t:file manage_file_perms;
')

########################################
## <summary>
##	Relabel mysqld home files.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`mysql_relabel_mysqld_home_files',`
	gen_require(`
		type mysqld_home_t;
	')

	userdom_search_user_home_dirs($1)
	allow $1 mysqld_home_t:file relabel_file_perms;
')

########################################
## <summary>
##	Create objects in user home
##	directories with the mysqld home type.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
## <param name="object_class">
##	<summary>
##	Class of the object being created.
##	</summary>
## </param>
## <param name="filename" optional="true">
##	<summary>
##	The name of the object being created.
##	</summary>
## </param>
#
interface(`mysql_home_filetrans_mysqld_home',`
	gen_require(`
		type mysqld_home_t;
	')

	userdom_user_home_dir_filetrans($1, mysqld_home_t, $2, $3)
')

########################################
## <summary>
##	Reverse interface for systemd.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access (systemd_t).
##	</summary>
## </param>
## <param name="domain">
##	<summary>
##	Domain allowed access (systemd_generic_bin_t).
##	</summary>
## </param>
#
interface(`mysql__systemd',`
	gen_require(`
		type mysqld_conf_t, mysqld_runtime_t;
	')

	allow $1 mysqld_conf_t:dir search;

	files_runtime_filetrans($2, mysqld_runtime_t, dir, "mysqld")
	allow $2 mysqld_runtime_t:dir { create_dir_perms list_dir_perms setattr };
')

########################################
## <summary>
##	All of the rules required to
##	administrate an mysqld environment.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
## <param name="role">
##	<summary>
##	Role allowed access.
##	</summary>
## </param>
#
interface(`mysql__admin',`
	gen_require(`
		type mysqld_t, mysqld_conf_t, mysqld_db_t;
		type mysqld_home_t, mysqld_initrc_exec_t, mysqld_installdb_exec_t;
		type mysqld_log_t;
		type mysqld_runtime_t, mysqld_safe_t, mysqld_tmp_t;
		type mysqld_unit_t;
	')

	admin_service_pattern($1, { mysqld_safe_t mysqld_t }, mysqld_initrc_exec_t, mysqld_unit_t)

	mysql_stream_connect($1)

	dontaudit $1 mysqld_installdb_exec_t:file execute;

	files_search_runtime($1)
	admin_pattern($1, mysqld_runtime_t)

	files_search_var_lib($1)
	admin_pattern($1, mysqld_db_t)

	files_search_etc($1)
	admin_pattern($1, { mysqld_conf_t mysqld_home_t })

	logging_search_logs($1)
	admin_pattern($1, mysqld_log_t)

	files_search_tmp($1)
	admin_pattern($1, mysqld_tmp_t)
')
