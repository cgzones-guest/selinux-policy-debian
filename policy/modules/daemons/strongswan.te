policy_module(strongswan)

########################################
#
# Declarations
#

type strongswan_mgmt_t;
type strongswan_mgmt_exec_t;
systemd_service_domain(strongswan_mgmt_t, strongswan_mgmt_exec_t)

type charon_systemd_t;
type charon_systemd_exec_t;
systemd_service_domain(charon_systemd_t, charon_systemd_exec_t)


# type for strongswan configuration file(s) - not for keys
type strongswan_conf_t;
files_security_file(strongswan_conf_t)

# type for file(s) containing strongswan keys - RSA or preshared
type strongswan_key_t;
files_security_file(strongswan_key_t)

# type for runtime files, including charon.ctl
type strongswan_runtime_t;
files_runtime_file(strongswan_runtime_t)

type strongswan_unit_t;
systemd_service_unit_file(strongswan_unit_t)


########################################
#
# Policy for charon-systemd
#

allow charon_systemd_t self:capability { net_admin net_bind_service net_raw };
allow charon_systemd_t self:process { getcap setcap signal };
allow charon_systemd_t self:fifo_file { read write };
allow charon_systemd_t self:netlink_route_socket { bind create getattr nlmsg_read nlmsg_write read setopt write };
allow charon_systemd_t self:netlink_xfrm_socket { bind create nlmsg_read nlmsg_write read setopt write };
xperm_pattern(charon_systemd_t, self, netlink_xfrm_socket, { ioctl_siocethtool })
allow charon_systemd_t self:packet_socket { create read setopt };
xperm_pattern(charon_systemd_t, self, packet_socket, { ioctl_siocgifname ioctl_siocgifhwaddr })
allow charon_systemd_t self:rawip_socket { create setopt };
xperm_pattern(charon_systemd_t, self, rawip_socket, { ioctl_siocgifindex })
allow charon_systemd_t self:alg_socket { bind create };
allow charon_systemd_t self:udp_socket { bind create getattr getopt read setopt write };
allow charon_systemd_t self:unix_dgram_socket { connect create getopt setopt write };
allow charon_systemd_t self:unix_stream_socket { accept bind create listen read write };

allow charon_systemd_t strongswan_conf_t:dir list_dir_perms;
allow charon_systemd_t strongswan_conf_t:file read_file_perms;

allow charon_systemd_t strongswan_runtime_t:sock_file { create setattr unlink };
files_runtime_filetrans(charon_systemd_t, strongswan_runtime_t, sock_file)


kernel_request_load_module(charon_systemd_t)
# /proc/sys/net/core/xfrm_acq_expires
kernel_rw_net_sysctls(charon_systemd_t)
kernel_getattr_proc(charon_systemd_t)
kernel_read_public_crypto_sysctls(charon_systemd_t)
kernel_read_public_kernel_sysctls(charon_systemd_t)


corecmd_search_bin(charon_systemd_t)

corenet_send_generic_node(charon_systemd_t)
corenet_send_stdextern_if(charon_systemd_t)

corenet_udp_bind_anyaddr_node(charon_systemd_t)
corenet_udp_bind_ipsecnat_port(charon_systemd_t)
corenet_udp_bind_isakmp_port(charon_systemd_t)

corenet_sendrecv_ipsecnat_server_packets(charon_systemd_t)
corenet_sendrecv_isakmp_server_packets(charon_systemd_t)

corenet_udp_bind_dhcpc_port(charon_systemd_t)
corenet_receive_dns_client_packets(charon_systemd_t)
corenet_receive_dns_server_packets(charon_systemd_t)
corenet_receive_dns_lo_client_packets(charon_systemd_t)
corenet_receive_dns_lo_server_packets(charon_systemd_t)

dev_read_rand(charon_systemd_t)
dev_read_urand(charon_systemd_t)

logging_send_syslog_msg(charon_systemd_t)

miscfiles_read_config(charon_systemd_t)
miscfiles_read_localization(charon_systemd_t)

systemd_use_notify(charon_systemd_t)


########################################
#
# strongswan_mgmt local policy
#

allow strongswan_mgmt_t self:process { getcap signal };
allow strongswan_mgmt_t self:fifo_file rw_fifo_file_perms;
allow strongswan_mgmt_t self:alg_socket { bind create };
allow strongswan_mgmt_t self:unix_stream_socket { connect create read write };

allow strongswan_mgmt_t charon_systemd_t:unix_stream_socket connectto;

allow strongswan_mgmt_t strongswan_conf_t:dir list_dir_perms;
allow strongswan_mgmt_t strongswan_conf_t:file mmap_read_file_perms;

allow strongswan_mgmt_t strongswan_key_t:dir list_dir_perms;
allow strongswan_mgmt_t strongswan_key_t:file mmap_read_file_perms;

allow strongswan_mgmt_t strongswan_runtime_t:sock_file write;


kernel_read_public_crypto_sysctls(strongswan_mgmt_t)


corecmd_exec_bin(strongswan_mgmt_t)
corecmd_exec_shell(strongswan_mgmt_t)

dev_read_rand(strongswan_mgmt_t)
dev_read_urand(strongswan_mgmt_t)

files_search_runtime(strongswan_mgmt_t)

miscfiles_read_config(strongswan_mgmt_t)
miscfiles_read_localization(strongswan_mgmt_t)
