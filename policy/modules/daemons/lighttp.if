## <summary>Lighttp - fast webserver with minimal memory footprint.</summary>

########################################
## <summary>
##	Start and stop lighttpd (systemd).
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`lighttp_startstop',`
	gen_require(`
		class service { start status stop };
		type lighttp_unit_t;
	')

	allow $1 lighttp_unit_t:service { reload start status stop };
')

########################################
## <summary>
##	Read an mmap lighttpd generic content.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`lighttp_mmap_read_content',`
	gen_require(`
		type lighttp_content_t;
	')

	miscfiles_search_web_root($1)
	allow $1 lighttp_content_t:dir list_dir_perms;
	allow $1 lighttp_content_t:file mmap_read_file_perms;
')

########################################
## <summary>
##	Lighttp process feedback permissions.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`lighttp_feedback_client',`
	gen_require(`
		type lighttpd_t;
	')

	allow $1 lighttpd_t:fd use;
	allow $1 lighttpd_t:fifo_file { getattr ioctl write };
	allow $1 lighttpd_t:unix_stream_socket { getattr read write };
	xperm_pattern($1, lighttpd_t, unix_stream_socket, ioctl_tcgets)
')

########################################
## <summary>
##	Reverse interface for systemd.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access (systemd_t).
##	</summary>
## </param>
## <param name="domain">
##	<summary>
##	Domain allowed access (systemd_generic_bin_t).
##	</summary>
## </param>
#
interface(`lighttp__systemd',`
	gen_require(`
		type lighttp_cache_t, lighttp_runtime_t;
	')

	allow $1 lighttp_cache_t:dir { getattr mounton };
	allow $1 lighttp_runtime_t:file { getattr ioctl open read unlink };

	allow $2 lighttp_cache_t:dir { del_entry_dir_perms read };
	allow $2 lighttp_cache_t:file delete_file_perms;
')

########################################
## <summary>
##	Reverse interface for systemd-tmpfiles.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`lighttp__systemdtmpfiles',`
	gen_require(`
		type lighttp_cache_t, lighttp_log_t, lighttp_runtime_t;
	')

	allow $1 lighttp_cache_t:dir { getattr open read relabelfrom relabelto search };
	allow $1 lighttp_log_t:dir { getattr open read relabelfrom relabelto };
	allow $1 lighttp_runtime_t:dir { create getattr open read relabelfrom relabelto setattr };
')
