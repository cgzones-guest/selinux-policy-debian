## <summary>rsyslogd - reliable and extended syslogd.</summary>

########################################
## <summary>
##	Stop the rsyslog daemon.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`rsyslog_stop',`
	gen_require(`
		class service stop;
		type rsyslog_unit_t;
	')
	allow $1 rsyslog_unit_t:service stop;
')

########################################
## <summary>
##	Reverse interface for systemd.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access (systemd_t).
##	</summary>
## </param>
## <param name="domain">
##	<summary>
##	Domain allowed access (systemd_generic_bin_t).
##	</summary>
## </param>
#
interface(`rsyslog__systemd',`
	gen_require(`
		type rsyslog_t;
	')

	allow $1 rsyslog_t:unix_dgram_socket { accept bind create getopt read setopt };
')

########################################
## <summary>
##	All of the rules required to
##	administrate an rsyslog environment.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
## <param name="role">
##	<summary>
##	Role allowed access.
##	</summary>
## </param>
#
interface(`rsyslog__admin',`
	gen_require(`
		type rsyslog_t, rsyslog_conf_t, rsyslog_initrc_exec_t;
		type rsyslog_unit_t;
	')

	admin_service_pattern($1, rsyslog_t, rsyslog_initrc_exec_t, rsyslog_unit_t)

	files_search_etc($1)
	admin_pattern($1, rsyslog_conf_t)
')
