policy_module(fail2ban)

########################################
#
# Declarations
#

type fail2ban_server_t;
type fail2ban_server_exec_t;
systemd_service_domain(fail2ban_server_t, fail2ban_server_exec_t)
# accessible by users
rbac_public_system_unixsocket_object_exemption(fail2ban_server_t)

type fail2ban_client_t;
type fail2ban_client_exec_t;
systemd_service_domain(fail2ban_client_t, fail2ban_client_exec_t)


type fail2ban_conf_t;
files_config_file(fail2ban_conf_t)

type fail2ban_initrc_exec_t;
systemd_service_initrc_file(fail2ban_initrc_exec_t)

type fail2ban_log_t;
logging_log_file(fail2ban_log_t)

type fail2ban_runtime_t;
files_runtime_file(fail2ban_runtime_t)

type fail2ban_state_t;
files_state_file(fail2ban_state_t)

type fail2ban_unit_t;
systemd_service_unit_file(fail2ban_unit_t)


########################################
#
# Policy for fail2ban server
#

# sys_admin	: modify firewall
allow fail2ban_server_t self:capability net_admin;
allow fail2ban_server_t self:process getsched;
# grep -qP ^\s+set\s+
dontaudit fail2ban_server_t self:process execmem;
allow fail2ban_server_t self:fifo_file { getattr ioctl read write };
allow fail2ban_server_t self:netlink_netfilter_socket { create getopt read setopt write };
allow fail2ban_server_t self:unix_stream_socket { accept bind connect connectto create getattr getopt listen read setopt shutdown write };

allow fail2ban_server_t fail2ban_conf_t:dir list_dir_perms;
allow fail2ban_server_t fail2ban_conf_t:file read_file_perms;

allow fail2ban_server_t fail2ban_log_t:file { append create getattr ioctl open };
logging_log_filetrans(fail2ban_server_t, fail2ban_log_t, file)

allow fail2ban_server_t fail2ban_runtime_t:dir rw_dir_perms;
allow fail2ban_server_t fail2ban_runtime_t:file manage_file_perms;
allow fail2ban_server_t fail2ban_runtime_t:sock_file { create getattr unlink write };

allow fail2ban_server_t fail2ban_state_t:dir rw_dir_perms;
allow fail2ban_server_t fail2ban_state_t:file manage_file_perms;


kernel_read_system_state(fail2ban_server_t)
kernel_read_public_net_sysctls(fail2ban_server_t)
kernel_read_vm_overcommit_sysctl(fail2ban_server_t)


# python
corecmd_exec_bin(fail2ban_server_t)
corecmd_exec_shell(fail2ban_server_t)

dev_read_urand(fail2ban_server_t)

files_search_runtime(fail2ban_server_t)
files_search_var_lib(fail2ban_server_t)

fs_getattr_tmpfs(fail2ban_server_t)
fs_getattr_xattr_fs(fail2ban_server_t)

libs_exec_ldconfig(fail2ban_server_t)
# /usr/lib/python3/dist-packages/systemd/__pycache__/
libs_dontaudit_write_lib_dirs(fail2ban_server_t)

logging_watch_generic_log_dirs(fail2ban_server_t)
logging_watch_generic_logs(fail2ban_server_t)
# /var/log/auth.log
logging_read_generic_logs(fail2ban_server_t)

miscfiles_read_config(fail2ban_server_t)
miscfiles_read_localization(fail2ban_server_t)

selinux_getattr_fs(fail2ban_server_t)

seutil_search_config(fail2ban_server_t)

sysfs_read_cpu_possible(fail2ban_server_t)

sysnet_dns_name_resolve(fail2ban_server_t)

systemd_getattr_exec(fail2ban_server_t)

systemd_journal_watch_journal_dirs(fail2ban_server_t)
systemd_journal_read_journal(fail2ban_server_t)


optional_policy(`
	apache2_monitor_logs(fail2ban_server_t)
')

optional_policy(`
	iptables_exec(fail2ban_server_t)

	# nft: /usr/share/iproute2/rt_realms
	files_read_usr_files(fail2ban_server_t)
')

optional_policy(`
	monit_read_logs(fail2ban_server_t)
	monit_watch_log_files(fail2ban_server_t)
')

optional_policy(`
	quassel_read_logs(fail2ban_server_t)
	quassel_watch_logs(fail2ban_server_t)
')


########################################
#
# Policy for fail2ban client
#

allow fail2ban_client_t self:process getsched;
allow fail2ban_client_t self:unix_stream_socket { connect create read shutdown write };

allow fail2ban_client_t fail2ban_conf_t:dir list_dir_perms;
allow fail2ban_client_t fail2ban_conf_t:file read_file_perms;

allow fail2ban_client_t fail2ban_runtime_t:dir search_dir_perms;
allow fail2ban_client_t fail2ban_runtime_t:sock_file write;

allow fail2ban_client_t fail2ban_server_t:unix_stream_socket connectto;


kernel_read_system_state(fail2ban_client_t)
kernel_read_vm_overcommit_sysctl(fail2ban_client_t)


# python
corecmd_exec_bin(fail2ban_client_t)

domain_use_interactive_fds(fail2ban_client_t)

files_search_runtime(fail2ban_client_t)
files_search_var(fail2ban_client_t)

# /usr/lib/python3/dist-packages/fail2ban/server/__pycache__/
libs_dontaudit_write_lib_dirs(fail2ban_client_t)

miscfiles_read_config(fail2ban_client_t)
miscfiles_read_localization(fail2ban_client_t)

sysfs_read_cpu_possible(fail2ban_client_t)

userdom_use_inherited_user_terminals(fail2ban_client_t)
# ~/.local/lib/python3.9/site-packages
userdom_dontaudit_search_user_home_dirs(fail2ban_client_t)


optional_policy(`
	apache2_getattr_logs(fail2ban_client_t)
')

optional_policy(`
	monit_getattr_logs(fail2ban_client_t)
')

optional_policy(`
	quassel_getattr_logs(fail2ban_client_t)
')
