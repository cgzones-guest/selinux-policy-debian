policy_module(pihole)

########################################
#
# Declarations
#

type pihole_t;
type pihole_exec_t;
application_domain(pihole_t, pihole_exec_t)
# run by timer
systemd_service_domain(pihole_t, pihole_exec_t)
# read process state of pihole-FTL
rbac_process_subject_read_exemption(pihole_t)

type pihole_ftl_t;
type pihole_ftl_exec_t;
systemd_service_domain(pihole_ftl_t, pihole_ftl_exec_t)

type pihole_ftl_poststop_t;
type pihole_ftl_poststop_exec_t;
systemd_service_domain(pihole_ftl_poststop_t, pihole_ftl_poststop_exec_t)

type pihole_ftl_prestart_t;
type pihole_ftl_prestart_exec_t;
systemd_service_domain(pihole_ftl_prestart_t, pihole_ftl_prestart_exec_t)


type pihole_conf_t;
files_config_file(pihole_conf_t)

type pihole_helper_exec_t;
corecmd_executable_file(pihole_helper_exec_t)

type pihole_initrc_exec_t;
systemd_service_initrc_file(pihole_initrc_exec_t)

type pihole_log_t;
logging_log_file(pihole_log_t)

type pihole_repo_t;
files_type(pihole_repo_t)

type pihole_runtime_t;
files_runtime_file(pihole_runtime_t)

type pihole_shmfs_t;
files_shmfs_file(pihole_shmfs_t)

type pihole_state_t;
files_state_file(pihole_state_t)

type pihole_tmp_t;
files_tmp_file(pihole_tmp_t)

type pihole_unit_t;
systemd_service_unit_file(pihole_unit_t)


########################################
#
# Policy for pihole-FTL
#

# sys_nice	: setpriority
allow pihole_ftl_t self:capability { net_admin net_raw sys_nice };
allow pihole_ftl_t self:process { execmem getcap setsched signal };
allow pihole_ftl_t self:fifo_file { getattr open read write };
xperm_pattern(pihole_ftl_t, self, fifo_file, { ioctl_tcgets })
allow pihole_ftl_t self:netlink_route_socket { bind create getattr nlmsg_read read setopt write };
allow pihole_ftl_t self:rawip_socket { create read setopt write };
allow pihole_ftl_t self:tcp_socket { accept bind create getattr getopt listen read setopt shutdown write };
allow pihole_ftl_t self:udp_socket { bind connect create getattr read setopt write };
allow pihole_ftl_t self:unix_dgram_socket { connect create };
allow pihole_ftl_t self:unix_stream_socket { accept bind connect create listen };
xperm_pattern(pihole_ftl_t, self, tcp_socket, ioctl_siocgifname)
xperm_pattern(pihole_ftl_t, self, udp_socket, {
	ioctl_siocgifname
	ioctl_siocgifflags
	ioctl_siocgifmtu
	ioctl_siocgstamp
	ioctl_siocgifaddr
	ioctl_siocsarp
})
xperm_pattern(pihole_ftl_t, self, unix_dgram_socket, ioctl_siocgifindex)


allow pihole_ftl_t pihole_conf_t:dir rw_dir_perms;
allow pihole_ftl_t pihole_conf_t:file read_file_perms;

allow pihole_ftl_t pihole_log_t:dir add_entry_dir_perms;
allow pihole_ftl_t pihole_log_t:file { append create getattr open read };

allow pihole_ftl_t pihole_runtime_t:dir rw_dir_perms;
allow pihole_ftl_t pihole_runtime_t:file rw_file_perms;
allow pihole_ftl_t pihole_runtime_t:sock_file { create unlink };
files_runtime_filetrans(pihole_ftl_t, pihole_runtime_t, { dir file })

allow pihole_ftl_t pihole_shmfs_t:file { create map open read unlink write };
fs_shmfs_filetrans(pihole_ftl_t, pihole_shmfs_t, file)

allow pihole_ftl_t pihole_state_t:file manage_file_perms;
filetrans_pattern(pihole_ftl_t, pihole_conf_t, pihole_state_t, file)


kernel_read_system_state(pihole_ftl_t)
kernel_read_network_state(pihole_ftl_t)
kernel_read_vm_overcommit_sysctl(pihole_ftl_t)
kernel_list_proc(pihole_ftl_t)
kernel_dontaudit_read_state(pihole_ftl_t)


# sh -c ip neigh show
corecmd_exec_shell(pihole_ftl_t)

corenet_send_stdextern_if(pihole_ftl_t)
corenet_send_lo_if(pihole_ftl_t)
corenet_send_generic_node(pihole_ftl_t)
corenet_send_lo_node(pihole_ftl_t)

corenet_tcp_bind_anyaddr_node(pihole_ftl_t)
corenet_udp_bind_anyaddr_node(pihole_ftl_t)
corenet_tcp_bind_lo_node(pihole_ftl_t)

corenet_tcp_bind_unreserved_ports(pihole_ftl_t)
corenet_udp_bind_unreserved_ports(pihole_ftl_t)
corenet_dontaudit_udp_bind_defined_ports(pihole_ftl_t)
corenet_tcp_bind_dns_port(pihole_ftl_t)
corenet_udp_bind_dns_port(pihole_ftl_t)
corenet_udp_bind_dhcpd_port(pihole_ftl_t)
corenet_tcp_bind_pihole_port(pihole_ftl_t)

corenet_sendrecv_dhcpd_server_packets(pihole_ftl_t)
corenet_send_dhcpc_client_packets(pihole_ftl_t)

corenet_sendrecv_dns_server_packets(pihole_ftl_t)
corenet_sendrecv_dns_lo_server_packets(pihole_ftl_t)
corenet_sendrecv_dns_lo_client_packets(pihole_ftl_t)
corenet_sendrecv_pihole_lo_server_packets(pihole_ftl_t)

corenet_sendrecv_icmp_packets(pihole_ftl_t)
corenet_receive_icmp_lo_packets(pihole_ftl_t)

dev_read_urand(pihole_ftl_t)

domain_dontaudit_read_all_domains_state(pihole_ftl_t)

files_read_usr_files(pihole_ftl_t)
files_search_runtime(pihole_ftl_t)
files_search_var(pihole_ftl_t)

fs_getattr_tmpfs(pihole_ftl_t)
fs_search_tmpfs(pihole_ftl_t)
fs_getattr_xattr_fs(pihole_ftl_t)

logging_search_logs(pihole_ftl_t)

miscfiles_read_config(pihole_ftl_t)
miscfiles_read_localization(pihole_ftl_t)

# ip address show
selinux_getattr_fs(pihole_ftl_t)

# ip address show
seutil_search_config(pihole_ftl_t)

# ip neigh show
sysnet_exec_ifconfig(pihole_ftl_t)
sysnet_read_config(pihole_ftl_t)


optional_policy(`
	dnsmasq_read_config(pihole_ftl_t)
')


########################################
#
# Policy for pihole-FTL-poststop.sh
#

# dac_override	: rmdir /run/pihole/
allow pihole_ftl_poststop_t self:capability dac_override;
allow pihole_ftl_poststop_t self:fifo_file { getattr read write };

allow pihole_ftl_poststop_t pihole_conf_t:dir search_dir_perms;
allow pihole_ftl_poststop_t pihole_conf_t:file read_file_perms;

allow pihole_ftl_poststop_t pihole_helper_exec_t:file read_file_perms;

allow pihole_ftl_poststop_t pihole_runtime_t:dir { remove_name search write };
allow pihole_ftl_poststop_t pihole_runtime_t:file { getattr unlink };
allow pihole_ftl_poststop_t pihole_runtime_t:sock_file { getattr unlink };

allow pihole_ftl_poststop_t pihole_shmfs_t:file delete_file_perms;


corecmd_exec_bin(pihole_ftl_poststop_t)
corecmd_exec_shell(pihole_ftl_poststop_t)

files_rw_generic_runtime_dirs(pihole_ftl_poststop_t)

fs_delete_generic_shmfs_dir_entries(pihole_ftl_poststop_t)
fs_list_shmfs(pihole_ftl_poststop_t)

miscfiles_read_localization(pihole_ftl_poststop_t)
miscfiles_setattr_localization(pihole_ftl_poststop_t)


########################################
#
# Policy for pihole-FTL-prestart.sh
#

allow pihole_ftl_prestart_t self:capability { chown fowner };
allow pihole_ftl_prestart_t self:fifo_file { getattr read write };
allow pihole_ftl_prestart_t self:unix_stream_socket { connect create };

allow pihole_ftl_prestart_t pihole_conf_t:dir { search_dir_perms setattr };
allow pihole_ftl_prestart_t pihole_conf_t:file { read_file_perms setattr };

allow pihole_ftl_prestart_t pihole_helper_exec_t:file read_file_perms;

allow pihole_ftl_prestart_t pihole_log_t:dir { search_dir_perms setattr };
allow pihole_ftl_prestart_t pihole_log_t:lnk_file read_lnk_file_perms;
allow pihole_ftl_prestart_t pihole_log_t:file { getattr setattr };

allow pihole_ftl_prestart_t pihole_runtime_t:dir { getattr setattr };
allow pihole_ftl_prestart_t pihole_runtime_t:file { create getattr ioctl open relabelfrom relabelto setattr write };
files_runtime_filetrans(pihole_ftl_prestart_t, pihole_runtime_t, file)

allow pihole_ftl_prestart_t pihole_state_t:file { getattr setattr };


corecmd_exec_bin(pihole_ftl_prestart_t)
corecmd_exec_shell(pihole_ftl_prestart_t)

files_search_runtime(pihole_ftl_prestart_t)

logging_search_logs(pihole_ftl_prestart_t)

miscfiles_read_config(pihole_ftl_prestart_t)
miscfiles_read_localization(pihole_ftl_prestart_t)

selinux_getattr_fs(pihole_ftl_prestart_t)

seutil_read_config(pihole_ftl_prestart_t)
seutil_read_file_contexts(pihole_ftl_prestart_t)
# TODO: avoid context translation in coreutils, e.g. install(1)
seutil_search_setrans_runtime(pihole_ftl_prestart_t)


########################################
#
# Policy for pihole
#

# TODO: maybe private labels for /opt/pihole/ scripts?

# chown		: /etc/pihole/gravity.db
# dac_override	: /etc/pihole/gravity.db
# fowner	: /etc/pihole/gravity.db
# fsetid	: /etc/pihole/gravity.db
# kill		: killall -s SIGHUP pihole-FTL
allow pihole_t self:capability { chown dac_override dac_read_search fowner fsetid kill setgid };
# TODO:
#type=PROCTITLE msg=audit(13/06/22 00:00:14.321:1353) : proctitle=curl -s -L --compressed -z /etc/pihole/list.1.raw.githubusercontent.com.domains -w %{http_code} -A Mozilla/5.0 (Windows NT 10.0;
#type=SYSCALL msg=audit(13/06/22 00:00:14.321:1353) : arch=aarch64 syscall=setsockopt success=no exit=ENOENT(No such file or directory) a0=0x5 a1=tcp a2=TCP_ULP a3=0xf1b493c47070 items=0 ppid=33454 pid=33455 auid=unset uid=root gid=root euid=root suid=root fsuid=root egid=root sgid=root fsgid=root tty=(none) ses=unset comm=curl exe=/usr/bin/curl subj=system_u:system_r:pihole_t:s0 key=(null)
#type=AVC msg=audit(13/06/22 00:00:14.321:1353) : avc:  denied  { net_admin } for  pid=33455 comm=curl capability=net_admin  scontext=system_u:system_r:pihole_t:s0 tcontext=system_u:system_r:pihole_t:s0 tclass=capability permissive=0
dontaudit pihole_t self:capability net_admin;
allow pihole_t self:process { getsched setfscreate setpgid setrlimit setsched signal };

# gravity.sh: tee /dev/fd/63 - openat
#dontaudit pihole_t self:dir write;
allow pihole_t self:dir write;

allow pihole_t self:fifo_file { getattr ioctl open read write };
# ss --ipv4 --listening --numeric --tcp --udp src  53
dontaudit pihole_t self:netlink_tcpdiag_socket create;
allow pihole_t self:unix_dgram_socket { create };
allow pihole_t self:unix_stream_socket { connect create getattr getopt read setopt write };
xperm_pattern(pihole_t, self, unix_dgram_socket, ioctl_siocgifindex)


allow pihole_t pihole_conf_t:dir manage_dir_perms;
allow pihole_t pihole_conf_t:file manage_file_perms;

allow pihole_t pihole_exec_t:file execute_no_trans;

allow pihole_t pihole_ftl_t:process signal;
allow pihole_t pihole_ftl_t:dir list_dir_perms;
allow pihole_t pihole_ftl_t:lnk_file read_lnk_file_perms;
allow pihole_t pihole_ftl_t:file read_file_perms;
allow pihole_t pihole_ftl_t:fifo_file getattr;
allow pihole_t pihole_ftl_t:netlink_route_socket getattr;
allow pihole_t pihole_ftl_t:tcp_socket getattr;
allow pihole_t pihole_ftl_t:udp_socket getattr;
allow pihole_t pihole_ftl_t:unix_stream_socket getattr;
# version
can_exec(pihole_t, pihole_ftl_exec_t)
gen_require(` #selint-disable:S-001
	class service { start status };
')
allow pihole_t pihole_unit_t:service { start status };

can_exec(pihole_t, pihole_helper_exec_t)

allow pihole_t pihole_log_t:file { read_file_perms watch };

allow pihole_t pihole_repo_t:dir rw_dir_perms;
allow pihole_t pihole_repo_t:file manage_file_perms;

# update-gravity: lsof -Pni:53
allow pihole_t pihole_runtime_t:dir search_dir_perms;
allow pihole_t pihole_runtime_t:file read_file_perms;
allow pihole_t pihole_runtime_t:sock_file getattr;

allow pihole_t pihole_state_t:file getattr;

allow pihole_t pihole_tmp_t:file manage_file_perms;
files_tmp_filetrans(pihole_t, pihole_tmp_t, file)


kernel_read_system_state(pihole_t)
kernel_read_network_state(pihole_t)
kernel_read_vm_overcommit_sysctl(pihole_t)
kernel_read_public_crypto_sysctls(pihole_t)
# pidof
kernel_list_proc(pihole_t)
kernel_dontaudit_read_state(pihole_t)
# lsof -Pni:53 -> /proc/locks
kernel_dontaudit_read_proc_files(pihole_t)
# systemctl --quiet is-active multi-user.target
kernel_read_public_kernel_sysctls(pihole_t)


corecmd_exec_bin(pihole_t)
corecmd_exec_shell(pihole_t)
# pidof
corecmd_dontaudit_getattr_all_executables(pihole_t)

corenet_send_generic_node(pihole_t)
corenet_send_stdextern_if(pihole_t)

# check if FTL is running
corenet_tcp_connect_http_port(pihole_t)
corenet_sendrecv_http_client_packets(pihole_t)
# nc 127.0.0.1 4711
corenet_sendrecv_pihole_lo_client_packets(pihole_t)
corenet_tcp_connect_pihole_port(pihole_t)

dev_read_urand(pihole_t)

domain_use_interactive_fds(pihole_t)
# pidof
domain_dontaudit_read_all_domains_state(pihole_t)

# /opt/pihole/
files_read_usr_files(pihole_t)
# update-gravity: lsof -Pni:53
files_dontaudit_getattr_all_runtime_content(pihole_t)
files_dontaudit_search_spool(pihole_t)

fs_getattr_xattr_fs(pihole_t)
fs_search_tmpfs(pihole_t)

logging_send_syslog_msg(pihole_t)
logging_search_logs(pihole_t)

miscfiles_read_config(pihole_t)
miscfiles_read_generic_certs(pihole_t)
miscfiles_read_localization(pihole_t)
miscfiles_read_terminfo(pihole_t)

selinux_getattr_fs(pihole_t)

seutil_search_config(pihole_t)

# systemctl
sysfs_search_firmware(pihole_t)

sysnet_dns_name_resolve(pihole_t)

systemd_use_fds(pihole_t)
systemd_rw_inherited_stream_sockets(pihole_t)
systemd_stream_connect(pihole_t)
systemd_append_stream_sockets(pihole_t)
systemd_state_system_targets(pihole_t)
systemd_search_inaccessible_dir(pihole_t)

userdom_use_inherited_user_terminals(pihole_t)
userdom_dontaudit_search_user_home_dirs(pihole_t)
userdom_dontaudit_search_user_home_content(pihole_t)


optional_policy(`
	dnsmasq_manage_config(pihole_t)
')

optional_policy(`
	lighttp_mmap_read_content(pihole_t)
	lighttp_feedback_client(pihole_t)
')

optional_policy(`
	# update gravity
	netutils_run_dig(pihole_t, system_r)
	netutils_nnp_domtrans_dig(pihole_t)
')

optional_policy(`
	tmux_dontaudit_search_session_dirs(pihole_t)
')
