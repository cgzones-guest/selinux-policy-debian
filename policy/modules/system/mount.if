## <summary>Policy for mount.</summary>
## <required val="true" />

########################################
## <summary>
##	Execute mount in the mount domain.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed to transition.
##	</summary>
## </param>
#
interface(`mount_domtrans',`
	gen_require(`
		type mount_t, mount_exec_t;
	')

	corecmd_search_bin($1)
	domtrans_pattern($1, mount_exec_t, mount_t)
')

########################################
## <summary>
##	Execute mount in the mount domain, and
##	allow the specified role the mount domain,
##	and use the caller's terminal.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed to transition.
##	</summary>
## </param>
## <param name="role">
##	<summary>
##	Role allowed access.
##	</summary>
## </param>
#
interface(`mount_run',`
	gen_require(`
		attribute_role mount_roles;
	')

	mount_domtrans($1)
	roleattribute $2 mount_roles;
')

########################################
## <summary>
##	Execute mount in the caller domain.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`mount_exec',`
	gen_require(`
		type mount_exec_t;
	')

	corecmd_search_bin($1)
	can_exec($1, mount_exec_t)
')

########################################
## <summary>
##	Read the mount binary.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`mount_read_exec',`
	gen_require(`
		type mount_exec_t;
	')

	corecmd_search_bin($1)
	allow $1 mount_exec_t:file read_file_perms;
')

########################################
## <summary>
##	Send a generic signal to mount.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`mount_signal',`
	gen_require(`
		type mount_t;
	')

	allow $1 mount_t:process signal;
')

########################################
## <summary>
##	Use file descriptors for mount.
## </summary>
## <param name="domain">
##	<summary>
##	The type of the process performing this action.
##	</summary>
## </param>
#
interface(`mount_use_fds',`
	gen_require(`
		type mount_t;
	')

	allow $1 mount_t:fd use;
')

########################################
## <summary>
##	Read loopback filesystem image files.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`mount_read_loopback_files',`
	gen_require(`
		type mount_loopback_t;
	')

	allow $1 mount_loopback_t:file read_file_perms;
')

########################################
## <summary>
##	Read and write loopback filesystem image files.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`mount_rw_loopback_files',`
	gen_require(`
		type mount_loopback_t;
	')

	allow $1 mount_loopback_t:file rw_file_perms;
')

########################################
## <summary>
##	Get attributes of mount runtime files.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`mount_getattr_runtime_files',`
	gen_require(`
		type mount_runtime_t;
	')

	files_search_runtime($1)
	allow $1 mount_runtime_t:dir search_dir_perms;
	allow $1 mount_runtime_t:file getattr;
')

########################################
## <summary>
##	Search the mount runtime directories.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`mount_search_runtime',`
	gen_require(`
		type mount_runtime_t;
	')

	files_search_runtime($1)
	allow $1 mount_runtime_t:dir search_dir_perms;
')

########################################
## <summary>
##	List the mount runtime directories.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`mount_list_runtime',`
	gen_require(`
		type mount_runtime_t;
	')

	files_search_runtime($1)
	allow $1 mount_runtime_t:dir list_dir_perms;
')

########################################
## <summary>
##	Watch mount runtime directories.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`mount_watch_runtime_dirs',`
	gen_require(`
		type mount_runtime_t;
	')

	allow $1 mount_runtime_t:dir watch_dir_perms;
')

########################################
## <summary>
##	Check write access on mount runtime directories (DAC wise).
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`mount_check_write_runtime_dirs',`
	gen_require(`
		type mount_runtime_t;
	')

	can_check_write_dir($1, mount_runtime_t)
')

########################################
## <summary>
##	Read mount runtime files.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`mount_read_runtime_files',`
	gen_require(`
		type mount_runtime_t;
	')

	files_search_runtime($1)
	allow $1 mount_runtime_t:dir list_dir_perms;
	allow $1 mount_runtime_t:file read_file_perms;
')

########################################
## <summary>
##	Watch mount runtime files, including read-like events.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`mount_watch_runtime_files',`
	gen_require(`
		type mount_runtime_t;
	')

	allow $1 mount_runtime_t:file { watch watch_reads };
')

########################################
## <summary>
##	Write and read mount runtime files.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`mount_rw_inherited_runtime_files',`
	gen_require(`
		type mount_runtime_t;
	')

	allow $1 mount_runtime_t:file rw_inherited_file_perms;
')

########################################
## <summary>
##	Create, read, write and delete mount runtime files.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`mount_manage_runtime_files',`
	gen_require(`
		type mount_runtime_t;
	')

	files_search_runtime($1)
	allow $1 mount_runtime_t:dir rw_dir_perms;
	allow $1 mount_runtime_t:file manage_file_perms;
')

########################################
## <summary>
##	All of the rules required to
##	administrate an mount environment.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
## <param name="role">
##	<summary>
##	Role allowed access.
##	</summary>
## </param>
#
interface(`mount__admin',`
	mount_run($1, $2)
')
