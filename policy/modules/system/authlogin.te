policy_module(authlogin)

########################################
#
# Declarations
#

attribute auth_pgm_domain;
attribute runuser_domain;

attribute can_read_shadow_passwords;
attribute can_write_shadow_passwords;
attribute can_relabelto_shadow_passwords;


type auth_lock_t;
fs_associate_xattr_only(auth_lock_t)
files_auth_file(auth_lock_t)

type chkpwd_t, can_read_shadow_passwords;
type chkpwd_exec_t;
userdom_user_application_domain(chkpwd_t, chkpwd_exec_t)
role system_r types chkpwd_t;

type faillog_t;
logging_writable_log_file(faillog_t)

type lastlog_t;
logging_writable_log_file(lastlog_t)

type pam_conf_t;
files_config_file(pam_conf_t)

type pam_script_t;
type pam_script_exec_t;
application_system_domain(pam_script_t, pam_script_exec_t)
corecmd_shell_entry_type(pam_script_t)

type runuser_exec_t;
application_executable_file(runuser_exec_t)

type shadow_t;
fs_associate_xattr_only(shadow_t)
files_auth_file(shadow_t)
neverallow ~can_read_shadow_passwords shadow_t:file read;
neverallow ~can_write_shadow_passwords shadow_t:file { append create write };
neverallow ~can_relabelto_shadow_passwords shadow_t:file relabelto;

type sulogin_t, can_read_shadow_passwords;
type sulogin_exec_t;
auth_login_pgm_domain(sulogin_t)
domain_entry_file(sulogin_t, sulogin_exec_t)
domain_interactive_fd_object(sulogin_t)
# signal sysadm_r
rbac_process_subject_full_exemption(sulogin_t)

type update_passwd_t, can_read_shadow_passwords;
type update_passwd_exec_t;
domain_type(update_passwd_t)
domain_entry_file(update_passwd_t, update_passwd_exec_t)
# setup /etc/passwd as system_u
ubac_change_object_to_sys_exemption(update_passwd_t)

type updpwd_t;
type updpwd_exec_t;
userdom_user_application_domain(updpwd_t, updpwd_exec_t)
role system_r types updpwd_t;
# setup /etc/shadow as system_u
ubac_change_object_to_sys_exemption(updpwd_t)

type utempter_t;
type utempter_exec_t;
userdom_user_application_domain(utempter_t, utempter_exec_t)

#
# nologin_runtime_t is the type for /run/nologin, see man:nologin(8)
#
type nologin_runtime_t;
files_runtime_file(nologin_runtime_t)

#
# utmp_t is the type for /run/utmp
#
type utmp_t;
files_runtime_file(utmp_t)

type wtmp_t;
logging_writable_log_file(wtmp_t)


########################################
#
# Check password local policy
#

allow chkpwd_t self:capability { setgid setuid };
allow chkpwd_t self:process { getattr getcap signal };
allow chkpwd_t self:unix_dgram_socket { connect create write };
allow chkpwd_t self:unix_stream_socket { connect create };


allow chkpwd_t shadow_t:file read_file_perms;


kernel_getattr_proc(chkpwd_t)
kernel_read_public_crypto_sysctls(chkpwd_t)
kernel_read_system_state(chkpwd_t)
kernel_read_public_kernel_sysctls(chkpwd_t)

dev_read_rand(chkpwd_t)
dev_read_urand(chkpwd_t)

logging_send_audit_msgs(chkpwd_t)
logging_send_syslog_msg(chkpwd_t)

miscfiles_read_config(chkpwd_t)
miscfiles_read_localization(chkpwd_t)

selinux_getattr_fs(chkpwd_t)
selinux_get_enforce_mode(chkpwd_t)

seutil_read_config(chkpwd_t)

term_use_inherited_unallocated_ttys(chkpwd_t)

userdom_use_inherited_user_terminals(chkpwd_t)


########################################
#
# Policy for update-passwd
#

allow update_passwd_t self:capability chown;
allow update_passwd_t self:process setfscreate;

auth_manage_shadow(update_passwd_t)
auth_acquire_lock(update_passwd_t)


kernel_read_system_state(update_passwd_t)


domain_use_interactive_fds(update_passwd_t)

files_rw_etc_dirs(update_passwd_t)
files_read_usr_files(update_passwd_t)

# /etc/passwd
miscfiles_manage_config_files(update_passwd_t)

selinux_getattr_fs(update_passwd_t)

seutil_read_config(update_passwd_t)
seutil_read_file_contexts(update_passwd_t)

userdom_use_inherited_user_terminals(update_passwd_t)


optional_policy(`
	apt_use_ptys(update_passwd_t)
')

optional_policy(`
	dpkg_write_inherited_script_files(update_passwd_t)
')


########################################
#
# updpwd local policy
#

allow updpwd_t self:capability { chown fsetid };
allow updpwd_t self:process setfscreate;
allow updpwd_t self:fifo_file rw_fifo_file_perms;
allow updpwd_t self:unix_stream_socket create_stream_socket_perms;
allow updpwd_t self:unix_dgram_socket create_socket_perms;


auth_manage_shadow(updpwd_t)
auth_acquire_lock(updpwd_t)
auth_etc_filetrans_shadow(updpwd_t)


kernel_read_system_state(updpwd_t)


dev_read_urand(updpwd_t)

logging_send_syslog_msg(updpwd_t)

miscfiles_read_config(updpwd_t)
miscfiles_read_localization(updpwd_t)

selinux_getattr_fs(updpwd_t)

seutil_search_config(updpwd_t)

userdom_use_inherited_user_terminals(updpwd_t)


########################################
#
# Utempter local policy
#

allow utempter_t self:capability setgid;
allow utempter_t self:unix_dgram_socket { connect create write };
allow utempter_t self:unix_stream_socket create_stream_socket_perms;


allow utempter_t utmp_t:file rw_file_perms;
allow utempter_t wtmp_t:file rw_file_perms;


dev_read_urand(utempter_t)

domain_use_interactive_fds(utempter_t)

# /usr/share/sounds/Oxygen-Sys-App-Message.ogg
files_read_usr_files(utempter_t)
files_search_runtime(utempter_t)

logging_send_syslog_msg(utempter_t)
logging_search_logs(utempter_t)

miscfiles_read_config(utempter_t)
miscfiles_read_localization(utempter_t)

systemd_user_instance_use_fds(utempter_t)
systemd_user_instance_rw_inherited_stream_sockets(utempter_t)

term_getattr_all_ptys(utempter_t)
term_getattr_all_ttys(utempter_t)
term_use_inherited_ptmx(utempter_t)

userdom_use_inherited_user_terminals(utempter_t)


########################################
#
# runuser policy
#

allow runuser_domain self:capability { setgid setuid sys_resource };
allow runuser_domain self:process { setrlimit setsched };
allow runuser_domain self:key search;
allow runuser_domain self:unix_dgram_socket { connect create write };
allow runuser_domain self:unix_stream_socket { connect create read setopt write };

allow runuser_domain pam_conf_t:dir list_dir_perms;
allow runuser_domain pam_conf_t:file read_file_perms;

allow runuser_domain faillog_t:file write_file_perms;

allow runuser_domain utmp_t:file read_file_perms;


kernel_getattr_proc(runuser_domain)
kernel_read_public_kernel_sysctls(runuser_domain)


domain_use_interactive_fds(runuser_domain)

locallogin_read_config(runuser_domain)

logging_send_syslog_msg(runuser_domain)
logging_search_logs(runuser_domain)

miscfiles_read_config(runuser_domain)
miscfiles_read_localization(runuser_domain)

selinux_getattr_fs(runuser_domain)

seutil_search_config(runuser_domain)

systemd_use_fds(runuser_domain)
systemd_rw_inherited_stream_sockets(runuser_domain)
# /proc/1/limits
systemd_read_state(runuser_domain)
systemd_use_userdb(runuser_domain)
systemd_search_key(runuser_domain)

systemd_user_instance_search_key(runuser_domain)

term_search_ptys(runuser_domain)

userdom_search_all_users_keys(runuser_domain)
userdom_use_inherited_user_terminals(runuser_domain)


########################################
#
# Policy for PAM scripts
#

allow pam_script_t self:fifo_file { getattr ioctl read write };
allow pam_script_t self:unix_stream_socket { connect create };

allow pam_script_t pam_script_exec_t:dir search_dir_perms;


corecmd_exec_bin(pam_script_t)

files_search_runtime(pam_script_t)

miscfiles_read_config(pam_script_t)
miscfiles_read_localization(pam_script_t)

selinux_get_enforce_mode(pam_script_t)
selinux_getattr_fs(pam_script_t)

seutil_search_config(pam_script_t)


optional_policy(`
	mta_mail_client(pam_script_t, system_r)
')


########################################
#
# Policy for sulogin
#

allow sulogin_t self:capability { sys_admin sys_tty_config };
allow sulogin_t self:process { getpgid setexec signal };
allow sulogin_t self:unix_stream_socket { connect create setopt };

auth_read_shadow(sulogin_t)


# /proc/consoles
kernel_read_all_system_state(sulogin_t)


miscfiles_read_config(sulogin_t)
miscfiles_read_localization(sulogin_t)

systemd_use_fds(sulogin_t)

term_use_unallocated_ttys(sulogin_t)
term_relabel_unallocated_ttys(sulogin_t)
term_control_unallocated_ttys(sulogin_t, {
	ioctl_kdgkbmode
	ioctl_tcsets
	ioctl_tcsetsw
	ioctl_tcsetsf
	ioctl_tcflsh
	ioctl_tiocsctty
	ioctl_tiocgpgrp
	ioctl_tiocspgrp
	ioctl_tiocgwinsz
	ioctl_tiocswinsz
	ioctl_tiocgserial
	ioctl_tiocslcktrmios
})
term_read_console(sulogin_t)

userdom_relabel_user_ttys(sulogin_t)
userdom_spec_domtrans_priv_users(sulogin_t)
userdom_signal_priv_users(sulogin_t)
userdom_search_user_home_dirs(sulogin_t)
