## <summary>Policy for dmsetup -- low level logical volume management.</summary>
## <required val="true" />

########################################
## <summary>
##	Execute dmsetup program in the dmsetup domain.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed to transition.
##	</summary>
## </param>
#
interface(`dmsetup_domtrans',`
	gen_require(`
		type dmsetup_t, dmsetup_exec_t;
	')

	corecmd_search_bin($1)
	domtrans_pattern($1, dmsetup_exec_t, dmsetup_t)
')

########################################
## <summary>
##	Execute dmsetup program in the dmsetup domain and
##	allow the specified role the dmsetup domain.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed to transition.
##	</summary>
## </param>
## <param name="role">
##	<summary>
##	The role to allow the LVM domain.
##	</summary>
## </param>
#
interface(`dmsetup_run',`
	gen_require(`
		type dmsetup_t;
	')

	dmsetup_domtrans($1)
	role $2 types dmsetup_t;
')

########################################
## <summary>
##	Execute dmsetup program in the caller domain.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`dmsetup_exec',`
	gen_require(`
		type dmsetup_exec_t;
	')

	corecmd_search_bin($1)
	can_exec($1, dmsetup_exec_t)
')

########################################
## <summary>
##	Read the dmsetup binary.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`dmsetup_read_exec',`
	gen_require(`
		type dmsetup_exec_t;
	')

	corecmd_search_bin($1)
	allow $1 dmsetup_exec_t:file read_file_perms;
')

########################################
## <summary>
##	Read the state of the blkdeactivate process.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`dmsetup_state_blkdeactivate',`
	gen_require(`
		type blkdeactivate_t;
	')

	read_process_pattern($1, blkdeactivate_t)
')

########################################
## <summary>
##	Read inherited temporary files from blkdeactivate.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`dmsetup_read_inherited_blkdeactivate_tmp_files',`
	gen_require(`
		type blkdeactivate_tmp_t;
	')

	allow $1 blkdeactivate_tmp_t:file read;
	neverallow $1 blkdeactivate_tmp_t:file open;
')
