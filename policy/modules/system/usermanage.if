## <summary>Policy for managing user accounts.</summary>
## <required val="true" />

########################################
## <summary>
##	Execute chage in the chage domain.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed to transition.
##	</summary>
## </param>
#
interface(`usermanage_domtrans_chage',`
	gen_require(`
		type chage_t, chage_exec_t;
	')

	corecmd_search_bin($1)
	domtrans_pattern($1, chage_exec_t, chage_t)
')

########################################
## <summary>
##	Execute chage in the chage domain, and
##	allow the specified role the chage domain.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed to transition.
##	</summary>
## </param>
## <param name="role">
##	<summary>
##	Role allowed access.
##	</summary>
## </param>
## <param name="conditional" optional="true">
##	<summary>
##	Conditional guarding access.
##	</summary>
## </param>
#
interface(`usermanage_run_chage',`
	gen_require(`
		attribute_role chage_roles;
	')

	# TODO: permit roleattribute in conditional policies
	ifelse(`$3',`',`
		usermanage_domtrans_chage($1)
	',`
		tunable_policy(`$3',`
			usermanage_domtrans_chage($1)
		')
	')
	roleattribute $2 chage_roles;
')

########################################
## <summary>
##	Execute chfn in the chfn domain.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed to transition.
##	</summary>
## </param>
#
interface(`usermanage_domtrans_chfn',`
	gen_require(`
		type chfn_t, chfn_exec_t;
	')

	corecmd_search_bin($1)
	domtrans_pattern($1, chfn_exec_t, chfn_t)
')

########################################
## <summary>
##	Execute chfn in the chfn domain, and
##	allow the specified role the chfn domain.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed to transition.
##	</summary>
## </param>
## <param name="role">
##	<summary>
##	Role allowed access.
##	</summary>
## </param>
## <param name="conditional" optional="true">
##	<summary>
##	Conditional guarding access.
##	</summary>
## </param>
#
interface(`usermanage_run_chfn',`
	gen_require(`
		attribute_role chfn_roles;
	')

	# TODO: permit roleattribute in conditional policies
	ifelse(`$3',`',`
		usermanage_domtrans_chfn($1)
	',`
		tunable_policy(`$3',`
			usermanage_domtrans_chfn($1)
		')
	')
	roleattribute $2 chfn_roles;
')

########################################
## <summary>
##	Run expiry to check password expiration.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed to transition.
##	</summary>
## </param>
#
interface(`usermanage_domtrans_expiry',`
	gen_require(`
		type expiry_t, expiry_exec_t;
	')

	corecmd_search_bin($1)
	domtrans_pattern($1, expiry_exec_t, expiry_t)
')

########################################
## <summary>
##	Execute expiry programs in the expiry domain.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed to transition.
##	</summary>
## </param>
## <param name="role">
##	<summary>
##	The role to allow the expiry domain.
##	</summary>
## </param>
#
interface(`usermanage_run_expiry',`
	gen_require(`
		type expiry_t;
	')

	usermanage_domtrans_expiry($1)
	role $2 types expiry_t;
')

########################################
## <summary>
##	Execute groupadd in the groupadd domain.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed to transition.
##	</summary>
## </param>
#
interface(`usermanage_domtrans_groupadd',`
	gen_require(`
		type groupadd_t, groupadd_exec_t;
	')

	corecmd_search_bin($1)
	domtrans_pattern($1, groupadd_exec_t, groupadd_t)
')

########################################
## <summary>
##	Execute groupadd in the groupadd domain, and
##	allow the specified role the groupadd domain.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed to transition.
##	</summary>
## </param>
## <param name="role">
##	<summary>
##	Role allowed access.
##	</summary>
## </param>
#
interface(`usermanage_run_groupadd',`
	gen_require(`
		attribute_role groupadd_roles;
	')

	usermanage_domtrans_groupadd($1)
	roleattribute $2 groupadd_roles;
')

########################################
## <summary>
##	The role template for newgrp execution.
## </summary>
## <param name="role_prefix">
##	<summary>
##	The prefix of the user role (e.g., user
##	is the prefix for user_r).
##	</summary>
## </param>
## <param name="user_role">
##	<summary>
##	The role associated with the user domain.
##	</summary>
## </param>
## <param name="user_domain">
##	<summary>
##	The type of the user domain.
##	</summary>
## </param>
#
template(`usermanage_newgrp_role_template',`
	gen_require(`
		attribute newgrp_domain;
		type newgrp_exec_t;
	')

	########################################
	#
	# Declarations
	#

	type $1_newgrp_t, newgrp_domain;
	userdom_user_application_domain($1_newgrp_t, newgrp_exec_t)
	role $2 types $1_newgrp_t;

	########################################
	#
	# Role dependent policy
	#

	domtrans_pattern($3, newgrp_exec_t, $1_newgrp_t)

	allow $3 $1_newgrp_t:fd use;

	# /etc/gshadow
	auth_read_shadow($1_newgrp_t)

	corecmd_shell_domtrans($1_newgrp_t, $3)

	logging_send_audit_msgs($1_newgrp_t)
')

########################################
## <summary>
##	Run newgidmap and newuidmap.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed to transition.
##	</summary>
## </param>
#
interface(`usermanage_domtrans_newxidmap',`
	gen_require(`
		type newxidmap_t, newxidmap_exec_t;
	')

	corecmd_search_bin($1)
	domtrans_pattern($1, newxidmap_exec_t, newxidmap_t)
')

########################################
## <summary>
##	Run newgidmap and newuidmap.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed to transition.
##	</summary>
## </param>
## <param name="role">
##	<summary>
##	The role to allow the expiry domain.
##	</summary>
## </param>
#
interface(`usermanage_run_newxidmap',`
	gen_require(`
		attribute_role newxidmap_roles;
		type newxidmap_t;
	')

	usermanage_domtrans_newxidmap($1)
	roleattribute $2 newxidmap_roles;

	# write to uid_map
	allow newxidmap_t $1:dir { getattr open read search };
	allow newxidmap_t $1:file { open write };
')

########################################
## <summary>
##	Execute passwd in the passwd domain.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed to transition.
##	</summary>
## </param>
#
interface(`usermanage_domtrans_passwd',`
	gen_require(`
		type passwd_t, passwd_exec_t;
	')

	corecmd_search_bin($1)
	domtrans_pattern($1, passwd_exec_t, passwd_t)
')

########################################
## <summary>
##	Check if the passwd binary is executable.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`usermanage_check_exec_passwd',`
	gen_require(`
		type passwd_exec_t;
	')

	corecmd_search_bin($1)
	can_check_exec($1, passwd_exec_t)
')

########################################
## <summary>
##	Execute passwd in the passwd domain, and
##	allow the specified role the passwd domain.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed to transition.
##	</summary>
## </param>
## <param name="role">
##	<summary>
##	Role allowed access.
##	</summary>
## </param>
## <param name="conditional" optional="true">
##	<summary>
##	Conditional guarding access.
##	</summary>
## </param>
#
interface(`usermanage_run_passwd',`
	gen_require(`
		attribute_role passwd_roles;
	')

	# TODO: permit roleattribute in conditional policies
	ifelse(`$3',`',`
		usermanage_domtrans_passwd($1)
	',`
		tunable_policy(`$3',`
			usermanage_domtrans_passwd($1)
		')
	')

	roleattribute $2 passwd_roles;
')

########################################
## <summary>
##	Execute password admin functions in
##	the admin passwd domain.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed to transition.
##	</summary>
## </param>
#
interface(`usermanage_domtrans_admin_passwd',`
	gen_require(`
		type admin_passwd_exec_t, sysadm_passwd_t;
	')

	corecmd_search_bin($1)
	domtrans_pattern($1, admin_passwd_exec_t, sysadm_passwd_t)
')

########################################
## <summary>
##	Execute passwd admin functions in the admin
##	passwd domain, and allow the specified role
##	the admin passwd domain.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed to transition.
##	</summary>
## </param>
## <param name="role">
##	<summary>
##	Role allowed access.
##	</summary>
## </param>
#
interface(`usermanage_run_admin_passwd',`
	gen_require(`
		attribute_role sysadm_passwd_roles;
	')

	usermanage_domtrans_admin_passwd($1)
	roleattribute $2 sysadm_passwd_roles;
')

########################################
## <summary>
##	Do not audit attempts to use useradd fds.
## </summary>
## <param name="domain">
##	<summary>
##	Domain to not audit.
##	</summary>
## </param>
#
interface(`usermanage_dontaudit_use_useradd_fds',`
	gen_require(`
		type useradd_t;
	')

	dontaudit $1 useradd_t:fd use;
')

########################################
## <summary>
##	Execute useradd in the useradd domain.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed to transition.
##	</summary>
## </param>
#
interface(`usermanage_domtrans_useradd',`
	gen_require(`
		type useradd_t, useradd_exec_t;
	')

	corecmd_search_bin($1)
	domtrans_pattern($1, useradd_exec_t, useradd_t)
')

########################################
## <summary>
##	Check if the useradd binaries are executable.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`usermanage_check_exec_useradd',`
	gen_require(`
		type useradd_exec_t;
	')

	corecmd_search_bin($1)
	can_check_exec($1, useradd_exec_t)
')

########################################
## <summary>
##	Execute useradd in the useradd domain, and
##	allow the specified role the useradd domain.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed to transition.
##	</summary>
## </param>
## <param name="role">
##	<summary>
##	Role allowed access.
##	</summary>
## </param>
#
interface(`usermanage_run_useradd',`
	gen_require(`
		attribute_role useradd_roles;
	')

	usermanage_domtrans_useradd($1)
	roleattribute $2 useradd_roles;
')

########################################
## <summary>
##	Execute wall in the caller domain.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`usermanage_exec_wall',`
	gen_require(`
		type wall_exec_t;
	')

	corecmd_search_bin($1)
	can_exec($1, wall_exec_t)
')

########################################
## <summary>
##	Execute a domain transition to run wall.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed to transition.
##	</summary>
## </param>
#
interface(`usermanage_domtrans_wall',`
	gen_require(`
		type wall_t, wall_exec_t;
	')

	corecmd_search_bin($1)
	domtrans_pattern($1, wall_exec_t, wall_t)
')

########################################
## <summary>
##	Execute wall in the wall domain,
##	and allow the specified role
##	the wall domain.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed to transition.
##	</summary>
## </param>
## <param name="role">
##	<summary>
##	Role allowed access.
##	</summary>
## </param>
#
interface(`usermanage_run_wall',`
	gen_require(`
		type wall_t;
	')

	usermanage_domtrans_wall($1)
	role $2 types wall_t;
')

########################################
## <summary>
##	Execute write in the caller domain.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`usermanage_exec_write',`
	gen_require(`
		type write_exec_t;
	')

	corecmd_search_bin($1)
	can_exec($1, write_exec_t)
')

########################################
## <summary>
##	Execute a domain transition to run write.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed to transition.
##	</summary>
## </param>
#
interface(`usermanage_domtrans_write',`
	gen_require(`
		type write_t, write_exec_t;
	')

	corecmd_search_bin($1)
	domtrans_pattern($1, write_exec_t, write_t)
')

########################################
## <summary>
##	Execute write in the write domain,
##	and allow the specified role
##	the write domain.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed to transition.
##	</summary>
## </param>
## <param name="role">
##	<summary>
##	Role allowed access.
##	</summary>
## </param>
#
interface(`usermanage_run_write',`
	gen_require(`
		type write_t;
	')

	usermanage_domtrans_write($1)
	role $2 types write_t;
')

########################################
## <summary>
##	Read the usermanage config files.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`usermanage_read_config',`
	gen_require(`
		type usermanage_conf_t;
	')

	files_search_etc($1)
	allow $1 usermanage_conf_t:file read_file_perms;
')

########################################
## <summary>
##	All of the rules required to
##	administrate an usermanage environment.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
## <param name="role">
##	<summary>
##	Role allowed access.
##	</summary>
## </param>
#
template(`usermanage__admin',`
	usermanage_run_admin_passwd($1, $2)
	usermanage_run_groupadd($1, $2)
	usermanage_run_useradd($1, $2)
	usermanage_run_chage($1, $2)
	usermanage_run_chfn($1, $2)
	usermanage_newgrp_role_template($3, $2, $1)
	usermanage_run_passwd($1, $2)
	usermanage_run_wall($1, $2)
	usermanage_run_write($1, $2)
')
