#!/bin/sh

set -u

echo "++ missing default labels:"
for path in /run /tmp /usr/tmp /var/backups /var/lock /var/log /var/tmp /run/user/*/
do
   [ -e "$path" ] && find "$path" -execdir restorecon -v -n {} \;
done

echo "++ devices with generic label:"
find /dev/ ! -type d ! -type l -context '*:device_t*' -exec ls -ldZ {} \;

echo "++ unlabeled files:"
find / -context '*unlabeled_t*' -exec ls -ldZ {} \; 2> /dev/null

echo "++ setuid/setgid binaries:"
find / -perm /u=s,g=s ! -type d -exec ls -ldZ {} \; 2> /dev/null

echo "++ world-writable directories:"
find / -type d \( -perm -0002 -a ! -perm -1000 \) -exec ls -ldZ {} \; 2> /dev/null
