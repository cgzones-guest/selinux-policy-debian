########################################
#
# Rules and Targets for building modular policies
#

all_modules := $(base_mods) $(mod_mods) $(off_mods)
all_interfaces := $(all_modules:.te=.if)
enabled_mod_fc := $(addprefix $(tmpdir)/,$(notdir $(base_mods:.te=.mod.fc) $(mod_mods:.te=.mod.fc)))

base_pkg := $(builddir)base.pp
base_fc := $(builddir)base.fc
base_conf := $(builddir)base.conf
base_mod := $(tmpdir)/base.mod

users_extra := $(tmpdir)/users_extra

base_sections := $(tmpdir)/pre_te_files.conf $(tmpdir)/all_attrs_types.conf $(tmpdir)/global_bools.conf $(tmpdir)/only_te_rules.conf $(tmpdir)/all_post.conf

base_pre_te_files := $(secclass) $(isids) $(avs) $(ctx_defaults) $(poldir)/mls $(poldir)/mcs $(policycaps)
base_te_files := $(base_mods)
base_post_te_files := $(user_files) $(poldir)/ubac_constraints $(poldir)/rbac_constraints
base_fc_files := $(base_mods:.te=.fc)

mod_pkgs := $(addprefix $(builddir),$(notdir $(mod_mods:.te=.pp)))

# policy packages to install
instpkg := $(addprefix $(modpkgdir)/,$(notdir $(base_pkg)) $(mod_pkgs))

# search layer dirs for source files
vpath %.te $(all_layers)
vpath %.if $(all_layers)
vpath %.fc $(all_layers)

.SECONDARY: $(enabled_mod_fc:.mod.fc=.mod) $(enabled_mod_fc)

########################################
#
# default action: create all module packages
#
default: all

all: policy $(builtappfiles)

policy: base modules

base: $(base_pkg)

modules: $(mod_pkgs)

install: $(instpkg) $(appfiles)

########################################
#
# Load all configured modules
#
load: $(instpkg) $(appfiles)
# make sure two directories exist since they are not
# created by semanage
	@echo "Loading configured modules."
	@$(INSTALL) -d -m 0755 $(policypath) $(dir $(fcpath))
	$(verbose) $(SEMODULE) -s $(NAME) -X 100 -i $(modpkgdir)/$(notdir $(base_pkg)) $(foreach mod,$(mod_pkgs),-i $(modpkgdir)/$(mod))

########################################
#
# Load all configured modules and remove foreign ones
#
pure-load: $(instpkg) $(appfiles)
# make sure two directories exist since they are not
# created by semanage
	@echo "Loading configured modules."
	@$(INSTALL) -d -m 0755 $(policypath) $(dir $(fcpath))
	$(verbose) $(SEMODULE) -s $(NAME) -X 100 -i $(modpkgdir)/$(notdir $(base_pkg)) $(foreach mod,$(mod_pkgs),-i $(modpkgdir)/$(mod)) $(foreach omod,$(filter-out base $(notdir $(mod_mods:.te=)),$(shell $(SEMODULE) -l)),-r $(omod))

########################################
#
# Install policy packages
#
$(modpkgdir)/%.pp: $(builddir)%.pp | $(modpkgdir)
	@echo "Installing $(NAME) $(@F) policy package."
	$(verbose) $(INSTALL) -m 0644 $^ $(modpkgdir)

########################################
#
# m4 frozen state
#
$(tmpdir)/m4support.m4f: | $(tmpdir)
$(tmpdir)/m4support.m4f: $(m4support)
	$(verbose) $(M4) -F $@ $^

$(tmpdir)/m4support_all.m4f: | $(tmpdir)
$(tmpdir)/m4support_all.m4f: $(m4support) $(tmpdir)/generated_definitions.conf $(tmpdir)/all_interfaces.conf
	$(verbose) $(M4) -F $@ $^

########################################
#
# Build module packages
#
$(tmpdir)/%.mod: | $(tmpdir)
$(tmpdir)/%.mod: $(tmpdir)/m4support_all.m4f %.te $(m4terminate)
	@echo "Compiling $(NAME) $(@F) module"
	$(verbose) $(M4) $(M4PARAM) -s -R $^ > $(@:.mod=.tmp)
	$(verbose) $(CHECKMODULE) -m $(@:.mod=.tmp) -o $@

$(tmpdir)/%.mod.fc: | $(tmpdir)
$(tmpdir)/%.mod.fc: $(tmpdir)/m4support.m4f %.fc
	$(verbose) $(M4) $(M4PARAM) -R $^ > $@

$(builddir)%.pp: | $(builddir)
$(builddir)%.pp: $(tmpdir)/%.mod $(tmpdir)/%.mod.fc
	@echo "Creating $(NAME) $(@F) policy package"
	$(verbose) $(SEMOD_PKG) -o $@ -m $< -f $<.fc

$(tmpdir)/all_mods.fc: $(enabled_mod_fc)
	$(verbose) cat $^ > $@

########################################
#
# Create a base module package
#
$(base_pkg): | $(builddir)
$(base_pkg): $(base_mod) $(base_fc) $(users_extra) $(tmpdir)/seusers
	@echo "Creating $(NAME) base module package"
	$(verbose) $(SEMOD_PKG) -o $@ -m $(base_mod) -f $(base_fc) -u $(users_extra) -s $(tmpdir)/seusers

$(base_mod): $(base_conf)
	@echo "Compiling $(NAME) base module"
	$(verbose) $(CHECKMODULE) -U $(UNK_PERMS) $^ -o $@

$(tmpdir)/seusers: | $(tmpdir)
$(tmpdir)/seusers: $(tmpdir)/m4support.m4f $(seusers)
	$(verbose) $(M4) $(M4PARAM) -R $^ | $(GREP) '^[a-z_]' > $@

$(users_extra): | $(tmpdir)
$(users_extra): $(tmpdir)/m4support.m4f $(user_files)
	$(verbose) $(M4) $(M4PARAM) -D users_extra -R $^ > $(tmpdir)/$(@F).tmp
	$(verbose) $(SED) -r -n -e 's/^[[:blank:]]*//g' -e '/^user/p' $(tmpdir)/$(@F).tmp > $@

########################################
#
# Construct a base.conf
#
$(base_conf): | $(builddir)
$(base_conf): $(base_sections)
	@echo "Creating $(NAME) base module $(@F)"
	$(verbose) cat $^ > $@

$(tmpdir)/pre_te_files.conf: | $(tmpdir)
$(tmpdir)/pre_te_files.conf: M4PARAM += -D self_contained_policy
$(tmpdir)/pre_te_files.conf: $(tmpdir)/m4support.m4f $(base_pre_te_files)
	$(verbose) $(M4) $(M4PARAM) -R $^ > $@

$(tmpdir)/generated_definitions.conf: | $(tmpdir)
$(tmpdir)/generated_definitions.conf:
# define all available object classes
	@cat $(m4divert) > $@
	$(verbose) $(genperm) $(avs) $(secclass) >> $@
	$(verbose) $(call create-base-per-role-tmpl,$(patsubst %.te,%,$(base_mods)),$@)
	$(verbose) test -f $(booleans) && $(setbools) $(booleans) >> $@ || true
	@cat $(m4undivert) >> $@

$(tmpdir)/global_bools.conf: M4PARAM += -D self_contained_policy
$(tmpdir)/global_bools.conf: $(tmpdir)/m4support.m4f $(tmpdir)/generated_definitions.conf $(globalbool) $(globaltun)
	$(verbose) $(M4) $(M4PARAM) -R $^ > $@

$(tmpdir)/all_interfaces.conf: | $(tmpdir)
$(tmpdir)/all_interfaces.conf: $(tmpdir)/m4support.m4f $(all_interfaces) $(m4iferror)
	@cat $(m4divert) > $@
	$(verbose) $(M4) -R $^ > $(tmpdir)/$(@F).tmp
	$(verbose) $(SED) -e s/dollarsstar/\$$\*/g $(tmpdir)/$(@F).tmp >> $@
	@cat $(m4undivert) >> $@

$(tmpdir)/all_te_files.conf: | $(tmpdir)
$(tmpdir)/all_te_files.conf: M4PARAM += -D self_contained_policy
$(tmpdir)/all_te_files.conf: $(tmpdir)/m4support_all.m4f $(base_te_files) $(m4terminate)
ifeq "$(strip $(base_te_files))" ""
	$(error No enabled modules! $(notdir $(mod_conf)) may need to be generated by using "make conf")
endif
	$(verbose) $(M4) $(M4PARAM) -s -R $^ > $@

$(tmpdir)/post_te_files.conf: | $(tmpdir)
$(tmpdir)/post_te_files.conf: M4PARAM += -D self_contained_policy
$(tmpdir)/post_te_files.conf: $(tmpdir)/m4support.m4f $(tmpdir)/generated_definitions.conf $(base_post_te_files) $(tmpdir)/all_te_files.conf
	$(verbose) $(GREP) '^[[:blank:]]*user ' $(tmpdir)/all_te_files.conf > $@ || true
	$(verbose) $(M4) $(M4PARAM) -R $(tmpdir)/m4support.m4f $(tmpdir)/generated_definitions.conf $(base_post_te_files) >> $@

# extract attributes and put them first. extract post te stuff
# like genfscon and put last.
$(tmpdir)/all_attrs_types.conf: $(tmpdir)/all_te_files.conf
	$(verbose) $(get_type_attr_decl) $^ | $(SORT) > $@

$(tmpdir)/all_post.conf: $(tmpdir)/all_te_files.conf $(tmpdir)/post_te_files.conf
	$(verbose) cat $(tmpdir)/post_te_files.conf > $@
# these have to run individually because order matters:
	$(verbose) $(GREP) '^[[:blank:]]*sid ' $(tmpdir)/all_te_files.conf >> $@ || true
	$(verbose) $(GREP) '^[[:blank:]]*fs_use_(xattr|task|trans)' $(tmpdir)/all_te_files.conf >> $@ || true
	$(verbose) $(GREP) '^[[:blank:]]*genfscon' $(tmpdir)/all_te_files.conf >> $@ || true
	$(verbose) $(GREP) '^[[:blank:]]*portcon' $(tmpdir)/all_te_files.conf >> $@ || true
	$(verbose) $(GREP) '^[[:blank:]]*netifcon' $(tmpdir)/all_te_files.conf >> $@ || true
	$(verbose) $(GREP) '^[[:blank:]]*nodecon' $(tmpdir)/all_te_files.conf >> $@ || true
	$(verbose) $(GREP) '^[[:blank:]]*ibpkeycon' $(tmpdir)/all_te_files.conf >> $@ || true
	$(verbose) $(GREP) '^[[:blank:]]*ibendportcon' $(tmpdir)/all_te_files.conf >> $@ || true

$(tmpdir)/only_te_rules.conf: $(tmpdir)/all_te_files.conf
	$(verbose) $(comment_move_decl) $^ > $@

########################################
#
# Construct a base.fc
#
$(base_fc): $(tmpdir)/$(notdir $(base_fc)).tmp
	$(verbose) $(fcsort) $< $@

$(tmpdir)/$(notdir $(base_fc)).tmp: | $(tmpdir)
$(tmpdir)/$(notdir $(base_fc)).tmp: $(tmpdir)/m4support.m4f $(tmpdir)/generated_definitions.conf $(base_fc_files)
ifeq ($(base_fc_files),)
	$(error No enabled modules! $(notdir $(mod_conf)) may need to be generated by using "make conf")
endif
	@echo "Creating $(NAME) base module file contexts."
	$(verbose) $(M4) $(M4PARAM) -R $^ > $@

########################################
#
# Appconfig files
#
$(builtappconf)/customizable_types: $(base_conf)
	$(verbose) $(GREP) '^[[:blank:]]*type .*customizable' $< | cut -d';' -f1 | cut -d',' -f1 | cut -d' ' -f2 | $(SORT) -u > $(tmpdir)/customizable_types
	@$(INSTALL) -d -m 0755 $(@D)
	$(verbose) $(INSTALL) -m 0644 $(tmpdir)/customizable_types $@

########################################
#
# Validate linking and expanding of modules, file_contexts, and appconfig
#
validate: $(base_pkg) $(mod_pkgs) $(tmpdir)/all_mods.fc $(builtappfiles)
	@echo "Validating $(NAME) linking."
	$(verbose) $(SEMOD_LNK) -o $(tmpdir)/test.lnk $(base_pkg) $(mod_pkgs)
	$(verbose) $(SEMOD_EXP) $(tmpdir)/test.lnk $(tmpdir)/policy.bin
	@echo "Validating $(NAME) file contexts."
	$(verbose) $(SETFILES) -q -c $(tmpdir)/policy.bin $(tmpdir)/all_mods.fc
	@echo "Validating $(NAME) appconfig."
	$(verbose) $(validateappconfig) --lxc $(builtappconf) $(tmpdir)/policy.bin
	@echo "Success."

########################################
#
# Create appropriate directories
#
$(modpkgdir):
	@echo "Installing directory $@"
	@$(INSTALL) -d -m 0755 $@

########################################
#
# Clean the sources
#
clean:
	$(verbose) rm -f $(base_conf)
	$(verbose) rm -f $(base_fc)
	$(verbose) rm -f $(builddir)*.pp
	$(verbose) rm -f $(net_contexts) $(net_contexts_nft)
	$(verbose) rm -fR $(tmpdir)

.PHONY: default all policy base modules install load pure-load clean validate
