" Vim syntax file
" Language:     TE (SELinux Type Enforcement Policy)
" Authors:      Thomas Bleher <ThomasBleher@gmx.de>, Lukas Zapletal <lzap+git@redhat.com>, Christian Göttsche <cgzones@googlemail.com>
"
" Uses m4.vim, some code taken from cpp.vim.
"
if exists('b:current_syntax')
    finish
endif

" First load the M4 syntax
runtime! syntax/m4.vim
unlet b:current_syntax

" Change m4Type to allow lowercase-macros
syn region      m4Function      matchgroup=m4Type      start="\<[_A-Za-z]\w*("he=e-1 end=")" contains=@m4Top
" Do not highlight `...' strings
syn clear       m4String

" Define the te syntax
syn keyword     teStatement     alias
                              \ allow
                              \ allowxperm
                              \ attribute
                              \ attribute_role
                              \ auditallow
                              \ auditallowxperm
                              \ bool
                              \ class
                              \ dominance
                              \ dontaudit
                              \ dontauditxperm
                              \ fs_use_task
                              \ fs_use_trans
                              \ fs_use_xattr
                              \ genfscon
                              \ neverallow
                              \ neverallowxperm
                              \ permissive
                              \ role
                              \ roleattribute
                              \ roles
                              \ sid
                              \ type
                              \ type_change
                              \ type_transition
                              \ typealias
                              \ typeattribute
                              \ types
                              \ user
syn keyword     teTodo          TODO XXX FIXME NOTE
syn keyword     teConstant      false true s0 mls_systemhigh
syn keyword     teClass         alg_socket
                              \ anon_inode
                              \ association
                              \ blk_file
                              \ bluetooth_socket
                              \ bpf
                              \ cap2_userns
                              \ cap_userns
                              \ capability
                              \ capability2
                              \ chr_file
                              \ context
                              \ dbus
                              \ dccp_socket
                              \ dir
                              \ fd
                              \ fifo_file
                              \ file
                              \ filesystem
                              \ icmp_socket
                              \ io_uring
                              \ ipc
                              \ kernel_service
                              \ key
                              \ key_socket
                              \ lnk_file
                              \ lockdown
                              \ memprotect
                              \ msg
                              \ msgq
                              \ netif
                              \ netlink_audit_socket
                              \ netlink_generic_socket
                              \ netlink_kobject_uevent_socket
                              \ netlink_netfilter_socket
                              \ netlink_route_socket
                              \ netlink_selinux_socket
                              \ netlink_socket
                              \ netlink_tcpdiag_socket
                              \ netlink_xfrm_socket
                              \ node
                              \ packet
                              \ packet_socket
                              \ passwd
                              \ perf_event
                              \ process
                              \ process2
                              \ rawip_socket
                              \ sctp_socket
                              \ security
                              \ sem
                              \ service
                              \ shm
                              \ sock_file
                              \ socket
                              \ system
                              \ tcp_socket
                              \ tun_socket
                              \ udp_socket
                              \ unix_dgram_socket
                              \ unix_stream_socket
                              \ user_namespace
                              \ vsock_socket
syn keyword     tePermission    accept
                              \ acquire_svc
                              \ add_name
                              \ append
                              \ associate
                              \ audit_access
                              \ audit_control
                              \ audit_read
                              \ audit_write
                              \ bind
                              \ block_suspend
                              \ check_context
                              \ checkpoint_restore
                              \ chfn
                              \ chown
                              \ chsh
                              \ compute_av
                              \ compute_create
                              \ compute_member
                              \ compute_relabel
                              \ compute_user
                              \ confidentiality
                              \ connect
                              \ connectto
                              \ cpu
                              \ create
                              \ crontab
                              \ dac_override
                              \ dac_read_search
                              \ destroy
                              \ disable
                              \ dyntransition
                              \ egress
                              \ enable
                              \ enqueue
                              \ entrypoint
                              \ execheap
                              \ execmem
                              \ execmod
                              \ execstack
                              \ execute
                              \ execute_no_trans
                              \ fork
                              \ forward_in
                              \ forward_out
                              \ fowner
                              \ fsetid
                              \ getattr
                              \ getcap
                              \ getopt
                              \ getpgid
                              \ getrlimit
                              \ getsched
                              \ getsession
                              \ halt
                              \ ingress
                              \ ioctl
                              \ ipc_info
                              \ ipc_lock
                              \ ipc_owner
                              \ kill
                              \ lease
                              \ link
                              \ linux_immutable
                              \ listen
                              \ load_policy
                              \ lock
                              \ mac_admin
                              \ map
                              \ map_create
                              \ map_read
                              \ map_write
                              \ mknod
                              \ mmap_zero
                              \ module_load
                              \ module_request
                              \ mount
                              \ mounton
                              \ name_bind
                              \ name_connect
                              \ net_admin
                              \ net_bind_service
                              \ net_broadcast
                              \ net_raw
                              \ nlmsg_read
                              \ nlmsg_readpriv
                              \ nlmsg_relay
                              \ nlmsg_tty_audit
                              \ nlmsg_write
                              \ nnp_transition
                              \ noatsecure
                              \ node_bind
                              \ nosuid_transition
                              \ open
                              \ override_creds
                              \ passwd
                              \ perfmon
                              \ polmatch
                              \ prog_load
                              \ prog_run
                              \ ptrace
                              \ quotaget
                              \ quotamod
                              \ quotaon
                              \ read
                              \ read_policy
                              \ reboot
                              \ receive
                              \ recv
                              \ recvfrom
                              \ relabelfrom
                              \ relabelto
                              \ reload
                              \ remount
                              \ remove_name
                              \ rename
                              \ reparent
                              \ rlimitinh
                              \ rmdir
                              \ rootok
                              \ search
                              \ send
                              \ send_msg
                              \ sendto
                              \ setattr
                              \ setbool
                              \ setcap
                              \ setcheckreqprot
                              \ setcontext
                              \ setcurrent
                              \ setenforce
                              \ setexec
                              \ setfcap
                              \ setfscreate
                              \ setgid
                              \ setkeycreate
                              \ setopt
                              \ setpcap
                              \ setpgid
                              \ setrlimit
                              \ setsched
                              \ setsecparam
                              \ setsockcreate
                              \ setuid
                              \ share
                              \ shutdown
                              \ sigchld
                              \ siginh
                              \ sigkill
                              \ signal
                              \ signull
                              \ sigstop
                              \ sqpoll
                              \ start
                              \ status
                              \ stop
                              \ sys_admin
                              \ sys_boot
                              \ sys_chroot
                              \ sys_module
                              \ sys_nice
                              \ sys_pacct
                              \ sys_ptrace
                              \ sys_rawio
                              \ sys_resource
                              \ sys_time
                              \ sys_tty_config
                              \ syslog
                              \ syslog_console
                              \ syslog_mod
                              \ syslog_read
                              \ tracepoint
                              \ transition
                              \ unix_read
                              \ unix_write
                              \ unlink
                              \ unmount
                              \ use
                              \ validate_trans
                              \ view
                              \ wake_alarm
                              \ watch
                              \ watch_mount
                              \ watch_reads
                              \ watch_sb
                              \ watch_with_perm
                              \ write
                              \ integrity
syn keyword     teSelf          self
syn match       teSelint        /\<selint-disable\>/
syn match       teIoctl         /\<0x[0-9a-fA-F]\+\>/
syn match       teTag           /<\/\?\w\+>/                                            contains=teTodo   contained
syn match       teTag           /<\w\+\(\s\+\w\+="\w\+"\)\+\(\s*\/\)*>/                 contains=teTodo   contained
syn match       teComment       /#.*/                                                   contains=teTodo,teSelint
syn match       teDocumentation /##.*/                                                  contains=teTodo,teTag
syn match       teComment       /^########################################$/
syn match       teContext       /\<[a-z][a-zA-Z0-9_]*\:[a-z][a-zA-Z0-9_]*_r\:[a-z][a-zA-Z0-9_]*\>/
syn match       teNegated       /\w\@<![-~][ ]*\(\w\|\$\)\+/
syn region      teNegated                              start="\~[ ]*{"          end="}" contains=teClass,tePermission,teSelf,teIoctl
syn region      teConditional   matchgroup=teBlock     start="if[ ]*(.*)[ ]*{"  end="}" contains=@m4Top
syn region      teConditional   matchgroup=teBlock     start="else[ ]*{"        end="}" contains=@m4Top
syn region      teConditional   matchgroup=teBlock     start="tunable_policy("  end=")" contains=@m4Top
syn region      teOptional      matchgroup=teBlock     start="optional_policy(" end=")" contains=@m4Top
syn region      teRequire       matchgroup=teBlock     start="gen_require("     end=")" contains=@m4Top
syn region      teInterface     matchgroup=teIfDefine  start="interface("       end=")" contains=@m4Top
syn region      teInterface     matchgroup=teIfDefine  start="template("        end=")" contains=@m4Top
syn region      teQuoted        matchgroup=teQuote     start="`"                end="'" contains=@m4Top
syn region      teSBrackets     matchgroup=teSBracket  start="{"                end="}" contains=teClass,tePermission,teSelf,teIoctl
syn match       teString        /".*"/

" Highlight teStatements and teComments inside of m4-macros
syn cluster     m4Top           contains=m4Comment,m4Constants,m4Special,m4Variable,m4String,m4Paren,m4Command,m4Statement,m4Function,teStatement,teComment,teDocumentation,teNegated,teConditional,teOptional,teRequire,teConstant,teClass,tePermission,teSelf,teString,teQuoted,teIoctl,teContext

" Reset highlighting of region content
hi def link m4Function          NONE
hi def link m4String            NONE
hi def link teOptional          NONE
hi def link teRequire           NONE
hi def link teInterface         NONE
hi def link teConditional       NONE
hi def link teQuoted            NONE
hi def link teSBrackets         NONE

hi def link teStatement         Statement
hi def link teSelf              SpecialChar
hi def link teIoctl             Number
hi def link teTodo              Todo
hi def link teSelint            SpecialComment
hi def link teTag               Delimiter
hi def link teConstant          Constant
hi def link teClass             StorageClass
hi def link teDocumentation     Comment
hi def link teComment           Comment
hi def link teNegated           Exception
hi def link teContext           Structure
hi def link teBlock             PreCondit
hi def link teIfDefine          Define
hi def link teString            String
hi def link teQuote             Label

" Adjust some cterm properties
hi Statement                    cterm=bold
hi SpecialChar                  cterm=italic
hi SpecialComment               cterm=underline  ctermfg=cyan
hi Delimiter                                     ctermfg=LightCyan
hi Constant                     cterm=italic
hi StorageClass                                  ctermfg=LightBlue
hi Exception                                     ctermfg=LightRed
hi Structure                    cterm=italic     ctermfg=LightYellow
hi Define                       cterm=bold       ctermfg=DarkBlue

hi tePermission                 cterm=italic
hi teSBracket                   cterm=bold       ctermfg=red


let b:current_syntax = 'te'
