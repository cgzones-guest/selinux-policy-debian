#!/usr/bin/python3

import argparse
import re
import os


admin_ifs = []
systemd_ifs = []
systemdtmpfiles_ifs = []
systemduser_ifs = []



def scan_file(path):

    with open(path, 'r') as file:
        admin_regex = re.compile(R"\s*(interface|template)\s*\(\s*`\s*(\S+)__admin\s*'\s*,\s*`")
        systemd_regex = re.compile(R"\s*(interface|template)\s*\(\s*`\s*(\S+)__systemd\s*'\s*,\s*`")
        systemdtmpfiles_regex = re.compile(R"\s*(interface|template)\s*\(\s*`\s*(\S+)__systemdtmpfiles\s*'\s*,\s*`")
        systemduser_regex = re.compile(R"\s*(interface|template)\s*\(\s*`\s*(\S+)__systemduser\s*'\s*,\s*`")
        for line in file:
            if match := admin_regex.match(line):
                admin_ifs.append(match.group(2))
            if match := systemd_regex.match(line):
                systemd_ifs.append(match.group(2))
            if match := systemdtmpfiles_regex.match(line):
                systemdtmpfiles_ifs.append(match.group(2))
            if match := systemduser_regex.match(line):
                systemduser_ifs.append(match.group(2))


def scan(dirname):

    for name in os.listdir(dirname):
        path = os.path.join(dirname, name)
        if os.path.isdir(path):
            scan(path)
            continue
        if name.endswith(".if"):
            scan_file(path)
            continue


def copy_stub_file(source, destination):
    destination.write("#\n"
                      "# This is a generated file!  Instead of modifying this file, the\n"
                      f"# {os.path.basename(source)} file should be modified.\n"
                      "#\n"
                     )

    with open(source, 'r') as file:
        for line in file:
            destination.write(line)


def main(argv=None):
    parser = argparse.ArgumentParser(description='Generate reverse permissions.')
    parser.add_argument('--admin_file', required=True, help='The admin policy file')
    parser.add_argument('--systemd_file', required=True, help='The systemd policy file')
    parser.add_argument('--systemdtmpfiles_file', required=True, help='The systemdtmpfiles policy file')
    parser.add_argument('--systemduser_file', required=True, help='The systemduser policy file')
    parser.add_argument('--modules_dir', required=True, help='The modules directory')
    args = parser.parse_args(argv)

    admin_output = open(args.admin_file, "w")
    systemd_output = open(args.systemd_file, "w")
    systemdtmpfiles_output = open(args.systemdtmpfiles_file, "w")
    systemduser_output = open(args.systemduser_file, "w")

    copy_stub_file(args.admin_file + '.in', admin_output)
    copy_stub_file(args.systemd_file + '.in', systemd_output)
    copy_stub_file(args.systemdtmpfiles_file + '.in', systemdtmpfiles_output)
    copy_stub_file(args.systemduser_file + '.in', systemduser_output)

    scan(args.modules_dir)

    admin_ifs.sort()
    systemd_ifs.sort()
    systemdtmpfiles_ifs.sort()
    systemduser_ifs.sort()

    admin_output.write('\n\n########################################\n#\n# Generated admin interfaces\n#\n')
    for iface in admin_ifs:
        admin_output.write("optional_policy(`\n\t" + iface + "__admin(sysadm_t, sysadm_r, sysadm) #selint-disable:C-001\n')\n")
    admin_output.write('#\n# Generated admin interfaces\n#\n########################################\n')

    systemd_output.write('\n\n########################################\n#\n# Generated systemd interfaces\n#\n')
    for iface in systemd_ifs:
        systemd_output.write("optional_policy(`\n\t" + iface + "__systemd(systemd_t, systemd_generic_bin_t) #selint-disable:C-001\n')\n")
    systemd_output.write('#\n# Generated systemd interfaces\n#\n########################################\n')

    systemdtmpfiles_output.write('\n\n########################################\n#\n# Generated systemdtmpfiles interfaces\n#\n')
    for iface in systemdtmpfiles_ifs:
        systemdtmpfiles_output.write("optional_policy(`\n\t" + iface + "__systemdtmpfiles(systemd_tmpfiles_t) #selint-disable:C-001\n')\n")
    systemdtmpfiles_output.write('#\n# Generated systemdtmpfiles interfaces\n#\n########################################\n')

    systemduser_output.write('\n\n########################################\n#\n# Generated systemduser interfaces\n#\n')
    for iface in systemduser_ifs:
        systemduser_output.write("optional_policy(`\n\t" + iface + "__systemduser(systemd_user_instance_domain) #selint-disable:C-001\n')\n")
    systemduser_output.write('#\n# Generated systemduser interfaces\n#\n########################################\n')

    admin_output.close()
    systemd_output.close()
    systemdtmpfiles_output.close()
    systemduser_output.close()


if __name__ == '__main__':
    import sys
    sys.exit(main())
